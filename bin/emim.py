#!/usr/bin/env python
# -*- coding:utf-8 -*-
################################################################################
#      This program is free software; you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation; either version 2 of the License, or
#      (at your option) any later version.
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#      You should have received a copy of the GNU General Public License
#      along with this program; if not, write to the Free Software
#      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#      MA 02110-1301, USA.
###############################################################################
                                    ##TODO##                                    
#migrate from static strings into lang and operator_data dictionaries
#create deb
#create ppa
#device usb1 shift if fail on usb0
#better send dialogue
#better sms manage outbox , reply, sent,contact
#enable log sms support
#fetch text from ext rc files
                                                                                
import pygtk
pygtk.require('2.0')
import gtk
from sys import exit
from gobject import timeout_add_seconds
from time import sleep
from os import path,makedirs
from base64 import b64decode 
from re import search ,split
import subprocess

try:
    import gammu
except ImportError:
    print 'please check if you have the package python-gammu if you use ubuntu try: \nsudo apt-get install python-gammu'
    exit(0)

def delete_event(widget,data):
    False

def destroy(widget,data=None):
    gtk.main_quit()

def USB_activate(data=None):
    global inactivate_handler_id
    try:
        sm.ReadConfig()
        sm.Init()
        main_window_activate_button.disconnect(activate_handler_id)
        inactivate_handler_id=main_window_activate_button.connect('clicked',USB_inactivate)
        main_window_activate_button.set_label('Inactivate')
        main_window_activate_button.set_tooltip_text('Click to Inactivate this program.')
        button_sensitivity(True)
        main_window_activate_button.set_sensitive(True)
        network_code=sm.GetNetworkInfo()['NetworkCode']
        if network_code!='602 03':
            GUI_pop_message('No etisalat SIM could be detected, Not all functions will work as it is supposed to, some options will be disabled','Error')
            main_window_getcredit_button.set_sensitive(False)
            main_window_recharge_button.set_sensitive(False)
            main_window_setpackage_button.set_sensitive(False)
            
    except gammu.ERR_DEVICENOTEXIST:
        msg=lang['nousb']
        GUI_error_message(msg,1)
        fix_activate()
    except gammu.ERR_NOSIM:
        GUI_error_message('No SIM card can be detected',1)
        fix_activate()
        sm.Terminate()
    except gammu.ERR_DEVICEOPENERROR:
        msg=lang['usb_used']
        GUI_error_message(msg,2)
        fix_activate()
    except gammu.ERR_CANTOPENFILE:
        gammubase64='IyBUaGlzIGlzIGEgZ2VuZXJhdGVkIGdhbW11cmMgZmlsZS4KIyBJdCB3YXMgZ2VuZXJhdGVkIGJ5IEdhbW11IGNvbmZpZ3VyYXRvciAwLjQKIyBJbiBVbml4L0xpbnV4ICA6IGNvcHkgaXQgaW50byB5b3VyIGhvbWUgZGlyZWN0b3J5IGFuZCBuYW1lIGl0IC5nYW1tdXJjCiMgICAgICAgICAgICAgICAgICBvciBpbnRvIC9ldGMgYW5kIG5hbWUgaXQgZ2FtbXVyYwojIEluIFdpbjMyICAgICAgIDogY29weSBpdCBpbnRvIGRpcmVjdG9yeSB3aXRoIEdhbW11LmV4ZSBhbmQgbmFtZSBnYW1tdXJjCiMgUG9ydCAgICAgICAgICAgOiBpbiBXaW5kb3dzL0RPUzogImNvbSo6IiwKIyAgICAgICAgICAgICAgICAgIChpbnN0ZWFkIG9mICIqIiBwbGVhc2UgcHV0ICIxIiwgIjIiLCBldGMuKQojICAgICAgICAgICAgICAgICAgaW4gb3RoZXIgKExpbnV4L1VuaXgpICIvZGV2L3R0eVMlIgojICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yICIvZGV2L2lyY29tbSUiICgiaXJkYSIgY29ubmVjdGlvbikKIyAgICAgICAgICAgICAgICAgIChpbnN0ZWFkIG9mICIlIiBwbGVhc2UgcHV0ICIwIiwgIjEiLCAiMiIsIGV0Yy4pCiMgTW9kZWwgICAgICAgICAgOiB1c2Ugb25seSwgd2hlbiBHYW1tdSBkb2Vzbid0IHJlY29nbml6ZSB5b3VyIHBob25lIG1vZGVsLgojICAgICAgICAgICAgICAgICAgUHV0IGl0IGhlcmUuIEV4YW1wbGUgdmFsdWVzOiAiNjExMCIsICI2MTUwIiwgIjYyMTAiLCAiODIxMCIKIyBDb25uZWN0aW9uICAgICA6IHR5cGUgb2YgY29ubmVjdGlvbi4gVXNlICJmYnVzIiBvciAibWJ1cyIgb3IgImRscjMiIG9yCiMgICAgICAgICAgICAgICAgICAiaXJkYSIgKEluZnJhcmVkIG92ZXIgc29ja2V0cykgb3IgImluZnJhcmVkIiAoRGlyZWN0SVIpCiMgICAgICAgICAgICAgICAgICBvciAiYXQxOTIwMCIgKEFUIGNvbW1hbmRzIG9uIDE5MjAwLCA4IGJpdHMsIE5vbmUgcGFyaXR5LAojICAgICAgICAgICAgICAgICAgMSBzdG9wIGJpdCwgbm8gZmxvdyBjb250cm9sKSBvciAiYXQxMTUyMDAiIChBVCBjb21tYW5kcyBvbgojICAgICAgICAgICAgICAgICAgMTE1MjAwLCA4IGJpdHMsIE5vbmUgcGFyaXR5LCAxIHN0b3AgYml0LCBubyBmbG93IGNvbnRyb2wpCiMgICAgICAgICAgICAgICAgICBvciAiYXRibHVlIiAoQVQgb3ZlciBCbHVlVG9vdGgpIG9yICJkbHIzYmx1ZSIgKEZCVVMKIyAgICAgICAgICAgICAgICAgIG92ZXIgQmx1ZVRvb3RoKQojIFN5bmNocm9uaXplVGltZTogaWYgeW91IHdhbnQgdG8gc2V0IHRpbWUgZnJvbSBjb21wdXRlciB0byBwaG9uZSBkdXJpbmcKIyAgICAgICAgICAgICAgICAgIHN0YXJ0aW5nIGNvbm5lY3Rpb24uIERvIG5vdCByYXRoZXIgdXNlIHRoaXMgb3B0aW9uIHdoZW4gd2FudAojICAgICAgICAgICAgICAgICAgdG8gcmVzZXQgcGhvbmUgZHVyaW5nIGNvbm5lY3Rpb24gKGluIHNvbWUgcGhvbmVzIG5lZWQgdG8KIyAgICAgICAgICAgICAgICAgIHNldCB0aW1lIGFnYWluIGFmdGVyIHJlc3RhcnQpCiMgTG9nZmlsZSAgICAgICAgOiBVc2UsIHdoZW4gd2FudCB0byBoYXZlIGxvZ2ZpbGUgZnJvbSBjb21tdW5pY2F0aW9uLgojIExvZ2Zvcm1hdCAgICAgIDogV2hhdCBkZWJ1ZyBpbmZvIGFuZCBmb3JtYXQgc2hvdWxkIGJlIHVzZWQ6CiMgICAgICAgICAgICAgICAgICAibm90aGluZyIgLSBubyBkZWJ1ZyBsZXZlbCwgInRleHQiIC0gdHJhbnNtaXNzaW9uIGR1bXAgaW4KIyAgICAgICAgICAgICAgICAgIHRleHQgZm9ybWF0LCAidGV4dGFsbCIgLSBhbGwgcG9zc2libGUgaW5mbyBpbiB0ZXh0IGZvcm1hdCwKIyAgICAgICAgICAgICAgICAgICJlcnJvcnMiICAtIGVycm9ycyBpbiB0ZXh0IGZvcm1hdCwgImJpbmFyeSIgLSB0cmFuc21pc3Npb24KIyAgICAgICAgICAgICAgICAgIGR1bXAgaW4gYmluYXJ5IGZvcm1hdAojIFVzZV9Mb2NraW5nICAgIDogdW5kZXIgVW5peC9MaW51eCB1c2UgInllcyIsIGlmIHdhbnQgdG8gbG9jayB1c2VkIGRldmljZQojICAgICAgICAgICAgICAgICAgdG8gcHJldmVudCB1c2luZyBpdCBieSBvdGhlciBhcHBsaWNhdGlvbnMKIyBHYW1tdUxvYyAgICAgICA6IG5hbWUgb2YgbG9jYWxpc2F0aW9uIGZpbGUKW2dhbW11XQpwb3J0ID0gL2Rldi90dHlVU0IwCmNvbm5lY3Rpb24gPSBhdDExNTIwMAojbW9kZWwgPQojc3luY2hyb25pemV0aW1lID0KI2xvZ2ZpbGUgPQojbG9nZm9ybWF0ID0gbm90aGluZwojdXNlX2xvY2tpbmcgPQojZ2FtbXVsb2MgPQo='
        gammu_file_content=b64decode(gammubase64)        
        gammu_file_path=home_path+'/.gammurc'
        create_file(gammu_file_path,gammu_file_content)
        GUI_pop_message(lang['gammurc_create'][0],lang['gammurc_create'][1])
        fix_activate()
    except gammu.ERR_DEVICENOPERMISSION:
        GUI_error_message("You Don't have permission to use the USB Modem, please check your user rights ?!!",1)
        fix_activate()
    except gammu.ERR_DEVICEBUSY:
        GUI_error_message("USB Device is busy .Error opening device, it's already opened by another application",1)
        fix_activate()
    except gammu.ERR_DEVICENOTWORK:
        GUI_error_message("The device is wrongly connected",1)
        fix_activate()
    except gammu.ERR_TIMEOUT:
        GUI_error_message("The device is Unresponsive , Try disconnect it then reconnect",1)
        fix_activate()
    except gammu.ERR_DEVICELOCKED:
        GUI_error_message("The device is Locked , Try disabling Mobile broaband from network manager indicator or just clicking 'Disable Wwan and Retry' below",2)
        fix_activate()        

def fix_activate(data=None):
    main_window_activate_button.set_label('Activate')
    main_window_activate_button.set_sensitive(True)
    
def USB_inactivate(data=None):
    global activate_handler_id
    sm.ReadConfig()
    sm.Terminate()
    main_window_activate_button.set_label('Activate')
    main_window_activate_button.disconnect(inactivate_handler_id)
    activate_handler_id=main_window_activate_button.connect('clicked',USB_activate)
    main_window_activate_button.set_tooltip_text('Click to activate this program.\nNote : you will be disconnected\nfrom the internet!!')
    button_sensitivity(False)

def get_all_sms(data=None):
    global messages
    messages=[]
    for i in range(40):
        try:
            sms = sm.GetSMS(1,i)
            for m in sms:
                time=search('\w+:\w+:\w+',str(m['DateTime'])).group()
                date=search('\w+-\w+-\w+',str(m['DateTime'])).group()
                DATA={
                'Location':m['Location'],
                'DateTime':str(m['DateTime']),
                'Date':date,
                'Time':time,
                'Sender'  :m['Number'],
                'Text'    :m['Text']}   
                messages.append(DATA)
        except gammu.ERR_EMPTY: 
            pass
        except AttributeError:
            print "Unknown error trying to retrieve Messages. May be you don't have permissions"
        except gammu.ERR_INVALIDLOCATION:
            GUI_error_message('Invalid Location in SIM , please contact the emim developer with your SIM operator and Modem Model',1)
    messages.sort(reverse=True)

def get_last_sms(data=None):    
    sleep(5)
    get_all_sms()
    serial=[]
    for m in messages:
        slum={m['DateTime']:m['Location']}
        serial.append(slum)
    serial.sort()
    serial_index=len(serial)
    if serial_index==0:
        pop_message_text_label.set_text('No SMS in your inbox.. Wait a moment and check your inbox :(')
    try:
        last_serial_index=serial_index-1
        last_sms_dict=serial[last_serial_index]
        for x ,y in last_sms_dict.iteritems():
            last_sms_index=last_sms_dict[x]
        last_sms=sm.GetSMS(1,last_sms_index)
        for m in last_sms:
            text=m['Text']
        pop_message_text_label.set_text(text)
    except IndexError:
        pop_message_text_label.set_text('No Credit SMS Recieved.. Wait a moment and check your inbox :(')
    refresh_tree_view()

def delete_all_sms(data=None):
    GUI_pop_message(lang['msg_del_wait'][0],lang['msg_del_wait'][1])
    get_all_sms()
    serial=[]
    for m in messages:
        serial.append(m['Location'])
    for i in serial:
        sm.DeleteSMS(1,i) 
    pop_message_text_label.set_text(lang['msg_del_done'])
    refresh_tree_view()
    
def delete_single_sms(data=None):
    sm.DeleteSMS(1,int(selected_msg))
    if tree_iter==None:
        return False
    tree_model.remove(tree_iter)
    sms_toolbar_delete_button.set_sensitive(False)

def submit_recharge_card(usb_recharge_text_entry):
    code= usb_recharge_text_entry.get_text()
    if len(code)<operator_data['credit_limit']:
        GUI_pop_message(lang['credit_no_inalid'][0],lang['credit_no_inalid'][1])
        return 0
    try:
        int(code)
    except ValueError:
        GUI_error_message(lang['credit_no_inalid'][0],1)
        return 0
    number=operator_data['recharge'][1]
    send_sms_to_number(number,code)
    GUI_pop_message(lang['credit_submit_wait'][0],lang['credit_submit_wait'][1]) 
    timeout_add_seconds(2,get_last_sms)

def create_file(file_path_name,content):
    file_path=path.dirname(file_path_name)
    try:
        f=file(file_path_name,'w')
        f.write(content)
        f.close()
    except IOError:
        if path.exists(file_path):
            print 'UNKNOWN ERROR'
        else:
            makedirs(file_path)
            f=file(file_path_name,'w')
            f.write(content)
            f.close()           

def disable_Wwan(data=None):
    raw_uuid=subprocess.check_output(['nmcli', 'con', 'list'])
    uuid=search('(\w+\-\w+\-\w+\-\w+\-\w+)(\s+gsm)', raw_uuid).group(1)
    drop_connection='nmcli con down uuid '+uuid
    undrop_connection='nmcli con up uuid '+uuid
    disable_wwan="nmcli nm wwan off"
    enable_wwan="nmcli nm wwan on"
    enable_network="nmcli nm enable true"
    disable_network="nmcli nm enable false"
    drop_connection_list=split('\s', drop_connection)
    disable_wwan_list=split('\s', disable_wwan)
    enable_network_list=split('\s',enable_network)
    disable_network_list=split('\s',disable_network)
    undrop_connection_list=split('\s',undrop_connection)
    enable_wwan_list=split('\s',enable_wwan)
    subprocess.call(enable_network_list)
    subprocess.call(enable_wwan_list)
    subprocess.call(undrop_connection_list)    
    subprocess.call(drop_connection_list)
    subprocess.call(disable_wwan_list)
    subprocess.call(disable_network_list)
    
    timeout_add_seconds(2,USB_activate)
    main_window_activate_button.set_sensitive(False)
    main_window_activate_button.set_label('Activating')

def button_sensitivity(Bool):
    main_window_managesms_button.set_sensitive(Bool)
    main_window_getcredit_button.set_sensitive(Bool)
    main_window_recharge_button.set_sensitive(Bool)
    main_window_setpackage_button.set_sensitive(Bool)
    main_window_usbinfo_button.set_sensitive(Bool)

def get_credit(data=None):
    GUI_pop_message(lang['wait_5_chk_crdt'][0],lang['wait_5_chk_crdt'][1])
    timeout_add_seconds(2,send_sms_to_number,operator_data['credit'][1],operator_data['credit'][2])
    timeout_add_seconds(2,get_last_sms)

def refresh_tree_view(data=None):
    sms_list.clear()
    get_all_sms()
    for i in messages:
        sms_list.append([i['Location'],i['Date'],i['Time'],i['Sender'],i['Text']])    
    if sms_window_started:
        sms_toolbar_delete_button.set_sensitive(False)

def on_message_select(widget,data=None):
    global selected_msg,tree_model,tree_iter
    selection = treeview.get_selection()
    selection.set_mode(gtk.SELECTION_SINGLE)
    tree_model, tree_iter = selection.get_selected()
    if tree_iter==None:
        return False
    selected_msg = tree_model.get_value(tree_iter, 0)
    sms_toolbar_delete_button.set_sensitive(True)

def on_radio_toggled(widget,data):
    global number ,text
    if widget.get_active():
        number=data[0]
        text=data[1]

def evaluate_and_send(send_sms_window_sms_number,text_buffer):
    text=text_buffer.get_text(text_buffer.get_start_iter() , text_buffer.get_end_iter())
    number=send_sms_window_sms_number.get_text()
    if len(number)>15:
        GUI_error_message(lang['sms_invalid'][0],1,send_sms_window)
        return 0
    try:
        int(number)
    except ValueError:
        GUI_error_message(lang['sms_invalid'][0],1,send_sms_window)
        return 0       
    if len(text)>70:
        GUI_error_message(lang['sms_txt_70'][0],1,send_sms_window)
        return 0
    send_sms_to_number(number,text)

def send_sms_to_number(number,text):
    message = {
    'Text'  :   text,
    'SMSC'  :   {'Location':1},
    'Number':   number,
    'Coding':   'Unicode_No_Compression'
    }
    try:
        sm.SendSMS(message)
        gtk.Widget.destroy(send_sms_window)
    except gammu.ERR_UNKNOWN:
        GUI_error_message("Can't send SMS to this number !",1)
    except NameError:
        pass
def apply_package(data=None):
    send_sms_to_number(number,text) 
    GUI_pop_message(lang['pkg_apply'][0],lang['pkg_apply'][1])   

def GUI_pop_message(message,title,transientfor=None):
    global pop_message_window,pop_message_text_label
    pop_message_window=gtk.Window(gtk.WINDOW_TOPLEVEL)
    pop_message_window.set_modal(True)
    pop_message_window.set_title(title)
    pop_message_window.set_transient_for(transientfor)
    pop_message_window.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    pop_message_window.set_resizable(False)
    pop_message_window.set_border_width(10)
    pop_message_text_label=gtk.Label()
    pop_message_text_label.set_justify(gtk.JUSTIFY_FILL)
    pop_message_text_label.set_line_wrap(True)
    pop_message_text_label.set_text(message)
    pop_message_window_vbox=gtk.VBox(True,0)
    pop_message_window_hbox=gtk.HBox(True,0)
    pop_message_window_close_button=gtk.Button(lang['ok'])
    pop_message_window_close_button.connect_object('clicked',gtk.Widget.destroy,pop_message_window)
    pop_message_window_close_button.set_flags(gtk.CAN_DEFAULT)
    pop_message_window.set_default(pop_message_window_close_button)
    pop_message_window.add(pop_message_window_vbox)
    pop_message_window_vbox.pack_start(pop_message_text_label,True,True,0)
    pop_message_window_vbox.pack_start(pop_message_window_hbox,False,False,0)
    pop_message_window_hbox.pack_start(pop_message_window_close_button,False,False,0)
    pop_message_window.show_all()

def GUI_error_message(error_msg,error_code,transientfor=None):
    global connect_error_window,connect_error_text_label
    connect_error_window=gtk.Window(gtk.WINDOW_TOPLEVEL)
    connect_error_window.set_modal(True)
    connect_error_window.set_title(lang['err_msg_title'])
    connect_error_window.set_transient_for(transientfor)
    connect_error_window.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    connect_error_window.set_resizable(False)
    connect_error_window.set_border_width(10)
    connect_error_text_label=gtk.Label("")
    connect_error_text_label.set_justify(gtk.JUSTIFY_FILL)
    connect_error_text_label.set_line_wrap(True)
    connect_error_text_label.set_text(error_msg)
    connect_error_window_vbox=gtk.VBox(True,0)
    connect_error_window_hbox=gtk.HBox(True,0)
    connect_error_window_close_button=gtk.Button(lang['close'])
    connect_error_window_close_button.connect_object('clicked',gtk.Widget.destroy,connect_error_window)
    connect_error_window.add(connect_error_window_vbox)
    connect_error_window_vbox.pack_start(connect_error_text_label,True,True,0)
    connect_error_window_vbox.pack_start(connect_error_window_hbox,False,False,0)
    connect_error_window_hbox.pack_start(connect_error_window_close_button,False,False,0)
    connect_error_window_close_button.set_flags(gtk.CAN_DEFAULT)
    connect_error_window.set_default(connect_error_window_close_button)
    if error_code==2:
        connect_error_window_wwan_disable_button=gtk.Button(lang['DiswanRtry'])
        connect_error_window_wwan_disable_button.connect('clicked',disable_Wwan)
        connect_error_window_wwan_disable_button.connect_object('clicked',gtk.Widget.destroy,connect_error_window)
        connect_error_window_hbox.pack_start(connect_error_window_wwan_disable_button,True,True,0)
    connect_error_window.show_all()
    
def GUI_usb_recharge(data=None):
    usb_recharge_window=gtk.Window(gtk.WINDOW_TOPLEVEL)
    usb_recharge_window.set_modal(True)
    usb_recharge_window.set_transient_for(main_window)
    usb_recharge_window.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    usb_recharge_window.set_resizable(False)
    usb_recharge_window.set_title('Recharge Your Credit')
    usb_recharge_window.set_border_width(15)
    usb_recharge_text_label=gtk.Label()
    usb_recharge_text_label.set_label(lang['rechargetxtlabel'][0]+str(operator_data['credit_limit'])+lang['rechargetxtlabel'][1])
    usb_recharge_vbox=gtk.VBox(False,10)
    usb_recharge_hbox=gtk.HBox(False,10)
    usb_recharge_entry=gtk.Entry(max=int(operator_data['credit_limit']))
    usb_recharge_text_entry=gtk.Entry.get_text(usb_recharge_entry)
    usb_recharge_submit_button=gtk.Button(lang['submit'])    
    usb_recharge_cancel_button=gtk.Button(lang['cancel'])
    usb_recharge_submit_button.set_flags(gtk.CAN_DEFAULT)
    usb_recharge_window.set_default(usb_recharge_submit_button)
    usb_recharge_window.add(usb_recharge_vbox)
    usb_recharge_vbox.pack_start(usb_recharge_text_label,False,False,0)
    usb_recharge_vbox.pack_start(usb_recharge_entry,False,False,0)
    usb_recharge_vbox.pack_start(usb_recharge_hbox,False,False,0)
    usb_recharge_hbox.pack_start(usb_recharge_submit_button,True,False,0)
    usb_recharge_hbox.pack_start(usb_recharge_cancel_button,True,False,0)
    usb_recharge_cancel_button.connect_object('clicked',gtk.Widget.destroy,usb_recharge_window)
    usb_recharge_submit_button.connect_object('clicked',submit_recharge_card,usb_recharge_entry)
    usb_recharge_submit_button.connect_object('clicked',gtk.Widget.destroy,usb_recharge_window)
    usb_recharge_window.show_all()

def GUI_send_sms(data=None):
    global send_sms_window
    send_sms_window=gtk.Window(gtk.WINDOW_TOPLEVEL)
    send_sms_window.set_title(lang['snd_sms_ttl'])
    send_sms_window.set_modal(True)
    send_sms_window.set_transient_for(sms_show_window)
    send_sms_window.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    send_sms_window.set_icon_from_file(eLOGO_file)
    send_sms_window.set_size_request(450,150)
    send_sms_window.set_resizable(False)
    send_sms_window.set_border_width(10)
    send_sms_window_vbox=gtk.VBox(False,0)
    send_sms_window_hbox1=gtk.HBox(False,0)
    send_sms_window_hbox2=gtk.HBox(False,0)
    send_sms_window_hbox3=gtk.HBox(True,0)
    send_sms_scrolled_window = gtk.ScrolledWindow()
    send_sms_scrolled_window.set_border_width(0)
    send_sms_scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_ALWAYS)
    send_sms_window_number_label=gtk.Label(lang['send_sms_label'][0])
    send_sms_window_message_label=gtk.Label(lang['send_sms_label'][1])
    send_sms_window_sms_number=gtk.Entry(max=15)
    send_sms_Message=gtk.TextBuffer()
    send_sms_Message.set_text('')
    send_sms_Message_TextView=gtk.TextView(send_sms_Message)
    send_sms_Message_TextView.set_editable(True)
    send_sms_Message_TextView.set_wrap_mode(gtk.WRAP_WORD)
    send_sms_send_button=gtk.Button(lang['snd_cls'][0])
    send_sms_close_button=gtk.Button(lang['snd_cls'][1])
    send_sms_scrolled_window.add_with_viewport(send_sms_Message_TextView)
    send_sms_window.add(send_sms_window_vbox)
    text_buffer=send_sms_Message_TextView.get_buffer()
    send_sms_send_button.set_flags(gtk.CAN_DEFAULT)
    send_sms_window.set_default(send_sms_send_button)
    send_sms_window_vbox.pack_start(send_sms_window_hbox1,True,True,0)
    send_sms_window_vbox.pack_start(send_sms_window_hbox2,True,True,0)
    send_sms_window_vbox.pack_start(send_sms_window_hbox3,True,True,0)
    send_sms_window_hbox1.pack_start(send_sms_window_number_label,False,False,0)
    send_sms_window_hbox1.pack_start(send_sms_window_sms_number,True,True,0)
    send_sms_window_hbox2.pack_start(send_sms_window_message_label,False,False,0)
    send_sms_window_hbox2.pack_start(send_sms_scrolled_window,True,True,0)
    send_sms_window_hbox3.pack_start(send_sms_send_button,False,False,0)
    send_sms_window_hbox3.pack_start(send_sms_close_button,False,False,0)
    send_sms_close_button.connect_object('clicked',gtk.Window.destroy,send_sms_window)
    send_sms_send_button.connect_object('clicked',evaluate_and_send,send_sms_window_sms_number,text_buffer)
    send_sms_window.show_all()

def GUI_manage_sms(data=None):
    global sms_show_window,sms_list,treeview,sms_toolbar_delete_button,sms_window_started
    sms_window_started=True
    get_all_sms()
    sms_show_window=gtk.Window(gtk.WINDOW_TOPLEVEL)
    sms_show_window.set_title('Manage SMS Inbox')
    sms_show_window.set_transient_for(main_window)
    sms_show_window.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    sms_show_window.set_resizable(True)
    sms_show_window.set_size_request(800,400)
    sms_show_window.set_border_width(0)
    sms_show_window.set_modal(True)
    sms_toolbar=gtk.Toolbar()
    sms_toolbar.set_style(gtk.TOOLBAR_BOTH)
    sms_toolbar_new_sms_button=gtk.ToolButton('New SMS')
    sms_toolbar_new_sms_button.set_stock_id(gtk.STOCK_NEW)
    sms_toolbar_new_sms_button.set_label('New SMS')
    sms_toolbar_delete_button=gtk.ToolButton('Delete')
    sms_toolbar_delete_button.set_stock_id(gtk.STOCK_DELETE)
    sms_toolbar_delete_button.set_sensitive(False)
    sms_toolbar_delete_all_button=gtk.ToolButton('Delete All')
    sms_toolbar_delete_all_button.set_stock_id(gtk.STOCK_CLEAR)
    sms_toolbar_delete_all_button.set_label('Delete All')
    sms_toolbar_refresh_button=gtk.ToolButton('Refresh')
    sms_toolbar_refresh_button.set_stock_id(gtk.STOCK_REFRESH)
    sms_toolbar_close_button=gtk.ToolButton('Close')
    sms_toolbar_close_button.set_stock_id(gtk.STOCK_QUIT)
    sms_toolbar_close_button.set_label('Close')
    sms_toolbar.insert(sms_toolbar_new_sms_button,-1)
    sms_toolbar.insert(sms_toolbar_delete_button,-1)
    sms_toolbar.insert(sms_toolbar_delete_all_button,-1)
    sms_toolbar.insert(sms_toolbar_refresh_button,-1)
    sms_toolbar.insert(sms_toolbar_close_button,-1)
    sms_toolbar_new_sms_button.connect('clicked',GUI_send_sms)
    sms_toolbar_delete_button.connect('clicked',delete_single_sms)
    sms_toolbar_delete_all_button.connect('clicked',delete_all_sms)
    sms_toolbar_refresh_button.connect('clicked',refresh_tree_view)
    sms_toolbar_close_button.connect_object('clicked',gtk.Widget.destroy,sms_show_window)
    sms_show_window_vbox=gtk.VBox(False,0)
    sms_show_scrolled_window = gtk.ScrolledWindow()
    sms_show_scrolled_window.set_border_width(0)
    sms_show_scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_ALWAYS)
    sms_show_window_vbox.pack_start(sms_toolbar,False,False,0)
    sms_show_window_vbox.pack_start(sms_show_scrolled_window,True,True,0)
    sms_list = gtk.ListStore(str,str,str,str,str)
    for i in messages:
        sms_list.append([i['Location'],i['Date'],i['Time'],i['Sender'],i['Text']])
    treeview = gtk.TreeView(sms_list)
    cell = gtk.CellRendererText()
    col = gtk.TreeViewColumn("Date",cell,text=1)
    col.set_max_width(80)
    treeview.append_column(col)
    col = gtk.TreeViewColumn("Time",cell,text=2)
    col.set_max_width(70)
    treeview.append_column(col)
    col = gtk.TreeViewColumn("Sender",cell,text=3)
    col.set_max_width(110)  
    treeview.append_column(col)
    col = gtk.TreeViewColumn("Message Content",cell,text=4)
    treeview.append_column(col)
    treeview.set_tooltip_column(4)
    treeview.set_has_tooltip(True)
    treeview.connect('cursor-changed',on_message_select,treeview) 
    sms_show_scrolled_window.add_with_viewport(treeview)
    sms_show_window.add(sms_show_window_vbox)
    sms_show_window.show_all()    

def GUI_choose_package(data=None):
    choose_package_window=gtk.Window(gtk.WINDOW_TOPLEVEL)
    choose_package_window.set_title(lang['pkg_chng'])
    choose_package_window.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    choose_package_window.set_size_request(450,170)
    choose_package_window.set_modal(True)
    choose_package_window.set_resizable(False)
    choose_package_window.set_transient_for(main_window)
    choose_package_window.set_border_width(10)
    choose_package_hbox0=gtk.HBox(False,0)
    choose_package_hbox1=gtk.HBox(True,0)
    choose_package_vbox0=gtk.VBox(True,0)
    choose_package_vbox1=gtk.VBox(True,0)
    choose_package_vbox2=gtk.VBox(True,0)
    choose_package_vbox3=gtk.VBox(True,0)
    choose_package_window.add(choose_package_vbox0)
    choose_package_vbox0.pack_start(choose_package_hbox0,False,False,0)    
    choose_package_hbox0.pack_start(choose_package_vbox1,True,True,0)
    choose_package_hbox0.pack_start(choose_package_vbox2,True,True,0)
    choose_package_hbox0.pack_start(choose_package_vbox3,True,True,0)
    choose_package_vbox0.pack_start(choose_package_hbox1,False,False,0)    
    radio=gtk.RadioButton(None,operator_data['go'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('(Pay-As-You-Go) pay 0.30 LE per 1 MB')
    choose_package_vbox1.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['go'][1:3])
    radio=gtk.RadioButton(radio,operator_data['daily'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('(Daily Unlimited) pay 3 LE/day\nand get Unlimited Internet with 20 MB fair\nusage and you can get extra 20 MB for 3 LE')
    choose_package_vbox1.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['daily'][1:3])
    radio=gtk.RadioButton(radio,operator_data['19LE'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('(Monthly Unlimited 19) pay 19 LE/month\nand get Unlimited Internet with 110 MB\nfair usage you can get extra 60 MB for\n10 LE')
    choose_package_vbox1.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['19LE'][1:3])
    radio=gtk.RadioButton(radio,operator_data['49LE'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('(Monthly Unlimited 49) pay 49 LE/month\nand get Unlimited Internet with 500 MB\nfair usage you can get extra 250 MB for\n20 LE')
    choose_package_vbox1.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['49LE'][1:3])
    radio=gtk.RadioButton(radio,operator_data['99LE'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('(Monthly Unlimited 99) pay 99 LE/month\nand get Unlimited Internet with 2GB fair\nusage you can get extra 1 GB for 30 LE')
    choose_package_vbox2.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['99LE'][1:3])
    radio=gtk.RadioButton(radio,operator_data['149LE'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('(Monthly Unlimited 149) pay 149 LE/month\nand get Unlimited Internet with 6 GB\nfair usage you can get extra 1 GB for 30 LE')
    choose_package_vbox2.pack_start(radio,True,True,0)    
    radio.connect('toggled',on_radio_toggled,operator_data['149LE'][1:3])
    radio=gtk.RadioButton(radio,operator_data['249LE'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('(Monthly Unlimited 249) pay 249 LE/month\nand get Unlimited Internet with 10 GB\n fair usage you can get extra 1 GB for 30 LE')
    choose_package_vbox2.pack_start(radio,True,True,0)    
    radio.connect('toggled',on_radio_toggled,operator_data['249LE'][1:3])
    radio=gtk.RadioButton(radio,operator_data['reset'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('Reset speed by buying extra bandwidth according to your package ')
    choose_package_vbox2.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['reset'][1:3])
    radio=gtk.RadioButton(radio,operator_data['enableSP'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('Subscribe to Speed plus service')
    choose_package_vbox3.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['enableSP'][1:3])
    radio=gtk.RadioButton(radio,operator_data['disableSP'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('Unsubscribe to Speed plus service')
    choose_package_vbox3.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['disableSP'][1:3])
    radio=gtk.RadioButton(radio,operator_data['enableLV'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('Subscribe to Never lose validity')
    choose_package_vbox3.pack_start(radio,True,True,0)
    radio.connect('toggled',on_radio_toggled,operator_data['enableLV'][1:3])
    radio=gtk.RadioButton(radio,operator_data['disableLV'][0],True)
    radio.set_has_tooltip(True)
    radio.set_tooltip_text('Unsubscribe from Never lose validity')
    choose_package_vbox3.pack_start(radio,True,True,0)    
    radio.connect('toggled',on_radio_toggled,operator_data['disableLV'][1:3])
    apply_button=gtk.Button(lang['apply_close'][0])    
    choose_package_hbox1.pack_start(apply_button,False,False,0)
    close_button=gtk.Button(lang['apply_close'][1])
    close_button.connect_object('clicked',gtk.Widget.destroy,choose_package_window)
    close_button.set_flags(gtk.CAN_DEFAULT)
    choose_package_window.set_default(close_button)
    apply_button.connect('clicked',apply_package)
    choose_package_hbox1.pack_start(close_button,False,False,0)
    choose_package_window.show_all()

def GUI_about(data=None):
    coded_gpl='VGhpcyBwcm9ncmFtIGlzIGZyZWUgc29mdHdhcmU7IHlvdSBjYW4gcmVkaXN0cmlidXRlIGl0IGFuZC9vciBtb2RpZnkgaXQgdW5kZXIgdGhlIHRlcm1zIG9mIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBhcyBwdWJsaXNoZWQgYnkgdGhlIEZyZWUgU29mdHdhcmUgRm91bmRhdGlvbjsgZWl0aGVyIHZlcnNpb24gMiBvZiB0aGUgTGljZW5zZSwgb3IgKGF0IHlvdXIgb3B0aW9uKSBhbnkgbGF0ZXIgdmVyc2lvbi4gVGhpcyBwcm9ncmFtIGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mIE1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4gIFNlZSB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy4gWW91IHNob3VsZCBoYXZlIHJlY2VpdmVkIGEgY29weSBvZiB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgIGFsb25nIHdpdGggdGhpcyBwcm9ncmFtOyBpZiBub3QsIHdyaXRlIHRvIHRoZSBGcmVlIFNvZnR3YXJlIEZvdW5kYXRpb24sIEluYy4sIDUxIEZyYW5rbGluIFN0cmVldCwgRmlmdGggRmxvb3IsIEJvc3RvbiwgTUEgMDIxMTAtMTMwMSwgVVNBLg=='
    decoded_gpl=b64decode(coded_gpl)
    about_dialog=gtk.AboutDialog()
    about_dialog.set_program_name(app_name[1])
    about_dialog.set_icon_from_file(eLOGO_file)
    about_dialog.set_version(app_version)
    about_dialog.set_copyright(release_date)
    about_dialog.set_license(decoded_gpl)
    about_dialog.set_wrap_license(True)
    coded_comment='QnkKRHIuQW1yIE9zbWFuCmRyM21yb0BnbWFpbC5jb20KCkZvcgpNeSBCZWxvdmVkIFdpZmUKCmFuZApMaW51eCBBcmFiIENvbW11bml0eQp3d3cubGludXhhYy5vcmc='
    decoded_comment=b64decode(coded_comment)
    about_dialog.set_comments(decoded_comment)
    about_dialog.set_website("http://emim.co.nr")
    about_dialog.set_logo(gtk.gdk.pixbuf_new_from_file(operator_data['logo']))
    about_dialog.connect("response", lambda d, r: d.destroy())
    about_dialog.set_modal(True)
    about_dialog.show_all()

def GUI_usb_info(data=None):
    usb_info_window=gtk.Window(gtk.WINDOW_TOPLEVEL)
    usb_info_window.set_title('USB Info')
    usb_info_window.set_transient_for(main_window)
    usb_info_window.set_modal(True)
    usb_info_window.set_border_width(10)
    usb_info_window.set_resizable(False)
    usb_info_window.set_position(gtk.WIN_POS_CENTER_ON_PARENT)
    sm.ReadConfig()
    imei=sm.GetIMEI()
    model=sm.GetModel()[1]
    quality=str(sm.GetSignalQuality()['SignalPercent']) + ' %'
    manufacturer=sm.GetManufacturer()
    firmware=sm.GetFirmware()[2]
    network_code=sm.GetNetworkInfo()['NetworkCode']#etisalat '602 03'
    if network_code=='602 03':
        operator='etisalat' 
    else:
        operator='unknown'
    simimsi=sm.GetSIMIMSI()
    device=sm.GetConfig()['Device']
    usb_info_table=gtk.Table(2,2)
    usb_info_label_key=gtk.Label('USB Modem\nUSB Model\nUSB IMEI\nDevice\nFirmware\nNetwork\nSIM IMSI\nOperator\nSignal')
    usb_info_label_sep=gtk.Label('\t:\t\n\t:\t\n\t:\t\n\t:\t\n\t:\t\n\t:\t\n\t:\t\n\t:\t\n\t:\t')
    usb_info_label_data=gtk.Label(manufacturer + '\n' + model + '\n' + imei + '\n' + device + '\n' + `firmware` + '\n' + network_code + '\n' + simimsi + '\n' + operator+ '\n' + quality)
    usb_info_table.attach(usb_info_label_key,1,2,1,2)
    usb_info_table.attach(usb_info_label_sep,2,3,1,2)
    usb_info_table.attach(usb_info_label_data,3,4,1,2)
    usb_info_window.add(usb_info_table)
    usb_info_window.show_all()


def GUI_main():
    global sm , main_window,activate_handler_id ,main_window_activate_button ,main_window_managesms_button, main_window_getcredit_button , main_window_recharge_button ,main_window_setpackage_button ,main_window_about_button ,main_window_quit_button ,main_window_usbinfo_button ,activate_handler_id 
    sm = gammu.StateMachine()
    settings = gtk.settings_get_default()
    settings.props.gtk_button_images = True
    #main window
    main_window=gtk.Window(gtk.WINDOW_TOPLEVEL)
    main_window.set_title(lang['main_title'])
    main_window.set_resizable(False)
    main_window.set_border_width(5)
    main_window.set_position(gtk.WIN_POS_CENTER)
    main_window.set_icon_from_file(eLOGO_file)
    main_window.connect('destroy',destroy)
    main_window.connect('delete_event',delete_event)
    main_window_vbox=gtk.VBox(False,0)
    main_window_hbox1=gtk.HBox(False,0)
    main_window_hbox2=gtk.HBox(False,0)
    main_window.add(main_window_vbox)
    main_window_vbox.pack_start(main_window_hbox1,False,False,0)
    main_window_vbox.pack_start(main_window_hbox2,False,False,0)

    activate_logo=gtk.Image()
    activate_logo.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(files_dict['activate_png'][0]).scale_simple(64,64,gtk.gdk.INTERP_BILINEAR))
    main_window_activate_button=gtk.Button("Activate")
    main_window_activate_button.set_image(activate_logo)
    main_window_activate_button.set_image_position(gtk.POS_TOP)
    main_window_activate_button.set_size_request(128,128)
    main_window_hbox1.pack_start(main_window_activate_button,False,False,0)
    main_window_activate_button.set_border_width(5)
    activate_handler_id=main_window_activate_button.connect('clicked',USB_activate)
    main_window_activate_button.set_tooltip_text('Click to activate this program.\nNote : you will be disconnected\nfrom the internet!!')
    main_window_activate_button.set_flags(gtk.CAN_DEFAULT)
    main_window.set_default(main_window_activate_button)
    
    usb_info_logo=gtk.Image()
    usb_info_logo.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(files_dict['usb_stick_png'][0]).scale_simple(64,64,gtk.gdk.INTERP_BILINEAR))
    main_window_usbinfo_button=gtk.Button("USB info")
    main_window_usbinfo_button.set_image(usb_info_logo)
    main_window_usbinfo_button.set_image_position(gtk.POS_TOP)
    main_window_usbinfo_button.set_size_request(128,128)
    main_window_hbox2.pack_start(main_window_usbinfo_button,False,False,0)
    main_window_usbinfo_button.set_border_width(5)
    main_window_usbinfo_button.connect('clicked',GUI_usb_info)
    main_window_usbinfo_button.set_tooltip_text('Click here to view information\nabout your USB Modem')
    

    managesms_logo=gtk.Image()
    managesms_logo.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(files_dict['mail_png'][0]).scale_simple(64,64,gtk.gdk.INTERP_BILINEAR))
    main_window_managesms_button=gtk.Button("Manage SMS")
    main_window_managesms_button.set_image(managesms_logo)
    main_window_managesms_button.set_image_position(gtk.POS_TOP)
    main_window_managesms_button.set_size_request(128,128)
    main_window_hbox1.pack_start(main_window_managesms_button,False,False,0)
    main_window_managesms_button.set_border_width(5)
    main_window_managesms_button.connect('clicked',GUI_manage_sms)
    main_window_managesms_button.set_tooltip_text('Click here to Send SMS,\nView SMS , Delete SMS')
    
    
    setpackage_logo=gtk.Image()
    setpackage_logo.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(files_dict['settings_png'][0]).scale_simple(64,64,gtk.gdk.INTERP_BILINEAR))
    main_window_setpackage_button=gtk.Button("Set Package")
    main_window_setpackage_button.set_image(setpackage_logo)
    main_window_setpackage_button.set_image_position(gtk.POS_TOP)
    main_window_setpackage_button.set_size_request(128,128)
    main_window_hbox2.pack_start(main_window_setpackage_button,False,False,0)
    main_window_setpackage_button.set_border_width(5)
    main_window_setpackage_button.connect('clicked',GUI_choose_package)
    main_window_setpackage_button.set_tooltip_text('Click Here to change the package\nyou are subscribed to')
    
    
    getcredit_logo=gtk.Image()
    getcredit_logo.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(files_dict['credit_png'][0]).scale_simple(64,64,gtk.gdk.INTERP_BILINEAR))
    main_window_getcredit_button=gtk.Button("Credit")
    main_window_getcredit_button.set_image(getcredit_logo)
    main_window_getcredit_button.set_image_position(gtk.POS_TOP)
    main_window_getcredit_button.set_size_request(128,128)
    main_window_hbox1.pack_start(main_window_getcredit_button,False,False,0)
    main_window_getcredit_button.set_border_width(5)
    main_window_getcredit_button.connect('clicked',get_credit)
    main_window_getcredit_button.set_tooltip_text('Click here to send SMS to\n555 and retrieve your credit')
    
    
    about_logo=gtk.Image()
    about_logo.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(files_dict['info_png'][0]).scale_simple(64,64,gtk.gdk.INTERP_BILINEAR))
    main_window_about_button=gtk.Button("About")
    main_window_about_button.set_image(about_logo)
    main_window_about_button.set_image_position(gtk.POS_TOP)
    main_window_about_button.set_size_request(128,128)
    main_window_hbox2.pack_start(main_window_about_button,False,False,0)
    main_window_about_button.set_border_width(5)
    main_window_about_button.connect('clicked',GUI_about)
    main_window_about_button.set_tooltip_text('Click here to view information\nabout this Application')
    
    recharge_logo=gtk.Image()
    recharge_logo.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(files_dict['recharge_png'][0]).scale_simple(64,64,gtk.gdk.INTERP_BILINEAR))
    main_window_recharge_button=gtk.Button("Recharge")
    main_window_recharge_button.set_image(recharge_logo)
    main_window_recharge_button.set_image_position(gtk.POS_TOP)
    main_window_recharge_button.set_size_request(128,128)
    main_window_hbox1.pack_start(main_window_recharge_button,False,False,0)
    main_window_recharge_button.set_border_width(5)
    main_window_recharge_button.connect('clicked',GUI_usb_recharge)
    main_window_recharge_button.set_tooltip_text('Click here to recharge\nyour credit with scratch\ncard secret number')
    
    
    quit_logo=gtk.Image()
    quit_logo.set_from_pixbuf(gtk.gdk.pixbuf_new_from_file(files_dict['quit_png'][0]).scale_simple(64,64,gtk.gdk.INTERP_BILINEAR))
    main_window_quit_button=gtk.Button("Quit")
    main_window_quit_button.set_image(quit_logo)
    main_window_quit_button.set_image_position(gtk.POS_TOP)
    main_window_quit_button.set_size_request(128,128)
    main_window_hbox2.pack_start(main_window_quit_button,False,False,0)
    main_window_quit_button.set_border_width(5)
    main_window_quit_button.connect_object('clicked',gtk.Widget.destroy,main_window)
    main_window_quit_button.set_tooltip_text('Click here to Quit')
    button_sensitivity(False)
    main_window.show_all()    

if __name__=='__main__':
    number=5031 
    text=''
    app_name=[' Mobile Internet Manager ','eMIM']
    app_version='2.1.1'
    release_date='Sept ,29th 2011'
    home_path=path.expanduser('~')
    files_dict={
    'desktop'       :   ['/share/applications/emim.desktop','W0Rlc2t0b3AgRW50cnldCkVuY29kaW5nPVVURi04ClZlcnNpb249MS4wCkV4ZWM9L3Vzci9iaW4vZW1pbS5weQpJY29uPWVsb2dvLnBuZwpOYW1lPWV0aXNhbGF0IE1vYmlsZSBJbnRlcm5ldCBNYW5hZ2VyCkNvbW1lbnQ9ZXRpc2FsYXQgTW9iaWxlIEludGVybmV0IE1hbmFnZXIKVHlwZT1BcHBsaWNhdGlvbgpUZXJtaW5hbD1mYWxzZQpDYXRlZ29yaWVzPUFwcGxpY2F0aW9uO05ldHdvcmsK'],
    'eLOGO'         :   ['/share/icons/elogo.png','iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAZiS0dEAP4A/gD+6xjUggAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAl2cEFnAAAAYAAAAGAAeNvZ9AAAJp9JREFUeNrtvXmUX1d15/vZ59x7f/OvfjWpSvPsQbItTxjcCQFCEuNMEBZpYEE7nSZZnSZpkpAOnc6DBw/oIS8k6UCHpJPQpA1NAs1jWtg4xoDBxjaeZMu2bI3WLJVqrvrN956z3x/3/qqEkWzLLkndq7XXKtmqqt8d9vecPX73EVyUi3JRLspFuSgX5aJclItyUf5PE1nqCz65+24EybU6jeE4aQniUUBVMMaSiwquWCiPq2p8+fpXX+j3v+DysgB45Jl/5OTkdgZqW/qSZP6qTty+wblki6qvGZFrVH2AgCIoIAjWmE7i4kdRmc9FJVU19wU2v0/VP10u9nXz+fw8it+y/rUXWjfnRV4SAI/teZj/9pkbefsvf3FVozn7s47WW5Xmq5zGBVUj4AEFMaAGMCgKoggeNL21YBE1XgxNxR8MbG5e8fcYyT9ajAaeKOWq+zetubIzV5/1K4YvudC6Oidy1gA89NQXyedKucnpsTfFrv37iXav9nirAogHUciUnK56k92p930FFcCk+0J9+hhiUlxQREzHkpszhE+Fpviolei2KMzvWLdu08TY2FG2XfIzF1pvSyZnBcADT3wFY4LqfOPk7ySu+W+9NItOYhJ1dJMu3aROnDRJXAfvu6nCRbCSI7B5wiAisDkCkyewOYyEGEz6FAqoRbFAkn5pgCHEYOtGzF4jwXesLf5jPlfYPjy4bCJxzl+2+rUXWofnB4B7H/0ShXAomGnt/r86vvH+rtaDeucEM42jzDZP0uxOEycdvPN4TVgwQwAEWLEYY7ESEgZ58lGRfDhAMT9IMd9HPqwQ2QLGWER7pktP+QJRixC0rdjdgcndFQWlr+fDgQeuv+Km1sTsIYZray+0Ps8NAI/s+xq3/sNHefMv/Ovfmm8e/aOJ5p7iscn9zDZO0HVzeHGZjZdTLqjZ5SU1O+jCQhdd/L4xlkDy5MI+irkaldIA1cIwpWiQKCgghIDJzJbLviTdGZqfEQkeDmzu9kJY+WYYlnYqzt+w9ecutF6XFoAHdn0ea6NNxyZ3fuPZE49sOjb9OB3XSC+wVIFsb7OIEEmeYlSjUhqlVlpJpTBCLihjJcqA8IgoqpmX0RAruaNizOet5D/XVxrcOTZxsPVzP/GbF1q/LygvqL5H9n+R6za8ha/e/9EPPXPkng+Oze7Bm+Y5fSjtmS8NCCiQj2pUS8MMVlbQVxwlF9Sw5FD1QJz+bubsrdhJa0rfj4LBzwz0rf72letfOTXbHKdWWnahdX1aeUEAvvXkB4jCwqqHn7znzsMzOy53JkkjzKXP4RZEMSiCkTgFQwVRS0COcq6fvvIKBvvWUcmPEJpStnt89kwCqa9ohkG0PWcLX8pFlc/fete7jv76zX/P1ZfedKF1/kPyvFpUHefX//Mwv/Dqd/7hI8/c+9HZeF6UHEYXHeO5fbDMj6jNlKpADKJEtkg5v5LB6noGy6spRlVELOpN5mu6CIIh7wNT2Bna/KejoPSF5SPbjs7VD+u2zT95oXX/wgA8sP+fUSlXB7c/8+BtT+478MqWWtL43b3QR5fw0RZiVE5xFKkvUEGMoRT2MVRZz1BtI5XCMFbC7OcmTT9QDIG3UtgZBPlP53OFf6i7A8eG5fVcs+36C6f959Pi8Zk/4KG9dzDct+rtTx168tNPH2jnPDZNtkiyeP3FiUr6hwCiior0kuHTbCRBsuKFLnxeQdxiYqdyym+miZ1RSxSU6C+vYqR2CbXSCkIpLERPRkGxCKG3EjwQhfm/LpeHv67qJ2+47E3/6wHw2J53cMnALXb72Ee+8PjhvW/efUSwCA4BSTibHaDpMsR4i6jgRTOlpoCQlSnohadIunLlufhI5n/SkDa9Tg8Mn/mLgBxV+ksrGelfT62ygpwpIz7M9lAMgJWcM5K7MwrLnxyojH7n5MThxk/f+C/OOwDBmX5gg7XsnfpuXzeur3KuA+TTcN70EqwXD4DxBiFVvM8+L7BQJ0pjezIQtGddsl0jiKafT5WfqjsFVVEUUVkseeCJdZaTc/NM1Q9QLa1g+cClDJTXEtlCdhvBq7de6zdr3Hnt5Ezz60FQ/PDeA4d3Ts3u9zdse815A+CMWrxnx9sR2Bbz2N1PHD5S23einO4A47MazosHoLeuE/GoCOIN1pvMhitiNDNtWV3IC/i0gOdR1GTVVHFZMa8HXLoXDD6z9YKXFAQkQTGglkBy9BeXs3xgCwPl9USmkoawEiPqEY3UUDgW2uKtOVv6ROy7x1/3indcWADufOSVWGOvVzn0rZ1H5qr7x0oYFGd0wZ6/+Jukq141wIiSD2MqRU9fQSnkDFFkUxBQVMElQjeBZkdpdYV6W2nHQuIEVQtqUDEI2WLApzdSg6jBC6i4U7LEGFEhpMpAeTWjg5dSK68gMBHiQgSLaozBOivFx0Jb/lgh6v9SO252X3f9Pz2nAJzRBEU5S6m44lXzjSNVkWDxJRHAsmA2XoQoAVYd1ULC6JBluM9SLCSERrHZSuyVTxXJNoHi1eJ8QOKEVscxXxdm6zDb7FDvCIlaPCZTtGb7wSHSywckcy0hKtDVOmP1nUw1DjBc3cSKoS305UeRTA1enHVav865+K9dp/MLxajvY8D2Qwf3s2bthnMCwBkX8veeeiW53PIPNVuPfnD30QZPHw3S9B+T4Zb8qKIlNeM9f6oioJ6C7bByyLJy1FLJdwh8jPEBiuCN44fCyyw0ElUyD7DwMy9CrJZ2LMw0hclZz/S8p9FOgUp1nmbFopJeQ3xq0bJrGwyoR7xQCAcYHbiE0YFLKIaDoAafOXmrhsAUDwRh+c9KldrfdePm3Ku3vHXJATjjDkBjfNIGHFHgEAK8CMbr85h/BdKEyYvHq6EQJGxcYVizzGBNB9FuGo2InhJoynOuwWKouhiMAkIgjmrkqUTCaDVHs5tjel45Od1lui60XIQSgKTAarYyei46jboMajxNd5ID4zNMzh1l1dBWhvrWE5gi4tOIKvbNdUk3+WM/E/9kPij/garueubZ+/TyDT+2ZACYM/1AEawNEfFEIRhJowcR5UzmRzSNSDwG8ORwrB8JWT0iBCZOM+gsmVsIRc9CtAeWAuoITJtyocHqZV2u2hhw9caA9UMxlaCB0TjzCVk1NdtLZgHQ1JF7EuZaR9l15B52HfkOc+0DeElQSdemajfqJrNvbHamvnr3Q599i9N2/rFddywZAGfMpt717vXkosEbuvGRn267LmPTFu/NQhJ6uk2QAgTeKOKV0ZpwyRrImbl05WlqBFQEpOdAzyaa6mXDBsFkRiVB8ITGUSnEDFYNtb4cFoi7nsQpi31pSU2o9ODMdplRvHRotE8yM38SCMjnK1hrU4MoHk9n0Gv355rtesU7u/1n33Jt8wufuetlA3DGHdBpO+bnT97jPfVcJISBOyV2PzOeaW/AkzfCqhEhbxtZ/V8XXlzo9QSe71qnkzRUVelFQwGiEWBQ8eAhxDNU7HL52oSrLlFWjcTkbRfj05DUmQRvYnpBhfSiKgQVQyOeYt+Je9l19C5m2gdSHyVp3pFIq9jxc7/TSqb+51D/2itUlT1HHj03ABjtw8dRSzTngxByOQV1gD1jDqDiUvPjhf6q0F9NQB2qufRz6IJtNmozEM72cdPrqPGpn5Hs+1n46cWh0iGQNsPlDlestVy5MWKkv01gGukLq8leQRdyCFGTVmEFEtPmZH0XOw99myOTTxD7Zlb2sHh80PH117aTmS99/7F/eEetuc089sQ9Sw9ArbqJWuUSCWyZwCilfICKR8WdsRStmXkIMCzrg8gkoGGayWqQ2X8Al+WwqW1eMEOy6JZT8QtXVdIV36uMKh5MDLgsYjrVeaehsvGeHE1Ga222bhQuXW3oCzzWGVRNZpQSDA7pbdAMGFVDM66z7/gD7Dl6L/PdcRCXmj5VEhqb652Zjz8++7lflUKr8IOnvra0ABTzo+Si0SOeeG+Ip1owYDwqSdZi/FERTZ1vPvDUSppFHGCkgxCTBXhpBJRRVCSr96dv7hFJK62amYVF6+3T3SMxSJLC5gOMWharQ6f+n6a5BGC8p2xbbBhVrt4csXIwJiddRC1ODM4k6X3ULAQS6TUSEhocm32KnYe+w9T8AaCbZe1CTHOg66Y+Pj174uN4M3D/jq8uHQDTrYdAhqbFhCfBUC56IiPpKjyT5cioJ1EE+VwKBuKzneOzFQTi8xjNMllv032gPm26+wJohGAxGiE+wniDUY8hSU1Gz4EKLxBNSbZzhDQ2a1AtN9m63nL5Sks+dCRiSIzJgoIzJJfimG8d5elD93Bw8gm6tNLFgkO1U2x2Z9413znxp4k0B+5+/NazAuCMecBQbTObRv5l8oNdX3g2jo9TyieUQkMnNqhxp4+CSJOfXM5jbVZaEI/6ENUIpIuYDqJhZm8tUMFoZTIMSpNe5x8RY5uIxXmnRosjRqKrnJ8pI/VBr/OkpfBe9usyn8Ip5m1R0vqRollHLd1THQLjWTMakq8Iew47pudyaeRmktMGGYKAUVpMsG/8+zTjSdYPX0fBDgDgpCPtxN2SJDGRKbz3rgc/NfVTN7zrRQHwvF7wniduRoh+pauP/V0iM+zcZzg4UcDbRfPy3Af1OFYNOq5Zp1iJ8SZBfJCVsEE0QPEIfU1rlj8UhIPfiGTwrmJw2dG+6uUT3sfJ6mXv4NkTf05gyoXZ+jODTg6PdtzUFd1240ql+bpEJi/1vllMVZwgnKk8nj1jVk1dME0oahxgaDTL7D0MR2eVRHoNnOe+l88csAWJCdQyWNrAxuU3UM4tS82qgtVAA0q3RkHhvYhOve76W14uAD+HkfyWjtv+bezUyMEx4ckDIbGxWVvyORdTg+JYM9Jl2xowuBQA7dWOBLRIYJYfiuyK91dyr/jSlvUfbrQ7DQr58vM+6Inj2xkpXy1PTb53sJXsu7bdHf8Z52ff6HV2ozIvp+/S9To+WZVVF+N6AOvTld1I8uw9Khweh1hPlxplTX/tNaQcqKGWX8mmlTdSK65Ov60ChBpI/tZKOPDe2LWnXnPD84PwvIF4pTxAuTwwZk35iHiolpQwTJ7nE6kjtUYWopK0IJZGQaoRxq/5RiW88ZduuOxPPxvkaYjICyofYHT5NUhF9Ip1fzZRLV5y54plr39fX/SqN5TCbR8MdN1jooOqWRCwqLeU3KUmQaX3pQtNfi8GjyMXtrhklWX9iCGUeCHdkwVea5AlmfHCblNRZltj7Dl8LxPze7LGkKDSFqetW5px/U/DXG3gnke/+NIBWLvytXzle38zKdr/uGAp5aFc4Hnid83q8JJlyz7b+unqDGXdM4X8Jb8bM/2okS166YqPvKDiTyeXrvgYG4c+6pf1v2XffOf2j/SXfvyNxfDqPwzNht1oWRf8Qda5Ex8hGmU7sRcYuPSLNB+JbIMNKx3rlhtC8YgGp5S6s0Cil0nTC1kT5jsn2H30Xsbmn8Ebj9EAJZauNm5pd2c/NFAdCh54/EsvDYD+6F28cssoQVD4AYQaWEO1bE5r/xdFSFxa118so3mU4pyV4X+3+9Df71oxuDTUkJXDP8tPbYNa/qZDN1z21f/UV7zm5shc+hFh+aRqBPiFUDVdzRkAZ1g8oW2xYYWybjggQIltGvaaM7yvZg2mVneWfcfuYby+GycGlQCVlnTiyVtOTO5/5yuv+iUefPL0ecIL1gJq5WsxJngYzU8YcQxUPJE5Uy8gTa5cYrJIpVfPLBCaVXf1Vy77xpYNv8SqoRcXIbxYWbPyjYgIgSnuXzX4xg8Xoy1vDc3Ge9CKS3eBQ6Sb7cQsIz+dMlQpSouNyz3LBmNQQTV6Xkep4vGiNONZ9h55gInGHrxx6b4xnb7Ytf7vb//gf1w9Oz/10gAwOkDEll3G9D+DKtWip5Q/U7ycXtIlgvdCr+AsFDUKV9x2fPbbnes2fXlJlX+qbF33F6xb/m/cXGv/t0q5q98SmS1/DqNdT5q/LMRBZ8obNABvKESzbFytDORt2rc4IwK9GpcBIprxSfYdv5fZ1iHUKGnmEq93tD5RrfStvm/H/zx7AEYGb2DHgf+3YSl932hIIQwol0zGy/yRNwCEOFGcZ2EHGOk7lssNPTBYve6cKf9U+elrHqMQrjg5WnvrB/LhZe8RRo6ohixGQ+Hp9Q8kWY2plnNsXmGIwhbuDARYyahfaZYegzE021M8e+RBmt2TqFi8SUiY//FGa/bdN175Fh58/IcrqC8IwNpl72HTytcT2NrdSqUVoAxWIZDeFExv+Ki3sQ1JYnBZf1bUYk3/wXyw7ZDo4HkBAGDLuv+HqcZDzfHJr//Xgt3yG8aMHvGSDogY7QUIZJHSoqNNS9cR4j3LagkjAwbRzJk/13Qt0Df8gsNXEWZahzlwYjuxziMCqo5u0vyVe3d8/sfq8dGzAwCgXLiaMLxsJ9J3UDWmVkoohWlhavGRehR0S+IsXRcgWQEt8fWHLlv1nnq1vOm8AQBww6X/jfUrP8C39n3ttjDY/BvIsiNpyUGyiGiRbZfWplLGRlotjTG2y6qhkHIU01tkaR7RS+kkyymyEg2KSoKzMSfn9nFi8imgCxrgpLO81Z1522X5W+Tuhz5/dgBgJ7hq0+8fDW3lMYgo5JW+MojLeJu9+DqbBUu80u4oimTEhPbMd55Ywfrhd59XAACuXP8Rfvnqv2TPsa/elpPV7zEyOKc4VKO0oipJ1lfIwmVxgMegGO1QLcUM9C1mw5qRxrKo/7T3FAxe2xwdf4KZ5gHUdlF1uNj90p7W57eqxmcHwOjA9Xz3sZ/3htodSOStcQz2C9a4rAbjTwksFKdQb3m89Cjj2W64QHLJmn/F9RvfzcqBN90WmQ1/rZJzKl3SiOjUSZxFFaJpth9Ji+GaJRCXlQEV06uWntGZp8W9djLF4ZM76ep8NmOSrPS03zY9f+jsABip/haV4gqioHyvoXzEqKFWVgr5GNQv1PV7D+Uw1BsudcQozp8dketcyNZ1n2R8/pFuPtryR4Gs+z6SNZY0zMoLve5YWt5OmRUWwdNXVEpRytpbCK11oeX/I5LuJosaZXr+OBOzBzJua0KindevWXF1/4NPfeXFAyAiVMprWDH05oNGBh8QH1LMeaoVh0gC2AyEXgVUaLY9se+xEuDcs6lfWNb1v5dmY89E0V7+SaND3XQXLE7ZLDraJEuyUjufDx3FqOeo0xBcJaVanl5haYNHscQ0OT65m04yB+JJXLx2vj43PN+ov3gAAC5f+Qm27/3NJLSD96ERgTgG+gyBcVkkkcVBSrb9lHY3o3FJrv+1Vx3k0Mn/ckEBGB6+nuHqjfQVtt1hzWC6C9CMOb3IWZWsr9FTrxVHLiSdgdMet+L52IG66BONZ759gun60aycwYiIXC+ZeT6rrnh/+VWohreLlA+iwkDZUIr8wtYU7V3Q006E+Va6G6Kg76oT07eXEte6oAAAdLTL4dlbZ43UviBaWQig09rQ4jQmnNo3BrHZ1I6aBdNz5ppYj7eaJmmJbzM5dwRHB/AmThoFl+nirADIhysZ7vunByy1+0Ut+chRqxgMLmU198yQBnhvqTcErx7nu7WZ+d1Bo3XyQuufqzb8EcX8KGEYfk/Eji0mlEFKBpaebc/KzihOLV23+HeV3tT/GWQBGF1g5801xunE8yA+HeX1/uwBuHbzX3F07M9iS+1ONOdCgaGaEkqSUTdS1NP8wDLfDEjUEvsZpuae1rnm/gutfwDKhTWUCmvmQ9vfScPOlKl9Smv/lBBT6LiARiftX8uC89XnZXWcwjoCgW4yT7M7jQCBjbAmPHsAAMrFywjDkbtF+g6Ks9QqXcp5n1IWs0uKplXCetvQjgPUdMv5Yqu/UIoutO4BKOSX01e6btx52ZFmvmah0U6muNQaGbwE1DuWRqfHVO0VGc9MKtMfKl2kRLSEDq1OA0XmbBDtERO8NAAK+TWsHn3fISsjD4paCmFCf9Vi1GcrwyLSRQXaiaHZshiRtdC5LOUVXXgRyiyv/YuuS2Q2bYv3HHCPNNb7m8UTMDHTpZuEaYa84AM8Z4hCezBk/+lxljxxEmMkOCk22GvDl7gDCoUyuw/+tgvNsttFcs4iDNaEMOitCr+QnyTOMzvviH3Hdjut2vTs0xda9wDUm4fZefj3RgJbegWaUdoRjKY2O2VapOqZqwsnJ11G+O1xMrMO8xktUGbWFsyUZEBYApu798pLXj020D/40gDYMPIHlEpryeeXfw+pPKPeUi07SgUy9qHLukngRZipe2KHCSi86sjEExda9wA028dodY6POp0YSPOYXv2nF0ikk6BdJxw44Zlv5xeGTLx40lmSdFWfTnr5A5m5ymhi5ILKbDFXvXX7M7e5q9b91EsDAGDZ0OX8f/d/4qDRyl1giQLo71OCBeIVC+ZovqW0Oo52fKT2ibvh2OwfX1DlP3not5huPEo3mXydysxQGrWlNZ5E0lZq4A2qEYfGlWOzlsSEZ3WPHudJsTjxKI5QchQLhTtuvOLN3x8cWqwKvyQAVve/j5/Z9mOEYf9dSKlrpMtQTckFaRfJi2ZEJ0MnCZma7+Ls/DWf/pfvGp6Y3XFBAei05lhR/fnRxI29VbUN9MZg4yx2c3g1HBwP2XvckHiDPYtpIEhp+qbHLzJpVlzJ1WYrhfKnPnf3b3evXvvGlwcAQLm4hULusu1GBveKKtW80lfqLPSLs2FTHMLkXELHtVa1O7Nrm63pC6b83Uf/HWuXvylsdve92+vEK1KOUsqMAI9Rg3MFnj0ZsOuI0HZCoA7rz+4+igVxGOkSeqhGBdYsX/a5m155y91bNl39Q7/7kgHIBSP83mf+89GA8h3iSuStMlgzWMmGgXyaDag45pqeRjcejF38Y5P13ew/8UfnXfk79v4Fk4072X/i0z/f1Wd/F+azo9V6swY5WnGVZ44FPH3U0HIhQaZMf5aVXJXUkRunVCNl0+rhfWtGV/z5PU98NL5m9a8vDQBb1n6UD//yTUThsjtEyg0hZqhqyYeaxRQCmqR1odgw3WjidP51r1n/V4Hx57cwNzF7L08d/U3iTuXGenvXv0/8WDmt+aSkXC85pupFntqfsH9MaWuY8VZDPAFuYTYafojNzQ/PMS82bDyinv4yXL5hMNmwauWn/uK2v901Orj8R54teBHPf0aplq4Booeb8f7HvR//J5XIUy3DzIzHqmSVQyFxlqnZDp3+6W17Tn5hrYrsO1/K/96Tr2Gq9Y38+mU3/2qr++zvI5PrJTuFJaWU5Dk2HnB4PKHREbwYTNY79pKFqLp42FQqmY/LTo3pUeJFHHjIIYwMxmxYWfGDhaH/MlK67OMfetsoW1d+bGkBUObYe/C/Ty8b+PGvd3Xin4htMlhTTsxqOsGCzVrGwtxcQKNTX1mI9tzQbI/tU92HyMZzpvi5eC9fv3sTxWjz6vHpez/ajff8skqzgHSz0kCe8Rnh8AnHdEOIJc1YTz96ZdKKqWjWglwkPXnIytIJgfdUirB2mWXlYJVitPoLtfy1H200ZxrbNv71aZ/zZduC+558E0by17TcQ3eqnBya7YZsf0aYb4eZM0rLupEqV24MWDu66o7h3C++ueX2t67b9LfnRPk79r4PI0PRfOubN8d+4v3Oj1+HnRFvlI4LmZ4JOHYSJuYMXU2ZC4sN+h9VSfozyfKE3oEhvf6HIMQUooTl/XlWLetSLYRxwWz6Yq14wx8mfv7AlRv+Kptd/lF5WTsAoFIaJDQju7pzux5OmHhDOUoYKFnm2ikJSlTwxpN4mJzxDA9OXTfh7r7SafPBpVb87qO/R7mw3oxNPXz9bOsHv5u4AzeLTPepFdquyOS05/i4Z2rW0lFS2y6SzSu4tFJ5Gj0pmg7y9ZI0TYcDrUIu6jLU71kxZOkvdShofz1nl/9FLn/tv59sPDT/6iu+C/zXMz7zS3bCPbly/afYf/Ivm6Fd9k00T6gwUBWM6TW4s8qJgZl5pd3uDHud+7V1lfcE2/ctTZN+fPohVFXacXvzsye+9KG51n1fTvTJt3kz39dIShyeKvH4fmXHfsfxaUtbU7ttNK1hiTi8yEIP+7nSqw8ZLxjvCLRJJV9n7Yjn6s0hW9Y6llUsRbN2byHc8s83DfzqBweioUz5zy9LEo5s3/92ILh2tv7wHXBseKYd8fBupdEFNMyclCfAsXVNxIZlldlSuPmdrc7Y1wf7r2fr2k++pPs+few/YKq/KN2xP97Qak+9s6XH/1mHYxucdqXdjBifMYzNJMw2HbGmnE3Bp0rXxaaJNz6lpSgY7Q3Dnnp0jmJIiExCtagM9QtDNUOp4AmwBDowXjCrvlwqbP3zLRs+/vSJub/V5X2//qLe4WWbIAD168lHVz5t5dCjsZ68KZ939BcDWh2Hk5Rz6YzSERib8qwYmOszHPn9QnT1g/X6+Fl3aZ7a/9vko2X5xvyRbc3p99zcZfydsZ/f0OgmMl1XxmcCpmaVduzwGVfJCNkhf7pQLiGbUTOa8oG0N8OcFRWNOixCseAYqMJQX0R/OSEXOEQFK32dwKz6ZmhW/sdqeMWDXhqJnOUxkkuyA9r6APdv/zVKhbXvbXR3/IkLpjl4osgzzypdY1l4J4RIErZuUlbVQs3r+s9WC9e+N/EzE9dc+tnnvcehXZ9jdd/bZcf8rww2k7FXOI1vcb5+c7c7Xp1udWRsDmZmEupNiBdCSIPxNh0sFL/Qr1gsFQcsMKYlwWfjTyGWYqj0l7sM1Rz9VUMhzDhOahHpnw3s6ocKuaFPVQorbk/8/NwVqz71knS3ZBnR9l2/Athtc937vpnIkeHpVsRjO0PqicFbnw3aKU6Ugarj2o1C1RhvzYov5vOr/2Sw/AtPdpKjzctWf+iUa74PK6VqO94zosH0DXE8d13i669t+ZnLZltJYXLWMDnbpd5S4iRN/1UWe1omU3Ya82s2qmSzWo1mzLaUzYbxFCNHfwn6q5aBilCKPJEkmbnKOSPlE9b23x6YNbeWylc/tnXlB+rPTv8lGwZeui9bMgB2H/lDrBnKH5383FcSf+Cmjo3ZsSvH8ekUAJuNgCYmrRZeMhKyaWVCYLsY6Z+yZujBMMgdC21uTJ3Oq+rmVnfCecwmr3ppN64vm2137VTdMTXbZa7l6DiDI8zGJvxzXihjZ4ueclJKetiIz2r5oSSUcgm1klKrCrWypZhTAtvJTlSpYOlrWzu0V7X033O2/6vL+m48oNqJ16/8t0uityXxAQAqXXY9+4l2rXr5nZ5jN4V06a8qJ6f9Qu/UE2A0QVQ5NJYgQcKqEUsumB6wbvINscvhxSqquMRIqwtzLWVq3jFbd9Q7ELsQ1Vx6epY4gl4DSJ7bzV3MXXtH11jpkrNdSgVLpRJRq0Jf0VAKHQEJqglIiPHVxJr+Q2KG/zEXLb+jUhl98NLRPzkx3TzGQGnlUqnsOQtmCeT+nb+IMflrWvGDd3qmhibqAdt3W1ouS3A0QCQ7p0EtVjy1YkR/n6OQ84hGdB00Oo5m29NsJ3QSIclOYBE8VnrDeAa8yQgAizycdOWnDAaDJ0DIR0KlINTKnloFKnlNj+AxCekhfiGilcTI4HFr+r4X0P/lamHz9kKp/6B3sdu0/MNLqvRTZcl2AEC+MExglu1qxXseFp1+QyWvFPJCo5Gd15PNj4EFgQTDeCNmopmy79B44XwfEBCbdZTSExslG7r2Pc6OUYy6xSl5TbAIUaAUCo5qOaG/YqkWAophh9B0M16PQQkRHYwNhT1GyveGUe07+XDzQ+X8jx+GdnfzivNzguKSlyW/+H1Y2X/zH3eSh/5NjOOJgxGHTspZ3WnxTFHJpuhTw+JFFuy39k5kVyUyUAiFSgn6K46+sqeYE3JWsh0TZ+lgUdHynDHFA4EM3Z8LVt5jTenuDYOfPPE/7tzqf/Ntu8+L0k+VJd0BAKsH34A1hW/GrvJbVibztYrj2EkhOYuauohfYJV57VHcFzk71ntygadcUPrKhlpFqRSVfCgZVTLJhjAsaMEZRseN6duFFL9pzfB3oqj/mUp+zZzik0tXvh/4m/Ou+HMGQD5YjaFve5tn9xkmtlYLjihIS9IvWnyYtvSyATsl/XcG8qGnWlIGKkJ/Bcp5R2Q6GFHEB3i14ANEyi0rQ8cDM/BgENbuE43urOS3HHeuPrd1w/lvBj2fLDkApXAzyyrvmNl57JEnEm+2FsKIfC6m0VBefJaYspDTMFHpK3lqfelQSDHnCE3qYNUL4nMgeSdSnghN+TGR4gNBUL2/EG7aVTJvP7xx7SsdcBb3Pr9yTp7qu9uvIxcO/Grb7fibWJx98lnhwIQgks2NSZKe10B21Iyk/BmjnsB4CrmEWkUY7AvoK3qKOYfFZ/O6gkgIWpoXBnYbW3hETPnuQrTuyVJ+cN/6FT/fHJt6iDWDv3OhdfuiZMl3AEAh3IrCdpFDjcAcqVYLZYzEeEnnCNJeMaRO1GFVyIfCQEUY6rMMVKAQeax0EZKUFEXJiVQnrKk+Zmzxu9b03ReZjU9s2/ix6fG5e3W07yeyu//HC63Ts5JzAkB/dTPem7GT9R2HvJorykVLzsS0vMUbUB+Ch8g4aiVhuN8xXPOU8kooDkTxWFSLiZXKYSvDj1o78K0oLH+vVviJvRuW/0Znfv5JqtUrgT+90Dp8WXJOAGglB9g88oGTk83bd8eJuaJUVEo56NYFS5dioNQqhuFBob9iKQQJAR3QHMYv64gpP4vJ3xvYoW/mg9EHB/quOH7/4/+p8/afHgP+HvhXF1pvSybnaAes4Xs7b3KV4prvCLk358IGq0aF/HSLajlgqCpUCgnWdlC1WO1zost2Wzv4XWuG7igE6x4cHXzNWDee8WtHz/+R8udTzllo8MBTb8KYwk824/u+ojJRSQhwGmLFYaULvqRGag0rxe9GdsNthuLX1i/7zPG/+nKff/+vzV1ovZw3OSc7ACAMCkDuGaFyQnWqEuAxEiNUEqurn7VB7as5u+FrtdLa7ZOzB+uv2Pp3wGdf5l3/95NzBkC5eCmXrPrg8e8+/vqvqtR/z2CnQzP4cBAOfikym24frb3maNdN+A2jv3GhdXBB5ZxmJ3c/+iaisDYcWPcWa9zjUbD50Ss2fqjtdR5rqktyj8OHD9NutwvOuauq1eoOa21rZGTkvChvKeR/zfTwLCQDwDjnStVqtWGt9f87AXBRLspFuSgX5aJclIvyf6b8/3Y3Pi83ojrkAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDExLTA0LTI2VDAyOjI2OjMyKzAyOjAwhaNtQgAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxMS0wNC0yNlQwMjoxMDo0MSswMjowMFsv0DwAAAAASUVORK5CYII='],
    'credit_png'    :   ['/share/icons/emim/credit.png','iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfbBQ4TFxCaRjfUAAAgAElEQVR42u19eXhb1Zn370i2JduSlzibE8U4sR1nIUAIhJgsQCjQIWWfPJ0O06Fl6fN1ofDRKfnaKZCEthCgZRkG5hva6TeEGZYAZS8dIGQB7AAhhKx27MSJb1Zjy4vkRbb1fn/oLuece65sS7IMhJvHT2RJvst5f+/+nvdlRIThHuWlU8U3GLNe8+djTPodYGAgIoABUF3aeF/+3On7ABgzzqlfz+lvGfdGvOfm79Pp2sa5hnKP8d6H83OJ987dM1M8AwPqDxwYNi1dSMVBFLsnmfj8QzDjq6RYOGYSkplfVACMOV3eIj5jzJkopAKpmiCkf5k5XZvsIHQkpA083DOS4vpMsYbC7wbQmfM1hnhkJEx0xoyV0q/vsKgYCvcQR0g1wBzvwVwIEgFmXooJxCSn1WIm6wufK6UVs7OuTZIaoJafi2Dds9NzmRKQ1GtgMlGcNR9xCSDfPMnoJrtE4JEPZuMcRy4yuZBJHEfWQpGaa3jCkMFyTHHfZLA+OXMTg/BsjD8RY4K0MCQiL/GE7wviXCL8IM8e/+bSqQJIJZ7tD2YSy3xoC/2k4iJZLCs4xZEzeW5jzFnsyipKJgApJBU5qB79ujynq2wrUtpMDl+SpYC8NjIgiNKtAjhx5mTsGFpCFnnc39TW18PlcoH0BTReu1wuDAwMWFzFndzlciEajZqvTcPUPC8DmMHtxnUJc+bOw6LFS1C14BxMnDgRXq8nRjgAXd3daGrSsGXLFry/eTP27twR38BjDPUH9mNgYABEhIyMDPM1EcHtdiOqv85wu01AGM/kdrsxMBDFjIpyZztBUG/MlDrEyKb60msDcMQ09apugMVuzo5uxkt+UlvGstaIb0hYBprplahEOyNkZGbib79zLa65+ipMm1oKr9cTA5rEaVMmT8KZZ5yO8887D88+9xxee/EFhV62bra8dCpqG+rt2tBYG/21oIZ0whHFMd6Yyk4gHQMkPiQlrv+TkAAkij0TCKS0BczPWHz3izH7ipguniyvJREofs/6ftn0Gbjjzjsxb95cuF1uzoiOrRxjFlHcbjdycnMw78wzcOrsWbjkkktw98qVOHHsqKi+rIe0GeOD35thCZDlHZEDeOU1oyHzxgjbAGTXOSQbTxw7W5+Ro8HDOK5g3ALadamlRkjnQtv39D+/4OJv4rF/exznzD8LmRkZcLlihpqLPz9noxB3zx6PBxcuPR//9w9P4KwFVTohOK7T12BGebldDZP4cGS4p4bVo//OZJ1PArbEdVa5hEkQPjkAMKYWO8b7Kos1TiAnJiZ5oUGOAR/mYA0zybBavPQbWLXqLpwyJWCKXJKI7nIxjl7WuWPXiX1xxvQKrL57NWaffoYjt9lpJEoJ8Z6t30l1/0yxbirJ6RRzSZsKUEXWzJUgKUKnQDYGiepJv8uEn15WLkT9+OsUjRuPFStux/hxY01RzyvxrMysAIApAALcKTUATb29EU1WOaUlJbjjzjvxvWv/Hj3d3bbo4syKCtQ3HpC8Yes5ePFvCVA9eKYjzfIgxPiKKXWY3TOx1C6lGQAk3QQpdBVTRP3kAIphzLqY5c6TXQUY1zCJrgomcdf/xR13omxaKcdxsY+yMjMNwi9wAoDHk1UTA0Kvxp/+tDmzcevPV+De1SuVrmH51Knm6911dVL4wwAD6fS2nglykIg5eVWxX/gQNcHZoB55N9Dmj4siiQEglQ8vR+r07wjhGrK8i8ppZYNEyyBE785ZuBiLFlYJkoMIyMrKCgCo0n8cAaC/X+3xeKp7IxHNAjXDpd+8BP/+2L+itaVZjM8b+kL/f1bldJMge+v3CYYy/z+Rwsjl7CDiYyr6OpO8liwJ6gNgiSSDKqZOEwMhPDWGcjrunmsb6sFcLtMIY4xh+rQytWQZJNnTcLDRFB19ff1kWPiZMc6vArBc/z8Q5+40ANUA1gGIgYBz6f7j/63FA7/9tTqM7Biyjt3fvgP70dffDxdjcLndoGgUM8orHIJafMBqCOuqxyXSIgFIGaPmOUGdLBF1IYlhYwCV5RX276oiYKqFtrleAnWmcNwfGOTxDLBoADQQaeDuZ+4Zp0uPwIt3psgJWPxhMM6e+n2co0SOEUHBhhpuaH5kA0GyCGZSkkIivP7PKZkxvazM5iHaQERqE/yCiy9B5YyZKCsrw9VXXmb7PDMzI6CL/AVDID4PggCAgNvtRjQahS5OUFFRjoceexw7d+7Erp07Ub1pY3xDViIkA8PMiukAEfY21McFNVFyUb4RUwHlpVNF0STnz5UBHAV4nJJIzJ5rMETwhEmTsHDJ+Vi4cCFmzaxEUVERfLk5cLncVhKGR3hGhiH6lw8DAODUwLre3ohmnJv0VPHAwAC6uroRDAaxe89ebNy4EdWbN6P5xDG7OpBz+UNdckU9Rby/NTyRtIaCLa+QnP1YUsW3JWuY03NC0YiOluXX/gPOOWcB5p15BiZMGK+7x8weP7D7xAHuZzjHFOuHNOv2Ys/gdrng9/uQ5/ehpGQKLrnoGzjR3IxPtn2KLVu2YN1/PSUYcJbVj6HZNarweZIWf2olQDxOV8X7nSpbnAwfnduvu/5GnLdkMSZPLoYnK0uwpOOFwY3vud3u5ZwEGO5hSIB1cliWBJXEu61RRCJ9OHL0KNZv2Ii1f/oPNB8/PqgEEL0WEg1LSdo6JY8SMQJZ6krCyO7GxAsCMbUPDwDzz12Ey6+8AksWL8K4sWOVyRHGXNavZgCSweVygTHGB3oMt68qAQBUA6jRgdDU19dnBomiREqQGwEeI0H2eUsrNr//AV568UVsrakeXKrGAwgfB2BWXsG4p0RUQGoAMMSQ72BqpbCoCLf+7J+w9ILzUVQ0hjP8mWAUGSlgXceronr8e6rPh3oYsYEa/X9N/qynt1cDyZKATAlhMMLnLa14+5138dAD9yHU0ZGY0a3yeDj6jRoAhEygJOINxColgkHEzEz8r5tvwVVXXo7iiRMtvelymQjnRWSmFdFzInAyRI8HBM3hvRgYeno1nvhGFBIAotHYsx89egwv/vllPPHYoxjo71cYxg4GpPyZJDEYY9iXVhWgsPxtCDVTwYCycIExzDrtdNx+++2YN2+uXjhhxRRcXMQsK8uR6KkmdiKgsL3u7u7WeEKZGgxAf38/Ptm2HfevWRMrPHGobBrUk5LUZ9olgEVcPkEkItjxIQD88JZb8Z2/+zbGji3iiieZIPoz1fH70SL6cMBQEwNCj2b6SFIgrKU1iP9++hn8+6P/MmithHNp+WgBYBi6XgZBRmYmVt+zBt+69Jtwu91gLimQBAaPJyvAGXFOCZwv4iHbDTUAmrq6ujWb18Bi0uC1N97EnStuV6tRwO4FkNrbSisAbBVAThEt6b1p0ytx16pVOOvMuabkMPL8BMCTlTWeI/g0DgBfdMIPAwgQEmhEwIcfb8XKO++EJhNRdp/jMN7oA2CQoNG06ZX43e9/j+kV5eD9asZcAAgejycAYB7nuk3Tif9lPnggVAOo7uru1szYTpTMJaqrr8ft//Rz7K+rHbp3wNlVo6MChqjzT5lWht8//BBmTK+AXIvHAIP4VQCu4JI2WfjqHEaW8VX9f627u7snKsQNgL21dbj1lp/iyKFDHJElT4CrEk5WAiSxMUTyQxkfvmeCrzqppARr7r8flRUVtnIor8cT8Hg8fLz+PJ3zv0rEB2fPGM+5IDs7OwCuXJIxYEZlBe67/wFMDAS4deY8KCMARQ6h93RIgIqp0+JvnJRE/2N/+CPOW7xQiNszxniur/oSGXnJHs2cNNgAoLqrq0vjQQAAm9+vwY9uvH7wjalJegEJSYAhgUan9u2/ugNLFp0rxrdF4hscUXUSEB8AxgGYC+By47lzcnICJs/o63tu1Xzc8vMVDjYWUyeVEjgSVwFMtYtX/Molyy7D1VddIYUtCV6R+CcL4eOphKrcnJwAX1rvYi4s/9urcdGyb4lrzqAMAydaEJJ4Wbgoz+1gAHDDjdfDl+sTkOH1er8mvhMIcnMDVt0fwe/34aabbhLXnByIndbdwbZ9adJeegC3/+oOzJoxQ7XbZzjlWScTCKp4l9egcWVFGX50623q+Irqdfq8ABK3dcNy66aUTsWFFy4FUZQjPUN2dnYi5VknCwgWAFiQm5MTMIxBoxZq2aV/g7ETJjh3YiFKuEgkCRtAMgqZVTN17T9eh0kTJ4BP2Gdne5VI//qwS0afLzfApw0mTy7G92/8gVoKGDuH0m4EkkIiAMjx+XD+eYulG6KvRX8CqoDf8rDw3CpkZGaqpQDRKBiBKt1DwLLLr0Rx8USzQwZjX4v+RFSBz5cbYPruYzBgSmAyLll22eCMmD4jUG2NXnTxRXAbe+8Bo5EDV2CZtkPjfr5sqmAKgCkEq5Q8I8ONS5cti28MJnAkvjVMUZRYNHYsSk8p4XLfjEd2YIS5v5kjeA9H+EASakeuBEpHHYKwVmR0BQEwbWopJkyajONHDouEYKMBAMkYnHvW2SgpnYqCgnyhllmPco0k8Q0ibXMAwIIkrs9n8WQAjCQYAgACvtzcQGdnSAOLgaCoaAzOW3ohtKZD+GDjBpERE5QEGSm5XQKmlpWjpKQE2V6vVNA+YuJfzrebAOjt7e0x0KkXlmhJXKO6p7d3HQAjgikDYCRyGNaaMWgxWUrwZGWhuLgY/X19ihzBaDSJ4tKUFRUVmDhxAuxbWEdE/BvJFKNsu6m3N6Jx25TMsHOqAA4APT09GsA0xsz09RQdfKn2bsw10wu/zVrJ4uKJsapoWyeR0bABmJWmLCwsQF5eXkxnGfv9R07km7t3I5GIJnSJ5dPkSRpIvJ3F9x+KEhAr+mRadraXtxFS7uKaewr1OotJkyYhMzPL2TNLCwAUqPP5fMjNzeX65MGq9Rsh4lvdPBi3195YC5ZCInA9jHjvhwHd3T1adraX3/GRUhCYm2t1lzonJwd5eX6RGCzdcQBjITjU5eTkICc7W9ipMgJHkyH6e3p7NasFG/Eup9maLWX3we09pCiJvTAZ0N3Tw6ukplQ+sNFMygi3ez1Z8Hg8UkfWdMcBoG9K4jghJycH2dnZ4n691ONAA6D19PRqfF1RjNhRRKOx/4feXnUYAs/s0EGI9EWsXn/6Guj7APbpP90pkz6c/ifEGlBmZmQoE3BpBYDc1oSI9L1r4DaCpBwBAQABr9djxcphxMGNrl/MkgDR1Fw/BqwYsV2M4eln1mFvbZ3FhFEz6dXE/aRS+JjP1dPTi+7unlQFAhMNBdt/7+zsRDgcNoNANDKqwMwneL0eq2hO2dKVpUoA2Fr77tq5Azdcdx1aWoM6I5qeT8qjj1LLQXR0dKC9o136QroBoGhnEg6H0RXusu6Ykr43JwlgFlDEikvs3Z1Zim1PuTurOyMD7cEg2ts7hEhcV3d3ix6RbAYQTVbVAdDMdnL6tT9vacGxY8dT9myuVLHGkSNHcPTYMTP+w1LuAChBsNzr9ejSwDKKRrazCmHmrFmYMGkyGg8eFLay6RdOhRQwVYmxYcZQsbV7a/HZ9u3O0ji9bqBlBB7YfwD9/QNC2bf+kl+QQIpBYAZjvF7PIZi7dHuODLrXbrhxAE4Pf/fa7+Cqyy9DT2+vtCWS8Z5KQGewQALcXwOgpjMU0kzjkwhtbe2orv4A9Xv3JBsATBIAig0hmtYEl9uNSKQPWVmZSjSnOEjCh2ObAJgA8Hq9RyAmcALJ8Txsja18fh98vlwbF3Z1dR/JycneohPfNcy4gM2d5LfF76vfjx3bttp7JBJGKRfABYRqNm/CocZG3HTj9ZgwfrxpF4W7urTcnJyRTMsaBD4HVn5ABkDSeQgSgM8HaXivKEascLhLy83NGW5wSAhyhUJhje+u1t3Tg6fWrlW0rZeaSqZPBdj7Ah1pOoS6ffUYP24cXHxP/JFRAyp7pkT/gQS4QDLgyvZ6A+amTq4a2laVzSyDUAKBBuekkW3vYCgU1gR3m4AtH36EzevfdpizMBq9gh3Gqbzx+hs4d8E5YHpvfn2tRkoNDEa8VLmdyMnJNogShNUEImx1AxFnCJEIAr4NbTwANBmczx8HDzXhkYceMgNwxPVM0lNFiQvxlLWI0c+TkZmJdS+9hGmlpWYSAwByc3IS7dc32odcFBLk3gsbr8PhLquXIMckxu7f3NzcQDwAdIZCmkBOnSytwSBWrlyNTe+8NUiHtTS2ihVdQBJ6+vb39aG6uiYGADE7x1vHX6bCUNmIVAIgN9e0c0ww8A2ew+GwBjCN51eSdgaDibzc1t6OBx96OEZ8QE181eymdABASPpINsG6Z5/FhUuXYuLE8ebbujFYjfSUh43kUaj/zFFICAMMHxm/h8Lhfib5bEJwRwqeGy8aDhzAww8/gk1vvyUwnX3dR8MNhNSVU0LfgX11ePmVV/GDm64XuljpIKiBVfHyVagQ5sFsgKHUsOp9ubmN4XCXJuRJSBidKLTbDba14a9vvY1/fehBdLS12YwvWx9lNpT5syNlAwyik5554QXMmjHDvHEGUxd+1fcGHuXcukbLwAtptoEYOhp6IxE8/8KLeO2VV7Bnx2c222rQWcWjMjtYHnTA714lwpP/udZKEOl3qbs4pr+LL1/Z9lCOYgBnA7iaM3yX+3y+Kr/fFzDCuvzhycpCXl5ejPhmNJWU01bjemNpAYDTYChhhCvwxst/xnPPv2gb53aSgIAPWfP9iqv8fl/AHJChuw4EwmXLLsVDjz0O1SwF22vGjdpNIh+Q3Ng4BsHVE/ap6ff64H33YPN7H3DRs9j3QmEBBBsB7AcQOUmAsNjv95f6/b4Mc7C1TsXzlyzCg48+ZjETz2z8a4Jj59U0qgBZMDDlPrXfrF6N2n310qMarpHVl1/XlV9VaaACwgK/zxeQ13HJ4oVYccddoqK32ofYCxScJPKIA0AKRdpGt+s3deywhltv/gn21TeY0W3jdnV1UMOBoPokAMECWSUQ5wq6XC5cdeXluPb71yubQjA+FpNkHCB5APDUlCeBcnd8pKkJP/zBD7Bj527hnhkDwuHw8VAovBXASycJEGQjsSrP7w/wktTjycL3v3cd5p2zwBoxq6+lYFONyuZQUx/BYTgEN9CQKw1sbW7Gpk2b0dsbsZkTgM0uOBmAoO4OohO2sLAA1994o527FG43S3s6WJGWFO/R+pAxhu/ecBOWL78GgcmTLOvV+BaXVtZBoPlyc43kifH/V7WNnKESmvx+X1NHZ6dmNopyuXD2WWdi+d9/F+v+e624rinKCKauWbTstuifV8ychZ+vWIH5Z50pqAUj8RFrE8tFuSDaPD4rifJF7hae7CG4xe0dHZrBJESEA40H8e1rrkZ/X5997gLXQziRQFDSQ6P4EaiC3idg3oIqrFy1EiWBybA63ljuQ0xDRC2ic1vKjJBpKBzWQNB8vtwmfLHmBYyEKogAiOTn5fV1dHQeNyTq1NJTcN2NN+GP//a4ndNt++HSpAIMwvM3RNzQyIqZs7D67tWYXFzMkZ2ECha/3ycTT4OeE4cUYQ6FwhoYNC6cnM6JIekEQawaGHScgen5f8KSJUvwx8cfs0deaVSGR5M40xgQjL0sjwd33HUXJhVPBABEKQoXc4GBweePSzwNQJPPl2tm1sw8OWdsxgGDCgBfJjAEAJwOoC4/L6+xvaOjxVjTyunlWLz0Qmxe/45oiCUZCEquHsCh/vpXq1bjtFNn24wUneP5rdRKAMBKrVb7fb7qzs6QBsS6Z/KDmIiZcQSNN0B9IiiquMX9oh9ZiDXKnobY1vAW0ndgZ2Vm4aqrr4kBQDa+MRoA4CJSVut3YNZpZ+CC88+zuQYc8eNlAeXUakD/21iRpCnqouaMAXOLIueVhMNhjSjmOuqVu4kSnwdkuiSJ1SOIaDuvOivKy0T7S1cPyQAhuapgbo69wZU33HSTWS5tfKoHOYabAua5Fz5frl4sSaZakV1SZrZeZ0nVyXEHX6yZLrWSDaACQEV+fl6gvb3DVIFFYwpxxTXLcejQQWz7aEtKtt6lpkeQPjq2ZOo0nDprJviIFiXXI1A2jDTB7CBCdCCKjIwMIcRMqdsTpAGoDoVC63w+X7rawwhSAAymQez1ejE5MBlEhG0fbknJhRICgNAaVrdGl178TZSccgqKioqESdk69yfTI9AKlPh8TWbJtJ5K/aBmCz54/wOcPf9szDl1NjdpFCnsERGrYyB9hjDnvYxUkEpZNkdEKCoai97e3rhRwREPBQvun14PeNrpp6OyshIufQAUrKKHVDSJEnvnwUpFFxQU4Om1/4nbbv4JXn3tDVPyxBorpAYBRocO45k7O0NaR2dnWkLW1l7b2LXHjx+HyZMDUtv4UfECONePMQQCARQVjTEBwqwt06koAg0AqARQ6ff5PguFwmEj+jWpeCL8/jx0drSbI2VJqtFPAQLM3gf8+Ts7QxqBtDy/f8T6BIljmQnjx41DhjsjJdyfsAQQAhH6kZfnh8/nM0W/ECNItXVsjJoDQ9GYMXjsiSdQPmMm+o1RrEnGx9V4Z1I0M7YIDAwdnZ0j1yKGm87KGENefh7yC/JsBnCia51cKJjbMJmTnY2cnGwrK0VILRfykoSwN4qoef45s2fhiT88gfaODgwMDMDtdqfSEARftWNO8DalTOz/9o4OLT8vL+V7IMk2m4GXvEi6EYsrcfYX1cFANIroQHSEOsMAsGrx5/j9voAMrsKCApxSMgVut9tGuFQCgfNsTM8jSlHk5+WN4H4H6zkikUhsW7pcg5HWiiCb2GGI9EbQG4kgSlFEiVIt/nk1UAWjsNKB0BbHplIUS9figK4Tf8RnIRARurq7EQqFrCpheXxf+iKBgkOOUDgMj9fDOQYjIgaE4JDfb4WJea5kKTYEjfMQRS3xrwe+8v15I7rPgfghkQS0t7cjFApZmddRyQUodM/hw4cxMNCP2UYgiFIvghUgCPj9vhoATbpFbhIrlQEhI+ponduMbxgxgJEbhMF3ZAUQCoXQ0dnJueHJISA1ySAA+xsadCtc3swQy3HrP1kpBoEZkPH7fWYGUbfKU2qEMjDk5fnTFQ3s5n6s2AsIx44dx+HDh4UYTDL6LrlkEKwcwIkTx+HxetHf14+MzExw3Vr55hDTRsArMCJyJgDy/H4NQKf+vdlIrkFEVX5eHv97OvIBZj8FiokgMMbQF+nDgQP7cVjTRNsw7fMCuAsboui9d9ejrHIG2q77R4wtGsNvDhkpAMhAMK5lgKFTIlqiRie486crI2itmW5QRymK1mAbNq1fj+bjxyR3fBQigZYRYu1gaairxZGjRzFubJEJkPaOjuP5eXlbweW5R3DheDB0cu/7U3C+dB0agO0Atre1tbfwMYjjJ06g+cRxqQtFckfym0P5wgQivP32O6bRxJkIKW+hOoTDz/18mQ5O/Ftl9USEDRs22tc8SRWQdK9gcV8g8Od1z+Lo0ePmvgAioK29w+x9h6/2rp9UcH8NgJpgW7tmDL9gjOFE8+d48bln1Y2wR21voBwYAdAdDuOD6hooJncYY122IdZK9evDTnwun0BCo+Dqmi0IdbTHndiWNgCIu1DExA8R4fVXX8HnLS1Wt25LChgP+LUUEPm3D7Hd0ZsBVAfb2jTLyI7iRPPneGHdc2IMxpS6o9AunhQ9AvnA0GefbMXb76wH09OzsAZIfw0CO/F7ANQD2CAQn6z4w+b33jO7hgjbwbiZTelVAXwiwsENuf83d6Ox8aBgrLa1tQsPe5KDgPRATx2A9aZ9xGnUaDSKg4ea8NuVdwlJH3FY9yjYAGaEzSjGVHYLAZ5+5hlEIhHhu23t7SdLd5DBjl6dGTYitiv642Bb2zHT8gehtzeCJ9euFTnfaRROOrOBYocwhUegH88//V947fW/WAEjQxJ8DYKIrvM3AXgVwLZgW3szEV9Iy/DO+nfx8vPPKV09vu4ima1hrqQkmFSJYt+3Bvx25Z34eOunYmKGTmoQ9CPWOew9AK8D2B4MtrUYBTbGbIBPP/0MK//5l44bQIjX/UnYAUnYAMy+P91hWNMD963BoabDsQAR90DtlmdwsnQH4Z/3FQCftAaDzaS700ZeRdOO4De/vnsIQR5+Mke6C0IkbiepRIy/n317dmPVypU4fPioVWGr/00w2HaygIB/zpcBbA0G207wThKBcOTIUaxauRKN9fug6gPAjLYwpgEuzjEYNi8n1B9g6jQIRWkEtVsoHTNOnYN716xBYPIk4YEMcVZYUMBX1nxVGkLY2sG3BoOaqevN4lrC4cNH8X9WrMDenTvUayi3ipWO+sYD6QLA1PhEd2ghC8Zw2pnzsGLFCkyfXm7rc8fAUFCQL+fZv4xAiMLK6G3hANAUDLZpJO9aIUJdfQPW3HMPPvtkq93dprhROXOd0wcAvl081wtYqFJxChYxBp/fj5W//g2WLF4EF9fpinEdMnVp8GUCAukGnobY+JqPeQDoqk7wmAx1uPm9atz1z79AqKPD8eQNBxtRdkpp3Pa86QeA1KZEAEE8xOqi79bbV+Dqq65EdrbX3O0bE42W9CgsLPiit4ghTsw3ygAIBtsOEx8I0Z8xSoTu7m689PIreHDNvXF3+pjER3ypm8i8gBRIAKYU9ZYhKD0Zibp//sJFuPmnP8WM6RWWgcOvl/6aA8IXpUWMQfQmBQC0YLDtGL9ZVSgnJ0JdXT0efvgRfFT9vlhdxYG/4WAjAFicz6+hvO6jBgCB4BzhJULbXUjrvYzMTPz41tvwrWV/g4KC/Jg1IG+GYBbkOfWgAkAG935xiojdzOn0HgcAaMFgm0a8JJQLNmNJMbzxlzfxL7+737aTaShi376e1jUSaRKVmi5hMlGZlKQY7BL6Q0wuKcWPb74Z58w/G3n+WB0HczHRxpCkS6FlNDoBIBkJYRB5mxMALIte0TKRW4POzhBqtnyIxx99FE2yrpbXiQENjY0oK51qX9M4azh6Ettem+kAAAeqSURBVEBhD6g/g23UiTDyTP+vasl5uPKqqzD/7LPgy80VjCYzCMm47VlmpxKGgvw8GQCGS1mVAACqdev9FQMAbW3tPcZOYd5olatziAgu5kJHZyc+/GgrXn3lZby/4V27lGR2dddwUCL+EI/0GoEKcR/XBYSDSoACGPpCTC45Bf9w3fewYMF8FE+cALc7Q194y22UQ8y8qgAYxhQW8K3ah3usA7CuNRhcxxtwolATW2BHidDf348Tzc346OOtePJPf0LTUDhzGPF82VYw1i19fQIhdfgUQ4JiE2MoYtgy4UkdQDp86CDu+/Vq5Pr9WHbFlThj7lycdupsjB83DmAMUX2njihNRqA/BLclxJA6TCe26bmw2GjXXbv3YEvNFrz43DOxRg6DzfcZarNnznuiFG69S3hnEMVrVOzkFajyCY4zUCzAhDo78OzaJ/HsU2uRX1CIqkWLccHSC1BZOR1jCgvh9XpRNKbQXJJgsI24TSGJDq3k/s4I3TABxN3d3Qi2taG+vgE1NTV4f9MmHNWalM9rk1ay1FIZzfJyKIpATdWY3iZR8cQ/qaWCAvWMAdv37MZpM2c5GkRW99FY3Lu9rRVvvvoy3nz1ZbgzMnDBRRejdGrc7QaJDK0k6e/MrqaR3gg+/PgT7Nq1Ew319di8/h0MDAzEV3EQizeM73y2exdOmzVbsJ1INRCSOYAGye/BTLhRpFgV5BDEUEUJue8Y7+3ZV4f+/n7MmTnLdj6buOM+G+jvx9tvvuFAwdgqtQbbtDGFBcOZVGYQ/wMA1a2tQatKh4CGA4247Sc/cojVq8rl7P0Ut+/ehcyMDET6+tRMoifLRMNX8gYIozs2TmXxi4EpK8mjGiRhhkS5v65tqAcRYUZ5hX3hHBbc+KystJRAwKp77kWU+B3CphQY6tBKsYSdu66LMezYsdNZpJODwatz9O66WrhcLvRGIrbiGd6wUzGM9R45Xi+9AGASKs0JouAMFXVsgBdtZHzGTUTZXVcLAHC73agsr5CGUXFBJ9ibJL75xl+wZPEi5ObkmIva2hrUxowpHGyadwcHlL8CqGlpbdWI634dbGvHn/7whJrTFd6MsQa7a2t5gc4JT2Y3jrm/c+rDnArOT4EXoHDBZFQKO1fJHuJVmrJcAJUIn+3aCZfbjf6+fpwx51QTRE5rUPPeJnz40ce44Pwl3NYEkkGgxQFADYBdLa2tmuzyvbthI1pOHFcTnrujXbW1sUYZA1F4vR4MDERNKcAvi1kEIr7pLPIxBHsrXQBwGhsrxvKlmzTXyGjrScLgEcPTBqJc9aslRXbs3qM3nmaW8WTzToDf3bcGFeXl5nAKI1LX0tqqFY0ZUz0IALTWYLCDCELrybp9DXj4dw84iu3ahn2I9PXF+hNFoxK+SexeytvQgxlxNklqD6UL8Zf0qoA4XpzK9SMOMPqmBn4mEqlm5Bnf00Ug0wG0fddOMObCnJkzbd7JiWPH8Mgjj+CXv/wF8vPzdRDEPm9tDWqkVxwxbvZeVJrDw4dzjxw9hjX33ouucEh4NuN+P925U3LTWBy1xb0VL9BDcSxrosFd77QbgbYwr9Ut1HDj+JZn1oYBsraPMQZQVFgdWV8K6QDmsAhE2PDW/wBg+NnPbsOE8eNA/KBF7rwDA1Fb0jKWp4+9amw8hPvvvx87+EINhaFmN2qlx5TzonGCQiS3gefWVTa0wecdEjiSGxrFl4OrAGuKUeIejEmcwMyCUiZYveL5TIuecZwLwu59dTbuMY4Nb/0V//uWW7B9x0709w8gGo0KXT/NKd7M7pr29fWhZstHuPnHP8LWmmqbijPE+c69e0xdbo1NJK6jtzEsk9lG4jAVUwGOndYEBuIHdqR9YITk38cdZ05O4kPiBindKc+iij08tz7EzxvixLI0aqS+di9+eOMNuObb38Hll1+GkpIp8GRlWVxD3LNECb2RCPbVN+ClP7+EV198HgrxIHA/cR3TRXY39ZXg3sX1HIZqdyXB8SkKBMmGoINekgNBiqYGpHhI++nI3p6NM6nrDuzHdDMaaLeO+/v78OxTT+LZp57E3LPn4xsXX4Li4mLk5fnh9XrQ1dWNzs4QDh48iL+8/hr27d1jk+E2ouvp156eHjAuRWQSOWonvkpl2GwoDIWBYHOtR2degE33qhEddysTWSLS7loZDMhsf8ak69ftb8D0srJBOWzbRx+CABRPmoSiMUUoHDMGLS2fIxgM4uCBA9i3Z7cyOyk/gZF7J8nDF4xZx5IIMg1gZxE/hKyhrfgkAWGeuqpgjvOccgSqohFbVM8pz6A4p6rSiLcU44jKQesXGRfrcIxASmVaUgRUqHcgdYTQsdIHcap/SHIH9deJFIQk1ylU4kLHtmxEAjfYeEIgLg1P6jDJpxLcOfv2teEkUUiVgWNMBJr0HNZ5Gfe7uGOKuGCWY85DtRK29jDcgEWk0wtg0qoy6eEJdjNX9nud3FwnmUtDMEp4A0x4i6nNbhaf+8XvOGSkSOWuSXEAHpS80LCJew4obPCO61Z/AJbegRHyrDomU1cQVYw33dW6iinEOotDKOYQjJJVCP91kobXs/gGGDmFZofSlk0peZiCC5zjGNaPwm7ipZix1gkWiPx/UandM7uVzY0AAAAASUVORK5CYII='],
    'info_png'      :   ['/share/icons/emim/info.png','iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAB1MAAAdTAB3TNyzQAAAAd0SU1FB9sFDhMoH1InAHkAAAtfSURBVHja7Z35cxTXFYW/WbQggQCBFoTAmBgCwdhxJanEoVIhyf+d2HGSihM7hAAWIIxjg4SQERYg0D6dH+556Uczm0TVlF7rnqquHmZ6Rszcc/f7XoPD4XA4HA6Hw+FwOBwOh8PhcDgcDofD4XA4HA6Hw+FwOEqCiv8EHdEHDOuoFn67BrAOvNR52wlQPsFPAxPApJ7L9HpVAl8G5oAHwGJqX7Lucm6KwxL6O8BlYAo4CfQXCNAAloA7wJfAFvBDdI0TIFHhvyfBf6DzuI56JNxgPZ8Dp4AhWYQZkaDhBEjPHR6S8D8GrgDvS/OHgIEW7zuu12NyJEMCJ8Drv8WkNP6KjhOF3yjW/vjxcb0vWISXwIoCwz2Nmsv9/zgEXJLgPwbelfAbkaArkfArhd9xCBiU4J8CC8DaXo8HnAB5xH8m0vyLEmgs6EqH7KkqAjREgu9FhM29/MWrLnuQ4CZl8iclfAqaX9TkrAkRanr/ZBQ7VJwAex8HJPwpRfv9BQE3E3bsCsLjakSkKeCgEyAdFzDUIdpvhzggrOkzBlNwsU6A/HeottHWTs/HlmITWNWx7QRIuy6QdYjis8L1oTK4pCBwda9nAV4HyAVZFHbWwX9nTeoBDeCxUsB51QMytwB7H9vAhsx3o4OmNxN+wKaE/0gEWHUXkAY2gBdYFW+lIOB2GUD8723gCfAQ+A4rBXsMkAjW5bOf8HrxJmtjBWLhb2Gt4BngKxFgLYUv7pXA3AXUgBFgFKvtH4py/GLeTyH//x64AXwKfA7cjyyJEyCRIHBTAh0gr+uH12oFMoRUb0U+/ybwF+AT4LasSOYWIC1s6diWcJ9JwK+iwHBLccI88LXM/TXgn9L8GaWASeW6jtfT4nFswGMaOE1e1j0m67CsKH9O58c6P5QrwAmQNmoy/8OKB0KD6KgI8DxK9b5X9rCuTMKHQkuIw1hTJ0z9rIkEz+QSHI504Rag/W+SOQH2F/pVC+iPsoHnToDy4aii/TEFfEHTB+TzYwI8xcq6ocO35gRIM707JOGOKa2bUoR/QHl+RWQ4LCIEAjzRsQjMKt1bIoFWb7c/TNkxKGGfVW4/ha34mcbm9obJCz01aX8okG1hhaBFHX/HVgBtkMDE734nwIC0/gxwAfhQJDghK3BEr/fxZmcvRkOkeaXPrGB9/hd6zgmwB3EQq+a9C/xUBLiIVfaC0GtdusCqAsMRbKlYKAUvlMENlI0AdfnwIPgPdT6poG+4cH07zW/22rjcxztY0yf5oLAsBKhKs8eBH0noH2ErfU4r0Gu1xCt+rtUYeLj+ANYTmMDKxP1OgL2BsKL3fQn/Awn+sPz0cwnrEK3Hvjut/ImDytAuTn6gpgwEOAL8GPiVjgvS0i2Z6QdYs+YEcE4B3UALTaeFG4iv2dbR8Cxg7wj/18Dvpfl9CtLuYL36RyLAeWyQox/r7nUTBJa+TlJPXPjngd8Av5P5rwF3gX8A17FJncfy00uyDKeVBnYzDJOVnQj1hIV/DvitNP+SBHoPG8v6BJvOWYyKPI+xIY4luYf+Lv5OpexEqCcs/KsS/k8k/NlI+Ld4fTqnn3zYc4TdjcKFeKHKmzOCToAepXqx2f+Dijt1mf1PddzCavcxjip1C6NdtV3+/YYyiiWsEpj8QEg1MbJOKc27Ks0vCv+mhJMVtP9dZQdnRYB6k2IPXRSCwjzgvEi27hagdxhRrv9RpPl3ZPL/HAm/URDcmN53CesLHOjSpzdb/fMMKwHPycVsugXoDQYVvV8umP12wg/fb1w1gGm5kGoXvrsVQX4gHwh94UFgb1DBavin5AL6FfB91kH4AWPK+yd4fbFHpU0hqNKEBCvKKh6JCJtOgN4R4FBUvHmKLcLoRvgh+Duhz6i+RaHnCfmy72US2QiyLDFAhvXgv5EGftaF8MOGTeM6Hyikbu3KwMXG0Ctsvd9tbEXQMiUZGK0nIvwVmf05CXxGGtlOCKFB9J7ih6EWAu4k/G1p/iy2FOwhJZoLTIUAr4D/Rtq+1EH4AxL6JR3xTt+dGkDN/P8iVkmcFxlxAvQWq+xsBi/MAU6Sd/+yXZJvUynfAvnun6VBSoWgnQgw7Ps3oaMeaXSlzWdlheu2o+BvgRLMAKZcCOoWfVjNf0IBYDz3n9G+hl8kxyb5/N88Jaj8pWwBusXByPxPFITbjfZTIEDQ/jkS2PRpvxOgrsLPWQWB4zvI/VsVfxYUACaz68d+JkDoFn6ENYtGebOn3228sSqzH/b926aEKBMBaljlL5j/0Q4a3y4NRAHfPFZ4WqCkewFUS/ZdRkSACazt263/p1AfWMcKPjexotN8WS1AmbKA0C8I27kUzX676D8WfgPr9IVbwc1jawFxC7C3v8eRKPU7sguzH6eKL8k3gHpMSRo/ZbYAYRHoeWzZ1tEm/r9TCThgCyv+zMn3r1BilMEC1KT557FhkWler/t3Sv+Ku30/l/9/iPUAMifA3sYB+f13sKGRtzH/21ijaY587s8JsMcxhBV/QuQf3++n3U0fmj23jRV8gv8v9f5AZYkBDkfR/8E2eX07zQ/XrMrsL1DSyl/ZLMAwecv3OPmiz8oOhBeu25TwQ/PnBfsAqVuAEfJ79I0pHug24i92BtfI9/yd3w/mvwwWYFSB3yn5/+oOhR8QbvZ0Fxv7ekCJiz9lsQAHyZd7jSr1203kX5H2h3n/RToPfoQ1gnGQ2XAC9D74CzP/x3ZIgCLWyLd9f9SBAHWs0BRKzht67yJWQXQC9AB9WL//ArYn0Cj5gs8K3Vf94nv9fSfTv9zBZY5hreaLWAUydAqX3AL0Dsex0m/Y8mWwRWDXTfC3Qt74aaf9NazPcBnbl+CyyHMdWzOQOQF6hzHy9X5H2dly76L/fxn5/4U2wV+8NP2q/vYDvbaeatBYTdgCjCsILKZ+2Q6IgPL9MPf3QxvtP4HtQfQLbLn5BrYjyS3yjaicAD3K/cNuH8PsfL1fHB+8ioK/J220+JjijZ9L+OvYoMgX5HcJ8zpADxD8cKj9D/Lmff261fxMGh/GvlqVfo9iO5GFjSeHVCz6l45vSXhcPEUCTEb+/yDdb/DYDIEAc7zZ+aso1TwPXJHpPymXEW4XN4ttGpHswEg9QcKORi4gjv53Q4CXSt+eFqL/moR/AduD8KrSvgxbHXxdwl8mcaRGgEz/57pqAW+j/eH79xd+h0G5mfPAL6X9F/XaDLY3QdiFbNsJ0HsLUMzzd5t/BxN/Qse4nptWoSf4/FNYpXAW+CvwN/I7hDacAL0V/jEdx8nHvrrZ8KFVFnAYW0X0M/LNn89gO5Gd1d8ITaJrwL+xjalKMymUEgEGFIRN6TywC+EXXUUo7mQSfF2fPS3zPovdFfwL8vsFPaNEgyIpEaCf/EZPJ6MAcCf+v1gs6ouKSedkAQYUEN7B9iH6XI+fKd0r1ZRQSgToU/R/hLz5s9voP27j9kVuZQurCN4A/iQC3MX6BaUcD0sxC2gV+LVyA8VNH7ImBSGwKuAj4D/AH7GNqO6VWfgpEqDS5Nxp359Oq4PXlNLNYWsBr2G3hruHjYWVejA0JQKEvXqeKjJvdsuWdi4hK1z/Up/1DdbO/Vba/7UeP2MfICUChBW7t7EhkLAVTL2JFWi2IigIf0WCvx993gzW0fsWK/WusU+QEgE2sKmdL8nn/y9jvYFKm1QvvHdVluOhhH0j0v4HMver7DOkRICGNHcmEvIW1qkbaePnw7jWEwl7Vpp+R0Hf8n4UfKpBYCDBV0oDX0l7w0h4o4XrCNu83NfxWKRYY58j5VuejGN1+pOqDdRaEGCD/O7fC9L4dRzJE6CGVe2Gda7SesHnqqL+DRe5w+FwOBwOh8PhcDgcDofD4XA4HA6Hw+FwOBwOh8PhKC/+B0JFH3hNaND9AAAAAElFTkSuQmCC'],
    'recharge_png'  :   ['/share/icons/emim/recharge.png','iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9sFDhM4IzeKbq8AACAASURBVHja7X1pjFzXdeb3tlperV1bd1V3VW/cmiabDEnFlMaCnR+JEs/YFuyBg9hIbM8YhqPJQE5sGIP8cTLzYyaAB3EmogzLiZQZ22MICBRbTmzI9si0NDQylkRxEcnm0tVd+77Xq6r36i3zQ7w3r4vVzeqFVJvqAzxUdVd31at7zz33fOc751zGMAzsybtX2L0h2FOAPdlTgD3ZU4A92VOAPXn3CfOr/gVefPHFGYZhZhiG+QDDMGAY5hjDMF6GGf7VDMOAYRirhmEkDMOoG4Zx4UMf+tDZB2lSN4PsfuUU4Pnnn/eyLPs4y7LvFwThA3a7fUYURVy4cAHT09OYmJgAAFSrVfR6PVSrVRw5cgSLi4tgGAadTgfdbheGYaBSqaDT6aDT6aBarV7Qdf2srus///jHP/69PQXYZfKtb33rAyzLfspisXza6/XC4/HA6XSC53nwPI/Lly+j1+shEonA5/PB6XTi2rVrUFUV/X4fPp8PkUgE4XCYWAHoug7DMKBpGjRNQ7VaRalUQrlcrne73e/puv7nv//7v7+6pwDvoPzt3/6tl2XZ5+x2++NjY2NwOBwQBIFOPLnIRAaDQXQ6HeTzeRSLRfT7fXS7XYTDYbz++uuw2+04cOAALBYLLBYLWq0WBEGAYRiIRCKw2WzQNA21Wg3pdBqNRuPvdF3/889+9rOrewpwn+Ub3/jGcZZlf+bxeLwulws8z9PJFwQBHMfRRzKZFosFHMeh1Wohl8tBURT85m/+Jv7hH/4BY2NjaLVaeN/73gebzbbGAui6Ti2BpmlQVRWqqqJarSKZTNb7/f4ff/7zn/+7B00Bdi0KOHPmzOO6rv/Mbrd7OY5Dv9+n5lxVVSiKQh/7/T5kWYbL5cLly5dhs9nAMAw4jsP09DTOnTuHSqWClZUVBINBsOzbX1vXdSiKMvTq9/vo9/uw2+2YmpryAnjur//6r/9yDwbeB3nqqadmADwHwAuATvygEjQaDXS7XSiKAlmWwfM8XC4Xut0uVFWFxWLBtWvXEI/H6Z6/tLSE1dVVWK1WdDodOuG3bt1aM/Hm5yzLwufzwTCML3zta1/7wp4C3OubetvL9+q6vmbCyYokE8MwDJaXlyFJEvr9PkqlEh555BHoug6GYSCKIn3PyclJ/PZv/zYAIJFIYGlpCYqioNFo4NKlS3A4HOj3++j1eqjVapBlmX5Ov98339tX9hTgHgvP83WLxXLH5JNVH4vFYLFYoOs6IpEI2u02NE2DxWKBy+WCoijodDoIhUIQBAEMw6BWq6HT6eDw4cNYXV1FMplEPp+H3+/H/Pw8rFYrstksbt68iU6ng1arBVVVIYoiVQyGYcDzvPdBUgB+V0anGGZVEAQ4HA50u11zAAe6ruPatWuw2+1oNpvgeR5utxs2mw379u0Dz/NwOp2oVCrm90Ov18Mbb7yBw4cPo9vtolQqQdd1TE5O4uDBg8hms2AYBoqiIB6Pg2VZHDhwAL1eD7Iso16vw+l0Yr0A054C7LA0Gg1EIhHq0dtsNqoERBF4nke73QbHcbDZbEgkEnC73ZAkCc1mE/V6HaIoolarAQAkScIbb7yBXC4HwzBQLpeRzWZhs9kQi8UgCAIAIBAIwGKxoNfroV6vo1arwePxgOM4WK3WPQW4H9LpdJBMJnHw4EEEAgEkk0koigKr1Uonn+M4unfLsozx8XGcPXsWoVAIiqLgl7/8JU6ePIl6vY5erweHw4FwOIzZ2VkAQLfbRavVgt1uR7VaRavVQr/fhyiKEAQBpVIJABAOhwEAvV4PHMftKcD9ElmWkc/nMTk5iYceegjVahWFQgGNRoMGcjiOo6ZbkiQcP34cuq5DFEXYbDbcunULoVAIjUYDkiShXC5jYmIChmHQmIIsyxAEAYFAAL1ej4aLZ2dnYbVaUSgUqBV60GRXKoBhGF5zUKPX66Hf7yMajWL//v1QVRXFYhGlUgmFQgG6rqPX66FQKMBisYBlWRocIr5CMBiExWIBALRaLRiGQZEE8fRFUcT09DT8fj90XUc6ncb169epQj2IsisVQNf145qm0YANUQSWZeF0OuFyuTA+Pg4AcLlcqFQqaDabKJVKUBSFmm7DMOjPbreb7t/EcXS73QgGg3A4HPR9CBJYXf2XyC/xDQAglUrtKcC9Fo7jzoqi+JVOp7MeTISqqqjVakgkElBVFQBgt9vh9XoxNzcHwzBgtVoRCoXo/5GgETH9HMeBRBmvXLmCZDKJZrOJVqs19HM3em1PAXZYPB4PACCfz8Nut8Pv96/7t+12G9VqFY1GA51OB71eD5qmEdwOu90Oh8MBt9uNsbEx+Hw++P1+jI2NgeM4iiw22JJQLpfRaDSwuLi4pwD3Q8rlMsLhMGw2G+LxOBRFAc/z8Pl89/U+er0e2u02/H4/FhcXYbfb9xTgPvkBSKfTmJubQzgcxo0bN9BoNFAoFHD8+HF4PJ41PsKOhUZZFjzPQ9M0tNttMAyDxcVFsCwLTdP2nMD7LQSnP/roo2g2m1hdXcXf//3fIxqNYn5+HizLQhRFdDodSJKE22lhI63sbrcL4my6XC7YbDbk83mk02mMjY3h5MmTYBgGhULhDm5hTwHuoaiq6jXvyYQUmpqawuHDh6EoCpLJJJaXl7G6ugpBECCKIliWXcPxq6oKwzDQbrdRr9dRKpVgt9tpSNdut6Ner6NcLqPb7WJ6ehpTU1M4cuQIisUi0uk0er0eff/b97anAPdBjg/7JcMwEAQBoVAIgUAA3W6XWgZJktDtdlEsFiHLMmRZXgMHAVA2kShKIBDA3NwcAoEAhXjpdBqJRAKSJA29sZs3b+4pwD2/KZ4/6/P5vpLP59Hr9TY04/V6HfV6naIAr9cLjuPgdruxb98+sCwLQRBgsVhgtVrBMAxlDV0uFwRBgKIouHLlCorF4rqfp2kabt68CbfbvacA90MsFgsmJiZQKBSgqipsNtuGUJBINpulMHB5eXldGEjIpVGk0+kgm81ifHycBqD2FOC2fP/73z/OsuzxVqs14/F4SE6+l5hxEsEzY2oAF27n5NcNw7hoGMaqruurH/nIR86SvyuVSpicnITP50M8HselS5cgSRKOHj1KYwT3UkiImFDOhw8fhiAIDxwfsGkFeP7552duZ+x8hOf5D4iiCLfbjUwmA13XcerUKbTbbZTLZTAMg3K5jH6/j4cffpiycACOE8JFkqTHb+fl48UXX0S/3z/b7/dX2+020uk0ZmdncerUKeTzedy8eROZTAb79+/H4cOHIYoi9fy3K8TC9Pt9VCoVtFotMAyDhYUFBINBNBoNGlx6kGTkb/Ptb3/70yzLPmmz2Y57vV64XC44HA6aln3jxg1IkgSLxYLp6WlK4dZqNfR6PYyPj4NhGLqqB7l9kpUrSRJKpRJqtRqKxSI0TYPf76dkTqVSQTweR71eh8fjQSAQWJPOVa/XIUkSFEWhEI+sXKfTSbeBsbExOJ1OaJqGZrOJXC6H1dVVlEolBINBRKNRhMNhlEolmj8giiJEUcRHP/rRXa0FO5oW/uyzz36AZdnnnE7njNfrhSiKa/LxOY4Dz/M068bn84FlWVSrVeRyOZpeRWDa0tISYrEYnE4nLBYL/H4/BEFApVKBLMtwOByIxWLQNA2dTgeFQgHFYhEulwv79u2D0+mkefzZbJbCu3Q6TWMADMOsgYOGYdC4P8dxNKhDKoQCgQD8fj/Gx8cRiUQgyzKSySRWVlbQbrcpDBRFERaLBR//+McfGAXYcAt45pln/pJhmC94PB6IogiGYSgOJh9CVjHP8xBFEcViER6PhyZmdrtd+P1+vPe978W3vvUtRKNRaJqGhx56iGJ8wzAwNjZGJ6zf79PVGwqF4PF4kEqlcPHiRZw6dQoTExPo9XqIxWKYnZ2FxWKB0+lEo9FAs9mELMsoFotDB8UwDIRCIVitVpAKI1VVUS6Xkc/ncenSJeRyuXVh4MrKyrvDB/j617/+HIBPk6CJOQBiJk+I+dZ1HTabDdevX8eJEydoogbDMDh27BheeuklKIqCdruNhYUF9Pt96iD2+/0172Mu0CBFGxMTE6hUKvj5z3+O06dPIxaLodvtroGBHMdB13Ua7CGhXTMMtNlssFqtIDmH5DvkcjmkUim02+11Q9PxePzdkRL29NNP/6Wu658mZVLDwquGYdDce6IQvV4Pk5OTWF1dRTAYhN1uB8MweOmll1Cv1wG8zdxdvHiRmnQC8wzDgCzLkCQJDofjjiodTdOoCX755ZfxW7/1W3cQM9VqdVNsoK7rIw2SLMsUBprp5QdB7mBTzpw5M8Oy7BcI8WFOyzZfqqqi3W5Tpk5RFLRaLRw8eBBTU1MAQBWAZN74/X4cOXIEkiTh+vXrSCaT6PV6UBQFhUIBV65cAc/z9DPa7fYdaeFk0s+dO3fPB4fUCDYaDRw4cAATExMPHB/AD8G/XmK+zQURZNVHo1FKkAiCAI/Hg0KhgGAwCE3TIMsyNf/A27w+Wf1erxcTExOIRCLI5XLQdR379++HIAjodDrYt28fDMOgCZper5f6A8QKkK2j2+2i0+mA53cmlsVxHFwuFxiGQb1eR7fbhSzLiMViiEQiNC3tQYOBd1iAJ5544gJJf+71erRIkqzAeDyOUqkEURTR6/XA8zyFg36/H9FoFC6Xi0bQAoEAHbTl5WVUq1XMz8+jUCig1+vhzTffBMdxOHnyJC3rKpVK6Ha7uHbtGnK5HERRhMfjQb/fR6PRgNVqBc/zOH/+PFKpFBRFgcVioUiDoACiOKR0rNfrodVqoVQq0Yvs/STO8Nprr6HT6WBhYQEf/OAHMTc3h2q1inq9jgexpd7Q5dPv9+H1vp2X2e1278jJr9fr4Hke3W4XjUYDU1NT4DgOU1NT6HQ6YFkWqqqCYZg7KNrz589jenoa7XabQsrl5WUK7wKBAGRZRiaToc5htVqlW4yu63SlBgIBtNttVCoVNBoNsCwLlmWpc2m2GuReSX4fx3GQJAmyLMNqtSIcDmNubg6nT5+GKIpIJBK4ceMGyuXyu48NlGUZhUIBs7OzaDQaKJfLa5SAeOtWqxWBQIDm6wPAK6+8gpmZGeTzeRQKBSwsLNCIHZF4PI7Z2Vlks1lK6iQSCTAMg2g0SieFwDVJklCr1cDzPMbHx+nr+/bto86mpmmUCRyEgMPQC3nviYkJ8DyParWKfD6PTCaDVCqFRqMx9D0SicS7AwY2Gg0Ui0XEYjEEg0Gk02ka6bNYLOB5nmJ9RVFgs9nAcRyi0ShqtRrcbjcajQbOnz+PEydOoNPpQJZl+P1++Hw+qKoKWZbXJGXouo5bt25R2Ga326kjGIlEaIo3idX3ej2srKygXq+j1WrRRhFer5d6+F6vlzKEjUYDNpsNdrudPvI8D0VRkE6nkUql0Gw2113lyWTy3bEFEOl2u7SQ4vjx43SgCoUCZexIRFCWZaysrEAQBGiaBkEQEIvFYLfbsby8DIfDQWEaAPh8PnAcB1EU6SRbLBYEAgFqsl0uFyKRCFwuFy3jMgxjaF5ev99HuVzeFAwcFdMrikKjkdFo9MFXgEFnR1EU6LoOv9+PSCQCQRBQq9VQLpdRLBaRyWRoXj6pxiWdO0gwhhRimi0M2aNJswe73Y5QKERj/7quY2VlBVeuXEG9XofVal0z+aSC+F6Jrutot9uQZRnz8/NwOp0PPhv49NNPH18vDEoKMxwOB0RRpGbZarWiVCpBlmWk02kAb2f1yrK87l4ciURgtVppgQbh+slevLS0hJWVlTugKJFUKoVTp06t+xlbEbvdTuMbJD4xNTWFcDhMm0488NXBDMOsut1uyLK8bhYsWdnNZhOpVAqSJMEwDFgsFgiCAK/Xu2a1E++ckDGkxw+xFCQLN5PJ4MqVK2i1WuuGZAFQtvDcuXMYHx8Hy7JwOBwU6jEMQ51VYsF4nqd1f+b+QMSvUBQF5XIZy8vL4DgOBw4cQCQSQaPRWMMGPvA+wB/+4R/Wn3vuOQQCAeRyObAse9cEDFKlQwowZVmGqqrQdR0sy1KHjpRgeb1eSsn6fL6Rgzm6rtNCzbm5OUxPT1PPnUwyiQGYeQozDCwWixAEgb5Wr9chCALC4TAmJiawsLAAq9WKZDKJpaUl+vq7CgZ2Oh3qJGWzWVy7dg2aplGC5X4L4RlKpRI8Hg8OHToEURQxPj6OQ4cOwWKxUF6fQEGyTQxCQAL/DMNALBaDx+OBy+VCtVpFJpNBJpNBMplclw001ww+sAqgaRqSySTm5+fh8/mQSCTw1ltvoVwuY2FhAfv3778nRRl33Nxts02ycw4dOgSPx0MjkMTnWI8NjEajQ9lAu91OE0eHwcD1JB6PUwbxgYeBJBgUjUZx8uRJVCoVpFIpvPzyy7h16xZmZmYwOzsLl8tFO3RtVSkI5SuKImZmZmj/nmq1CpZlsW/fPkxNTaHb7aJara67ZdwrNlBRFGSzWUxMTFDf5pOf/OQXcLuL2Tbla9/5znfquzIOQGCQ1WpFJBLBvn370Ov1kEwmcfHiRZw7dw4ul4tm95ABJwQOCeXezv1Du91Gq9VCq9WiaViEOJIkCcViEYlEAjzPY2JiAocPH0YgEECj0cDKygqtEtopAmgUxSS9BA4ePAibzYY333zz2sc+9rGfLC8v9wAEAQhbRZmGYRQ4jvvjRx555Olf/OIXhV2hAE899dTMoKND9k9BEDA2Nobp6WkaASSNGorFIk3rIk0aSRm3OVjTbDbRaDSQTCappx4MBuF2uxGNRvHQQw/B7/ej3W4jmUzijTfeoD1+zIEbVVXh8/lQrVZ3bDDWYwMDgQCazSYMw4AkSZFms+lTVZXVdV3YqgIYhqFzHBey2WxMKBR6AsBXdoUCsCzrXY/1IkUVpBGTrusIhUI0OESKJsyOGAn6kPZu5tSs8fFxGlgxDAO1Wg3JZBLnz59HPp+nPXuGyY0bN6DrOsbGxmCz2SgbSGCg2bwPwkCy5aiqCpZl0el0KDO4vLyMQCCAo0ePwmKxoFQqIZVK0b5Bqqo6dV0XOY5jeZ5nGIZhNxMbMH1/g2VZxm63z4ZCIWnXbAFPPPHEhWeffRa9Xg+SJFFWcJgQerbRaFAYyLIsrFYrNE3D1NQUtQZ2u30NlcuyLCRJojCRbDe5XG7DGACp0BFFkWYUkWxeUtlrGAYYhqEZRWYYSMLYpMMYaTEbiUQwNzeH48ePI51OY3V1lbaoM3cI0TSNY1mWI2ll5tyHzSiBpmkMwzC8zWazsyzL7RoFIOZ1enoaqVQKyWQSgiCMXJffarVQLpehqiqSyeSGcQCSTjaqdDodpNNpTExMIBgMwuVywePx0ESSVqtFV/d6A08+z+/3w2azYXZ2FqqqYnl5GdevX0cikUCr1aKTbxbSkZTkS9jtdgiCsGnnl/Qn0nWdEQSBwTvYsHOoAhAv+uDBg0gmk7h16xaazSYWFxffkViAqqrUGTtw4AANP5MMJHNt4KgowOfzYWxsjDq767WjIUKKXYgCkU7lxPndrAIQa3b7f5ldpQAklm+xWLCwsEA9/1dffRXpdBpHjhxZ04Z1p8XpdEJRFOJ0QVEURKNRmpp1P2PypD0tSV/L5/M01Gy2KKNaMvN9m/5n9ykAsJYOPn36NFqtFlZXV/HCCy8gFAohGo3SNq0Oh4O2VZVlmXr4w8yjLMvodDqwWq20yyeJ5rlcLsTjcaysrEAURRw8eJBOfCaTgaZp97xNC+EuCEcQiUToPZBty5y+vhUFIEyoruuMYRi7RwHOnDnjHYSBhA6ORCKYnZ2lVTmlUgnxeJw2UTR30SRFHqRxc7PZXJPowfM8DckqioJcLgdN0xAOhxGLxfCe97wHDocDqVQKN27cGEoHk/A0iQxuJRClKAoAYGxsDO12G41Gg7abI9+33+/TSKM5RkKUYNC/uNvkE5RCrl2lAAzDzKzHAprpYIIObDYber0eZQZJdQ7DMBQOmgeGZA/Z7Xbouo7p6WlYLBb8xm/8BiYmJlCtVlEul5FIJJBOp9fdm/P5POLxOIWfhFEkSaEkmZVYMhKIIns52cMJbM3n8+h0OjTyOT4+DkmSaB0iKQ0zm34z4ziKBSDoxJxWd/v/do8CPPHEExeeeeYZipeHCVkJJKBD6GCSx0fO5CHBG3MxRaVSWXPkCwkYaZqGbDaLt956i9LB68UAms0mqtUq/uRP/oTWBTabTTSbzTWt48k9kBVLIpCEug4EAvB4PIjFYvj1X/91BAIB1Ot1GukkHcfNMHBw8s0VUpvZAsj/7joFIBIMBpHNZu8rHXy3VaTrOkqlEhqNBhYWFpBOp2k2MvHGx8bGaMSQWCPyvlardU2DB7IiSbr7G2+8gVKphFarNZQNJM6neQWbLcAoVmCYAhiGsftgoNVqxfz8PFKpFK5duwaGYd5ROpgkbLjdbhw+fBgOhwOqqtKS8GEwMBqNgud5qoCiKNKoHmkUSYJQRHHNQpJHDcOAz+eDrutU0YZtAaP4AGbUYNr/d58FKBaLiEQiWFxcRCqVwqVLl5DP53HixAnEYrH7c3O3KV/SnnVhYYGmbY0CA9Pp9NA4wHqZPWRr8Pv9mJ6ehiiKaLVayGQyCIfDcDgc+Od//mdae7DZLYBYnF+JLYA4T+Zq3lQqhR/+8IeIRqM4cOAAJicn13jhW8XmZDDtdjui0SgymQylgzmOw5EjRzA2NkaTT3cSBiqKgqtXr9J2c+FwGC6XiyIVks5eLBbXFLzuhAUwDIMh165UAPMg+Xw+zMzMoN/vI5fL4cKFCzh79iycTielg4lzNyodXK/Xcf36dVrFk0wmkUwmaXbw4uIi+v0+TdYYhIGkSaQkSVs6yOHSpUuIx+O0PQzhKczl8Ha7HQsLCxAEAS6Xi36O2Q/YLAw0WQyGZVk7wzC7hws4c+bM8fVapbEsi4mJCczPz9OM3Gq1ikqlssYTJwQQx3FrkES/36eTn0ql6AAGg0EEg0HMz8/j5MmT0HUd2WwWV69eXRcGlkol/OAHP6AKaEYWd4OBzWYTFy9epGwi8QPMqMO8wkVRRDabhcvlogGc7cBA099zHMfZHQ7HB5555pk/+9znPvdn77gCsCy76nQ6Kd27EQwkFbNWqxWxWIz26mFZlpIpbrd7jSceDochSRJ4nsfk5CStD9wsDCwUCvjd3/1dcBy3BgqS9DECA81nA7fbbUiSRHG92+2mtPAw0sh8NpHFYsGVK1eQz+chyzJVXuIPbDYSSCyApml2m83GCILwlTNnzhxTVfUzTz75ZP0dUwCSFRwKhWiP3LvBQHLWHjnDbyMYWK/XKQyUJIma3c3CwJmZGdqKZmZmBgcPHqSfAaxNCH366aepdSKRP/OZASR4RELA5CK/I80wKpUKqtUqut3uHannm/UBCALodDq4ceMG0um0quv6o71e78cf/vCHEwD0USwKGRoA5Uaj8XSz2VwB0NuWD9DtduHz+WCxWJBOp7G0tDSSItxLGEhKv0RRxKFDh2Cz2VCv16Gq6h0wEAClr3meJ4dA05NGSU2j+RoM1Q7bs0mJPKlnIO+zmS3ArAiE5ex2u+B5ngXg0DRtXtO0iK7r/VHfzzAMxTCMBIBPWyyWVwD807YUgDhk+/fvh9/vx/LyMi5duoRSqYRjx44hFovdFzbODAMJPU2ygs1RvmEWKZVKUTq4VCpRH2BYJdFG32XwNeLfmJHAZsigwS2GnHiGt3MCLIZh8ACcxsAbDr6/+WfDMFSGYUSO4yyiKE5tWwEIY5fL5RCLxfDe976X9uczw8BIJLIjMJCsMAIDyWFQlUoFuq5jbm4Ok5OTlPvfLOlD8vs0TVvTvGoYSbPepJNHsi0Q8mmwW9qoKIAoEiG1bv8ve/u6Q7E2UjRd1y23/bdpAPZt+QCDk0KOZI9EIpifn0er1UIqlcJrr70GSZLgdDrh9XrpoGwFBlqtVnS7XZoO3u12MTU1hYceegihUIj26q1Wq2ugIDnWlRzscDdTOdjX0DwZw7aD9Z4PwsBReQDzFmBWosFtZFiuwUb5B5qmMZqm8QCsACzbUoCnnnrKO6w3EImSTU9P0y7crVYLhUIB5XKZQjsyWGYzOQgDiRKRwXO73bT0+tSpU7TzRzKZxNWrV2nb98Gs4BdeeAHBYBA2m40WgJhhINlnzQMuCMIdaVyDCjDsGvTgCdW9mW1gUOnIvQxuKaNc5rkhLXwIstguDDy+3r5ozgomCR2xWAwHDhygfYIGO3QQb5zctMfjgdvthmEYsNlslJwxDAONRgPpdBqXLl1COp3eMCs4Ho/jwx/+MD0MktQZmO+TmFaSnk5wOFl1dzso6m6RSzOlu5U4gDmUPKoPMuz1UU9JGZUOPvs3f/M3tGffei3aSV+/waxgj8cDRVHo+X7mGgFSnmWGYaTBk67r1IpsdDQbadhImlP5fD7Mzc2B53mKUgbP9nvllVfoaiVbkzl1fCMfYNhr5tjCKJM4TAmIQppzGNbzJ9bbGsjEm7KLNq0I/HpaFY1GkU6n6Unao2YFZ7PZTdHBmxm8Xq+HdDqN8fFx2tm70WisgYGqqsLj8dAIpDlTyDxZwwZzFLM9uC1uh/sYbMI5bNI3skrmLYko82Yd5KEKQKKABw4cQCKRQDweR6/Xw+HDh98RSpiUaCmKQjt1WK1WGtQZ5C1SqdSarGCS0kYmzRzKNQ/qKKtnPYSwFSXYzJYzzIoMbid329Y2hQLy+TxEUcSRI0dQLpexurqKcrmM2dlZHD9+HD6fb02mzE6KzWajJWYEQUSjUUxNTdHmUpvR9EFv3Txww1bdVip9diLYtVk/ZDC3YCs5kRvCwFqtRgtDZ2dnaRXOjRs3KHnjdrvhdDppdw5CJN1tYIgTabFYaNs3cnxbPB5HIpGArusIh8MUdWSzWdq3kKACQjqNsmoHufthZnyUVbTTQbCtOqNmBTfnG+4oHUzKqux2Ow4cOdGv/gAADXhJREFUOIAjR46g0+nQOrpKpQKGYeBwONYERgjJQoouBrOCSRYx4dwrlQo6nQ6cTifGx8fx/ve/H6FQCPV6HalUCplMhpaYEfnpT38Kj8dD38tisUBRlDs6hd4NW29nkndqC9iOZRngF7ZNB88MK4siDhU5cXtqagoPP/ww7HY7MpkMzQAmHjhpGjV4k6RtazAYBMdxcDgcmJiYwMMPP0zz+cgp3m+++SYymcwdLCTwdqWOzWZb87p5QswBFrJfmqHXZuDbvbQC29lCdsIf4YfdkN1up6dpDPtQm80GSZJQqVQoueJyuRAKhTAzMzNyDz7iqJE2csViEa+99hpqtRqNGq4Xpk4kEviDP/gDRCIRmo9Auo4DoIdBmrcKM3u3XQXYDTKI/8l33KoCMACYP/qjP0o/99xz6Pf7qNVqsNvtG0JARVHWxAGG0cEulwuTk5M02XQwK9hcIHK3JkztdhuZTAYzMzMQBAHxeByqqsLr9UIQBMzPz8MwDNqyHng784f0GDDv/1sxmbtp8s2Bpa3s/0QBSFYqS57LsoxwOAyO45DNZum5faSYcrNCiCWHw7HlEzdUVUWz2YSmadi3bx/tLK5pGhqNxobFoaTcfKvs3W6U9VDDViwAO6gExJTOzs7C5/Ot6dTxnve85751yyQmjuTph8NhTE5OQtM0kNNMRo0jmLH/gySDMHa7CkBr1RuNBrLZLKanpzE5OYmVlRVcvXoV6XQa+/fvx8LCwhoqeCdEEAT4/X44nU4sLS2h0+mg0WjA7/fjxIkTtEXtViZxvcHZCWfunVaqzVDS620Bg0pAIWCtVsP4+DgWFxdx4sQJZLNZrK6u4sKFC/B4PAgGg9TJEkWRHtBAYCBhq8wXWZGkCwnLsrRmLx6PI5VKQRAETE1N4fTp09Tbz2azNM+f+B+iKNLq4vUUcZBpG2XfHFUxdpNF2bEtQNf1BgCPWRFIwsbRo0dp5/BSqYR8Po9yuYxSqbQmYYLQr2TCiWNIMD/xWCVJgqZp9OwBMulOpxO1Wg3pdBpvvfUWdSDNk/yjH/0oLgjCnMvlohw/uQhdezsmYNyWkXPwtxIVfKcsyHb+/w4n8PHHHx9TVXVo8h/LsrDb7bRJlMPhoB07eJ6/IxZAJJVKwTCMNVQw8C/U8MTEBCwWCz01NJfL4erVq8jlchs2i/7TP/3TJ8PhsPPYsWNzc3NzR71e76wgCA5RFGd5nneQAyYNw+ioqtpnGMbOsqzAsixzW4bCqUHSxwwZdxo5bNcp3QkfwGz6mYMHD04TD3u9wSciSRKy2SwajQY9qtXhcNB6ANJbf3p6ek2zaLJCSUSQpIVXq1UsLS3dtVk06Rf86KOP7n/11VdXc7ncEoAl87iYH0+dOvVZXddjVqs1xHGch+d5K4lCmk8THQwemaOJZGszp5ftxOQTi3n7HgzDMFRN02RVVXu6rvcNw9A22tbI+7AsKxuG0QbQ3UocgCrAX/zFX7z17LPPwu/3o1Qq0a4ddxNJku6IA/ziF7+4Kx1M8gJGhYIk6ygcDuPVV19NY/0+fVQJ6vX6G6Iotvx+v+FyuRxWq9VmPjmEtJAdJLcYhoEsy1AUhTaJyOVytFOJ2Z/Y6solcRK32018qG673U40Go24JElFwzDUge9k3J58Y8D5UwHIABxbtQBEEaCqKvx+P1iWRalUwpUrV+i5QFuNBWx3pZAziMfGxmhW8mOPPTb/0ksvJXBnGpTZAhi3bt26AuDyF7/4Rdbr9S5arVbaLpZUCZOL4zjaY4B0AyMUeCqVwpe//GUUi0WsVz21FeTj8Xhw8uRJOJ1ORtd1Wy6XS1y5cuU/V6vVumEYBsMwZOINhmF0juP0we3jNttpaJrW2k4kEACYXq8HwzAwOTkJm81G9+RSqYSFhQUcOnTonlHBZn/DfDIZwzCYmZlBKBSCqqqwWq3weDxOANw6CjB4gWVZxu/301PINoobkEwncjQt8SesVusd5nerVoBkS4miiLm5ORiGgXQ6bTl27Jjnm9/85pLp3vWB76Lt1DjzA4NGv0mxWATP84jFYgiFQshms6hUKjh37hwuXbqE+fl5ekYg6QO0lT49xM9gGIY2bCRn+ZFuIsFgEDMzM7DZbNQ3MA26MaDAhul3a342DAMulwuCICCTydCfRxHSiNrMkWxn8s1KQE5bbzabiMViI2df7bQCDA4o7RIWDofxa7/2a+h0Osjn88hms7h8+TIuX74MhmHgdrtpPH8QhpFTPs1p4eQsPlLUScibQqFA4wIcx2F6ehrRaBRjY2O0Tt/cJYxlWR3/UkLFDDP/g1epVEIsFoPVakUikcDly5ehqirtgzyKH2JOLNnO6jdbAcMwsLi4CACDkVZjvfnZKQUwTKvfGGJOIcsyHA4HQqEQYrEYBEGgTaLz+TyKxeKaeDthpgbr+EnPPTOkMpMadrsds7OzCAQClC5OpVJYWlpCpVK5ozy8+XZzf/VuTqDJeTJIXuH+/fsRCoVw69YtSjsfOXIE8/PzW2r/up1QN8/zWFxcpMq/zmQbQ6zbjlkA8xvr9Xr9uwzD/N7gH3Mct+bQqJmZGZoObi4PJ0GiYQUTREHsdjuCwSAAYGpqiub5NZtN5PN5JJNJrKysbFge/sMf/vDWEB/mDhhoUgCdRBALhQIikQhOnTpFC09efvllvP7664jFYpiamqJHzHe7XXS73R07JGMwAslxHE2sMSm4MWTSjXtlAXDblLIAcO3atf+2sLDwe3crDyddwmRZRr/fh8PhgMPhwNTUFGZnZwHgjnP2VFWFJEkU/hFnb7Pl4a+//vpPAfRH+I5mJ5B+IVKxJIoiAoEAotEoTp8+Tc8gIqnkgiDAZrPRuIDZOpgTTbZi+s1j0uv1hk3+eoqwowpAJ548/+53v7v6pS996QnDMJ7e6fLw7XQJIys/kUgs/+AHP3h6QAGYdcwmfXzppZd++LGPfeyTwybFbrfD5XIhGAzi6NGjtPqY9CIgGU5Op3NNwOhunMIoQZzBhZZOp18Z4v3r90IBWNOb6+YP+upXv/qdVqv1RLVabSwtLaFcLo9cA3cv4gCKoiCTyWB5efn/fu973/tCqVSqAVBuX/2B5/2B530A/TfffDPdarXeXG+ySPdScpZxrVaDruuUn/jgBz94x+TvNIFTKBRw5cqVi/dj8tezAMQZZL/61a9++5Of/OSr+/bt+/qlS5fe906Wh0uS1E4kEl974YUXfrTOvr+RBaDPf/KTn/yXD33oQ/+TYRjX3bz+VqtF4wCtVgudTmcoCthKHN/EUOqqqjK6rjOZTAa5XO5/feMb3/j+EAtwz2CgYZp482UAYL/zne+sAvjgk08++Wiv13sik8n8a1IeTppG3W5wsGPl4YVCgZaHS5KUzefz//vs2bP/lE6nW4OU9RBFwEZbwfnz55M+n+9zDz/88H+/efNmxDAMetLJKNJut7dNBpmUxtA0Ten1evVkMjnR6XT+x5kzZ754r1f9oAJoGJ4TYIaFzF/91V+9AuDVT33qU9OSJP2bZDL5CZ7nj46NjdGUcJL2PVgeTnwCcgQcKQ9vNptrysNTqRRyuRwkSWq1Wq3/k8/nX37++ed/NmTSmbtM/oaW4Kc//elbKysr//ajH/3olw3DeLxer2NxcZEWvo6irNuZfNOjrmma1Gw2b0iS9B/OnDljXvn3RYatpGGKMHixAJjf+Z3f8R47dux9DofjfYIgHOU47ijDMJ7BZoiDUS9S0EiqdRRFudrv9zPNZvP/JRKJX7744otLg/zEkMeNJh8bQMI1v/vEJz7x0Pz8/H90uVzvnZycxP79+3Ho0CF6GHalUkGtVqNbwM9+9jPcuHEDnU5nDSwcpc5wMHhkGEYfQI7juJeWlpY+v1MTvymHdIPfr5cpNFQZzNcXv/jF95FGiB6P518BYMjPqqo2q9Xq1dut4DL/+I//mN7g/bGFlb9pJQBgPPbYY5MnT578dw6H4zFBEKYikQi8Xi9sNhttmNloNPDjH/9Yu3nzJjqdDjesucPgSh+cFLMC6Lqu6LqeAPDi8vLyl3bSsdyuAmCdyd5IEZgt/j3u8nyUiWdGDJcaIzwan/nMZw6Hw+FHHA7HIzzPH+E4Lkqs109+8pP06uqqT5ZlO8uyzLDg0HoWYEh/AFnTtFUAL8bj8S+/EwrAj7ByBskWdoNJxYgKsNFKX2+/ZzapvKPE0odF2IznnnvuIoALAM4AwGOPPeY+duzYEUEQ3I1G49/3+/2juq5bGYbhBgmhYc8Hu4OR7XEzTSbvlw+w2f/d7GoedcXfbe/fig8w6nYwLBJHf3fo0KH/KsvyYwAmb6eXbdhYYnBVDtTxyYZhJAH8Uzwe/0+70QKMah2whdW9FSeP2YbiGpuwBus+KoqSYVk2A8BgWVbYKJ9wIwh4u5C2D6DEMMzuOThyBxViqxM/igJs5x5HVYChFsFut5cMw7gE4Ba5n22kkBssy3YEQSj/Km4BO/W5u1UBjBGcyl0p92sL2KmJMLaJUjYz4Zt9/YEX5kGrlduTzQm7NwR7CrAnewqwJ3sKsCd7CrAnewqwJ3sKsCd7CrAnewqwJ3sKsCd7CrAnewqwJ3sKsCcPrvx/40j5TKq9JgIAAAAASUVORK5CYII='],
    'usb_stick_png' :   ['/share/icons/emim/usb_stick.png','iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABFpJREFUeNrsnW1O20AQhtcEKFVbwp+2UlEFUg9AbkBu0B6BI+QGcARuQI7AEeAEwAmS9geqWrWlUj8okLi7ylhYNMEO9cd653mkERBHdsi++87MxrGNAQAAAAAAAEVEDX3dGzY2PXtNQxvvkVT57NqIPY1dhqf8mR97HhtNekMXGiaAIm2/K+HzayydRcVucoyhNs8BAAEAAgAEAAgAEADQBhazHpDmCAGwHoAAFBDf+TvS+CZQA1ADBJvLi8rpZe0XATQklx/jADqqf1IA1T8CoBugCwAcgJmsxiFwAOUgAAQACAAQANAF0P/jAAAAAKAE30+D8vE6APMyNFw3YG7e2hgY/78KnjcG8j9BzsGPAw1EkINBwAIYMLzZOT8OPLy6gohvC0FVFnxDiZD/x0w0LwXvyc8+xusP2zXk4qprjm1SgD+z/+7voMABBjV3HjiAR7MfF1BaA6TPBai6DcQBAAEAAgAWgvykO+PxIwSgg2McoNlc2NhPzVg3o3s21tB2+G3gNxtbU/a7JdvytIGzoA1sAG7mn015/Ey2QeACOHrgNrqAgPL//5KkgejO3zhAA3j3gPYOKAIr/ayAIrBE1iTXp2/ftiuP0QYqcICiZviBmZy8eRK6A7AS+G8R2ZOfpxpcgw+DbjmVQrFj41BLysABJvTNZKGoLwJgHUARSeuoslAkBUxs/9Ao7RJCFsCJVPL9jOf1NKs/ZAF0pLBzs3vHFLNMjAAaxpoIoCNWf8qQ+02Zp4W7lOCWhA8Mp4WrLAI7Uukfmvs/NCIFKEgJXYZedxvYY+hZBwClAhhKW5iwY+q5UghU3AUkH/O2pxy3neoOVHUBmgSQdYWutqnmOgG0gTWxl7H9u1F4nQBNAhgW9BwEAAigiWwW9BxocBHYpgjU7QD797SB+xodQNspYW7RpyvV/jAljD3s3w+4WDRwuXjtcMMI4JYxVcFNo8rHFZvcNAoASAFz8ziAtYobG78RwHy4kzdf23gUyEQb2fhg4wsCyMYty74J1HFdQfjVpxfko72u2xgHKoB1BJDvNYUqAOe4KzYuEcBsyh78KxufZBBcbn5i44WN5Yr+vxYOUJ8A3DeEz6c85uKVqeYaATECqEcA1zY+3rP9XNrOJU3rAD4KYFTSfj/nEJd7zkscoF7iEh0gi0sT+LWBNaeAuObj4wA1D8BSjvZrKeAWVH0N8NTGj4wZuFri8XGAmt+glrR5F1OOEcm2FjVAuA7gcKtwz238tPFLBtu1fs9k8EcNFjg1QE4iSQdX0hmsVnRcBOCJAOo+HinAgwG5TK0LVC0AHKBGAcSS/6NUzscBlAhgJIWfO8toWVpCHECJAJKCb0Vm/pgaQIcA3Gz7Y25PxIhS+49xgLAFMJaZvygx697AOECgAriSAV+Ysc9EDNem2rN0cICKa4Cs/Y0kWsb/70kggAfMtHHG9sQhbuT3SJMD+Kj4qOA3Oppje6xNAAAAAAAAoI2/AgwA68+0fsHg1nUAAAAASUVORK5CYII='],
    'settings_png'  :   ['/share/icons/emim/settings.png','iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAdTAAAOpgAAA6lwAAF2+XqZnUAAAAGHRFWHRTb2Z0d2FyZQBQYWludC5ORVQgdjMuMzE3g3eGAAAK00lEQVR4Xu2dO4wkNRCGj/cbhuchhGAEEo8ANASgCwgmJJyQcELCCQkIJkBAdgEB4YYECF14CdJKEFy44QUg7SECApAWCSGON/7QtGQZ93RV2b273V2WrJ3dsctVf/1Vrrb74MIFb46AI+AIOAKOgCPgCDgCjoAj4Ag4Ao6AI+AIOAKOgCPgCDgCjoAj4Ag4Ao7AgBG4FHT/x9BXA7bZVY8QODA4H8IcOYrjQODYSABIMBsHBNO1Yl7gfAjg28DAuYMDLft/M2c7cPsnr/7lQgIcTh7BgQNwrZAAJwO3f9Lq3x2s/7WQAGwFi0mjeIrGvxXW+iR0HFejLSs4HwJsaiizk/Fe+LmuKG80onB+E62/hc/vVLBsW4kAnCOUNg6jvo30cRJEiMbOjyt29m+As7arlQjAOYK1kc0+bNHDSRCAaXN+TIRPw7gnDR6ggCt5BIznzg3rQ97rHTpMmgQS5zdOYHvYhi6tDxYVna89EJqFtallpOSrsd0Z+Hm2UzTOj4H8fpc12rQnU2wEkSd1TjOOdUnlL3XA9nb4nrHM+11BAsg9mWZ1fuy0KwGteYTYMnyuted3kYNLonXiLYhXuv4kSFDD+fG28HkAHod0Oa2P7ykOIQIpvFa9MWoS1HR+Hw61ymxSvnV+Om+0JCBt1wJpzHJ+DDjNxlgQXAxG/eUkEAXBYowEwKb3nQCtBPhjh81otwAIwHP8DSdBKwk4/ZSedQw2Sax2BNA8J495z29s40liPlivKhU/9CzwvyxAYEymUeScVVRzrAwB0177cU5jH0fHk2ua83INmOlYDmwuh06EzTpQZv9dhr4Nvesip0SneC7rjH7fz+HO8WmtE7ScMw6C/JLrZHTm7B/y9KUn2ajrfmHUmYFj1FqR1MjhwGleGTUyR9u9fon+m8p6DlLcUSUSdN0UxuCQGUj1cZeAJ7njlxKC9xy8BQQWOwKUnBJS0O17aYQ1SOXUA/scBImoTVZ7PEM2wHlSR6fjfglzb4SOHG87BL4qAJSU31ZErQVOb3Mk+/67e2RvC3SGjN4iBKzbAM7PNW4erTJTQpAV2t7asZKA4s/ymtsoSbMyRhJpPxf5Vqd0pXTSfm49nja65ua+34zSmwajOP/WAkhUphE0C38jI2hlacaTVeaJjZDCcmaADZN8/o/xA0yNA5qxpPi09e38Zm2cDdnixrM8aV1ry8oQMKOasjWAlnt86uMZfZ8zc7WHxZa2GmZUTm4zhuj/WkmAXPFENtBGXo3xODxupHPtfcLNTDYZrfMXwbJN6LD+2Oi03ONTrWpfS4ocGbFPK4fxFLScP6xDB6fBt1mwYBk6UYJxlv0xB2QKjhXw9EZQG7mNbikhyQK1bAU35EOK0juNUyEUDofFfUUkctOmdRwy3m5BA/2vhq6J4FwWOFDK0KzXkCJXBJ+Kk/ctojHEMpZsEjccppGT2z5y9nDoo4liTgvjtlLqpbEhHnvmDk8VsBoinYfD40a2kc6VOr+RT5aQyiYq4zZTzJWukRs3OQKkhybS9J/bOiTgXVE4Mj2UshwMackgseFUx2gN0Iw/TiyZK5xDSrY0nCrVMV1DQx7pGuk4i029zrEaIpmXplmqYsm8k0KLWVeyTnpZdBoHU4Wm1Z8uAco65iBRl4iTyKKqL2lSR26TRfhdol/JmBK7eplbYkzX3JQA0lfJ0nlaw6nwu3Tj+3SdtXCeRHbbGK0tvY8vMaZrbgqwtEpnLy5p0kgmU8RNSpwuu/d9X2JXL3NLjOmam6bypTDCqMZLGgTq0o3vN74FyICSgJkbc5QAPBc6BlmMtTQeO0+E66Sni9xYWm2VzrPY1OscqeKWcZzMxU1z5m6tA4hqqa68DxA3CCudax3XqzMtwq2GSOctEqU0UbZSGoRDpdF/XEBOqe25cUqT+h9eYoxkLhEZt7UiynDmUggBzsepEp0YczmRyzrSuSXjhOac3jAcJD2etRieFoKzsJ7m0oY1qdaZ19awQRr5jQ0psaRnBxYMmAM506eO0/OyYKV5GLMOvY+r4fTMnejTAomD2T62oeNwflInWMh7LYOHRU6bDcgHRx4rIdogXyJFaZQH6ENDhMXg4LC4QQhtFtASZt/4RaLPykDIRj7YENnr0FO5GZ4N+08YyGnez0rAiK40CvpOuW0EuJJxgfaFEmS/OWxXlmmvudNvHJFmAQhB9NSM7C5Z7MOzCtGf20LKEB3Y7KXBcaT8eWInztBU7l0O3vc966cpGhJa1k/JPDD31VHXAhyRk24FOKVmAZYjAc7PvY9HEaklFbLSorYOogOTQhRowWM8TwBpA1DIYZHXNQeippHP+mvjerkaYmCuq6MukWyNXMBPG/IgR5dDNd9T3M0yay3D36xPITky1UF0gFJKrk9zJACCeehEmcbR6ViyCU7ONf5+YpT/zQB91KvKzwbpfxvBxGnbPdpxtEtGuC6UTzbi6SS31zfLcPtnjfyGZG3E6hXo8yrc8jiYRiuPgl1FFWRY7wgDaZjDOQKfOZdYdADE9lJDV3Q/Oq/OOG29LgkjU5LKSck4so9GRpBmEYmujOlL1z7s700mkSAFTDoOmatKGi+DnNJaok1vCNuVtSqZcT7FbHpwfgw2+zn7P07UtEUYvA2dRz8p6azj2FIm2WC+9fHPCjZ7PoDjXDrEgIR8hih8b63srToxD8JNrgF4CWhjmju5OwEYPyYHltjC/yuZ+asppYC+iqoSR5z1XOqNQb7woSUuTD9rsM/r+pyIjrrB8B+cAK0B8GfAZtSPhR+58zuz32djTgEHToBOAnDzOOpaYEwk+K4yoUfv/Ca71SABt3EUTS+GzplC6e2cpjDkKYZHWdo69BoHWpNxfg0SQKC0WJqFv3G50tcbQVwEQbh5Zo8mZXOrqCFRPHZyzreS4ChMXAqKJO7rrc7IzftAsCZDuG7GmZq1J+t8DQlIsZqrUyJS44SusU26F/Lgv5dJJFfHk3d+Fwlu7lIr6V3byBZdjpV8T21hrco3Ye5PLXq48xOPpoXhF7uUqnV8F6kkTo/HQKSSBnk/Tkjgzm9BFBLcCJ09vLStK2UAnjBqtDeCkC9Dd+d3oFnrKHRRiQAQqVZjK7FuJ7V0mJSckwokmE8KsZEZe1hIAAjkbcAIlBzOUAhy4udtwAisCjPAdsC2u+oBAfZv7WNfPB4CeRs4AscFJJgN3HZXf7ePW7IAR7neRoCA9WLI9/8RON9NcAQuPB4w4L/Epe3POHbjQIA3hvgXyNq+GIf5bsWrAYLXjf02h2/YCNwZ1H+toD80bPNd+wcDBK8U9IsO4bAReCKo/3JBf3rY5rv2OPCFgv68QzhsBOZB/ecK++3DhmB42teqvCkAeZYv7fdVhPCWIIvurQWBWfg7hdc9FRC6N8h4qkKv9SQAIR8L/VEnQd67VOw4v+kPh88l6TeVF8vWfH6kkIxE/AOJbRDdM0EE7P27yCA64g74RLIFLCI3lWf5HR0s62MeL4AyP7cupLDKLeTk+ZpOusdZ+zrRTArVtC6Zmu+1dQmZCwd3rVGzvtBgc27G3hU0AQRphywSZ+AAqUzJOCn5bt1FvURmM2ayr4nfsQOreVde85O5ufQJOXCWRpZ0LHIhVlva5juprHQc9kyqESkAVtqRQ6slT6oPRGuIwE9+l85tGyfJbJMiiRvrCDgCjoAj4Ag4Ao6AI+AIOAJ9IPAv2dwCWS9Mx+AAAAAASUVORK5CYII='],
    'activate_png'  :   ['/share/icons/emim/activate.png','iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB9sFDhQTKj1pPecAABkqSURBVHja7V15cBxXmf+91697RhrJsmxj4pwm7EaphMUKeEmylYRgJbbWcpGTMgu7ltc2JAUhYWMwC5vdJX+EkCoMixeTmLJAAmJiJ3bkk5ESh9OBYIyjjSnH2Dl84UvXyDp6uvu9t3+43+SpPbpGozmc/qpUI8vTM93v+333974HhBRSSCGFFFJIIYUUUkghhRRSSCGFFFJIIYUUUkghhRRSSCGFFFJIFxqRfN/A8uXLL0okEle6rhsFIMvKyjqmTZv21qOPPno2ZM8FBID77ruv3Lbte6WUswG8D8DFAKJCiAiAKADqv9UFYFNKXQBJAB0ATgM4QQh5gxCy55prrvnjihUrvJB9RQCAJUuWXM05/w7n/HYAHACllBoAiBBiyOsoPYcHIYSklEohhAQg/M+QlNLThJCnGWNPA3izoaHBDtlZQABYunTph5LJ5M8AXOX/SfqMHfE7hwKGDgr93imlfUKII4ZhxBlja23bPsgYE01NTTxkcY4BUF9fXw7g85zzx33myMD3SCEEkVKefzOEpJg8Eji098mgNjEMox3A9wH8Rkp52DTNEw0NDb0huycYAPX19dOklD8QQtzlMwKe54FzLj3PI1JKcM7hui4455BSQkoJQggIITAMA4wxGIYx6G/qVf2uwJMODP7fBmkHQshPpZQ7CCGvR6PRt9asWeOGrM8yABYvXjxJCLEGwCcJIbBtW/b29hLP85BMJuG6LjzPG6TedWkPqn1KKRhjg34IIbAsC5ZlobS0FIScu/00gFDqhQS+bzeAPxFCXi0pKVn/1FNPdYUAyI69Nz3PWw7gcdu2kUgkZF9fH3EcZ0g7nokPoMiyLBiGgUgkgmg0mnpV2kEIAd9xJP73SSGE/qwOgD9SSn9DCNnW1NT02xAA41P9s6SUL3d2dpZ2dXVJIQRRjlo6hvsMGpL5w/1fOkCpn5KSElRWViIajaY0g3YfMo1GcIQQXQBOM8Yea2xsXB8CYIy0bNkyZtv2E2fOnHm4u7tbOXxSefvDMXS8NJSmUCaivLwcsVhM1wr62xQglLnwKKXHCCE/YIx9793iNI4bAAsWLKiQUnafPn1aMsYmnOnDgUELE1OvjDFMnz4dJSUlYIwN0hx+joHo0Yn/f4IQ8iCA9QC6m5qavBAAQ9Ctt976VF9f332UUjmaGD/XJIRIAcGyLFRWViIWi6lwMR149LB1wDCMRinl46Zpnm5oaEiGANCorq7u5s7Ozl8rSQpIVMEBQb1Go1FMmjQJ0WgU0WgUjLGgvxDUCJwQ8k0AO6PR6G/WrFlzwWgEI9MLV65cSffv39/quu5UKSUhhEii4rJCRLqfV6CUwvM89Pf3o7e3F8lkMqUBTNMkhBAIIdT7CSFECiGolPIWKeXHOeczq6uru9ra2o6+qwFACFnqOE6967pK6kmhP6zGWChGO46D/v5+2LYN27ZhWRZM01Q5BKlA4IeYJVLKDxNCbp01a9ats2fPPrh3794T70oTUFNT0+o4To3jOKRQ1f5onMZgJMEYQ1lZGSZPnoxoNJoyDf4zBjONxymlOyiljzY2Nh4vRgBk5Ko/9NBD06SU7/evJ35IVVwP7jNfzyMAgOd56OnpwZEjR3Ds2DFdUFRiSc9tXCKEWOJ53v76+vp/e9dogNra2gccx3k8mUyWeZ6X85Av15qhoqICU6ZMgWVZw9YfKKX7KaVLKKV7fvjDHxZFvSEjH2DmzJlfklJexzlP2dVc2G7936qQpIpJQ703E2dRgUCllpPJJLq7u0EIGVSsEkJIKSWhlCpn8T1CiGUAJs2aNeuNm266KbF79255wQHg8ssvXyKl/BvP86QfAUwY46WU52kY3ZELfrcChLo2k3vTr1HfL6VEf38/enp6UkBgjOlRA5F+VUpKeYOU8p89z0tWV1cfaWtrK9j2tjGvzr333julvb29GcDNqsKXTROgq17G2KDPVr8HGZTuWj3uH6oCmWmWUQiBWCyGWCyG8vJyRCIRcM6HyiH8ghDy5CWXXLLxG9/4hih6DXDVVVfNcBznU1LKi9ViZFMDSCkHlYB1Jy2dxOuaQH+vYRiDXtV1uvkY7X3r1+rOogofASASiRDlO2jagEgp30cImXf27NlLZ8+evXvv3r39RR0FuK47GUBsItS9ytKZpplK02Yj+WMYBkzThGVZqcxfUGNkEjkAgG3bOH36NI4dO4ZkMgnDMFJho/8+KYSYxDn/TDKZ3LtkyZIFxQ4AQwhBM1Wnw6lYVdOf6GygaZooKSmBZVlpzcRIIAj+LoTAwMAA3n77bZw6dSplWrW6gqSUmgAudhzn+fr6+oaVK1caRQmAoRZjvMy3LAu5ziTrTSXB7qSxAkJpha6uLrzxxhtIJpMqmwgNBIRSanDOl7S1tR1eunTp3xadD3DttddOd133binlewgh8DwvI8bpadmgs5dzT1gL75RTqUcTIz2f/v/qms7OTkgpUVJSosJJ4n+mChknSSmXzZo1q2P27Nlte/fuFUWhASilCQD949UCuj3Nhr3PFhAikQgsyxrUO5CJr8AYQ3d3Nw4fPoze3t5UOKl3KAkhSoQQT7qu++3FixdfURQAYIwlhBD9Q4VuY1X9+kIXSiZQ+QkKCAqs6cLMkYDgeR5OnDiBM2fOwHEcqLI53mlTgxDiASHEuvr6+o8VPABuvPHGswAGsil1BZsk8YGgQlLdPxjr/oVEIoETJ06gs7MThmEQIUSwafUfpJSNixYt+q+C9gFaW1vFzJkz7wFw9TlTKUkmuQApJUzTLIo6gp5PGCoFPVLuQEoJz/Ng2zZc10UsFiN6W5rvF0yWUt5YXV393ra2tp8XbBQQiUS61XNmosK1BgwUE6moYSzPHMwdCCHQ3d2No0ePIplMpjMJESHEA4sWLdr4wAMPRAsSAISQPQD6xqPC09XiR1IagdexvCerBRnlH4w2ZEyn5WzbxpEjR9Db25sKF32gqBzC3T09Pc319fWXFBwAYrHYLgCJcTBztKpfZxwJvKbF5hDvIdkGg9IGumSP1jHUrzl+/Di6u7tV+EkUCPwoYZ6UcnV9ff2VE+bnZHrhnDlz9gshrgYAzrnycEdtAkbI+umducHNpUlK6ZsA/gqgB4DQ9gtGAUwDcJEQ4mIA5jCfmzXinA/a9jba3U+64FRUVGDatGlgjEFvsvVNw25CyD81NTW9UTAAqKmpeYlzfivOFTxg23Y2AaBLqsqitQB4lRDyZjQa3V1dXf36Y489dt5MgHvuuefivr6+Ktd1q6SUl+DcIIrrhBDXTSQYdBCMZR304lJ5eTmmTJmCWCwGzrn6PyGEoP42ti82NTX9qVAA8BTnfBkAI8sASOXPffTvo5R+0zCMnS0tLSfHep91dXWlnudd7nneNYSQOiHEQiFELN13jXcxpZRwHGfMG2N0bWBZFqZPn46ysjJwzoOaYJ9pmh9vaGh4K+8AqK2t/Yxt26sBmFJK6TgO0R9kuAUYBgCDGGIYxgbTNB+Kx+Mns/GwdXV15QMDAzMYY3/POX+Cc37JKExOzkHAGMO0adMwefJkXROovMEeSuknGxsbD+UlD6Dohhtu6Ojp6fkcAObnt1N7/kd6cFXzTwMAxXxpGEbDzp07Fx06dChre/QOHjzovPXWWx1VVVX7Wltbv11VVXVcSvkBKeWUoMnJFASqrqD3Soy2nqBAwDmHbdswDENtdFW9BVJKeTGAa66//vrmPXv2OHnTAABw2223dXueV6ESQqM1A2k0QFDtv/rSSy9dl6v4fv78+Z/q7+//Kc7NIKIBEGQMBn3TSSaaAABmzJiBsrKy86ItSulay7K+sHbt2nHNRhpXGs6yrDXjjO2Dkk8ope25ZD4A7NixY90VV1xRahjGGgDd2dAEfsJsTCFiMHEEAKdOnUJ/f7++tqp+sMx13cXLly8neQNAeXn52nc0GMlKWpcx9nA+snxNTU32zp07P29Z1kJK6a8CIBiPkJy3a3ksIFC5gt7eXlU1TRWRpJRPtre335w3AEgp3/TLw4Pi3wy0gHL6TldWVj6bz3Rva2vrCyUlJXcxxr4MwA6AYMxgyLTfIagJTpw4oUCgnEHp9zeuuv/++2N5AcCGDRs4pfT/tJuWer/cSLYujfR/PZFI5H0L9vbt27tefPHFbxmGcZdhGHt9EIhMzYEqJI1VOIKa4OTJk+jr6yMKBH6OYFZ/f/938gIA3841qnZ4QgjJ0A8gfjLk5Xg8XjAbKXbu3Bk3DOMuSukf/LXKWBNkWvnUQeB5Hk6dOgXXdVUZWdUNPrN06dIP5AUArus+G5y8kaEG6IBfYCokam1tPWyaZp1hGD8dr2OoCkiZggAAHMdBe3s7CCGDmkpc130xLwCglCYNw2jRUrajko40Y+HafZtbcNTS0tJumua/G4bxQiBhlJEpyCRS0gUrkUigvb1daVu1cXVKfX39g/kwAS4h5CdaNEDG8lCas5QwDKNgJ2/E4/HjpmkupZT+NpBDGTUQVKo3U9Kdwp6eHtVPoHcbfzLnANi8ebPknP8lgFQ50iDoNLGxTQgRKGCKx+NHTdNcAeBtlbEcqzkYT7ist6Dbto2uri5wzlV7GQVQtXjx4n/MKQB81XbcMIzXfZSmpoWN5PVyXnyznFtaWn5nWdZ/jydHoFLFmYTMesk5kUiorWnqXqYIIRbnHADV1dWnGGNrgygfTVFId5RRBGNmfMfwx4yxDVpkIDOR5EzG6QXXtaOjI7ip5YqlS5fOyCkAnnjiCS6l3EspHVBoHE3fXAAATEpZNKNmysrK7se5/RFkrNpgvJ3Quino6+tDb29vqpWMUnqR53lVOQWAr9YOEkJeVs842mhALZyUsoRzXjSjRpqbm7sYY+u0ZyCZMHE8DqH6nI6ODv0zpwC4NOcAuOyyy04A2K8WZKSkkFoATeqjUkoDRUQXXXTRV7SIQOYSAPr1tm1DTWsBUA5ges4BsGbNGo8Qsg+ApxZkNCGPNqu3lBDCigkAx44d62WM/WI8EpyFPAwApOoEvlBNyjkAAGD69OlbKaV7RxPyqDBQTdLw27SKaqPA5MmTPcMwGgN5gbxQZ2cn8M5uZC8vAFi3bt1fCSG7oRVORsp8abasvNgA0NzcLFzXPTzW69T08myQiggcx5F++5hHCOnMCwB8Z/CXeKepYsgqmJ7a9ItJVEpZXFuFzmk5OxOmZcMPCHwGsW0bPvOP5w0AM2bMeBHA2XSMHs4P8NvE3lOEABhzMmgiZitSSqXneQBwCsChvAGgsbGxKxKJvOabgVRDxGicIc759QsWLCiqSGCsWiub6n8IOtrU1PTnvAHAd46+hnOnfqZldnAB1L855zWu6xYNAObPn08AVIzlGjVVLJsYVMrIMIwuxljjmDTHRCzM+vXrXzMM40g6MxBMf6qSpr/l+lZWaBMjhjddlDH2sQAjhiRV+8iy+lfFIGGa5isNDQ3P5h0AvjO4eYzhoOScwzTN9xYLABhjFa7rfm40YaDaPzlRRCntmzp16tfHfN1E3VA0Gn1Slwx96GM6L1YNcOzv77+5WADgOM4yIUTpSNKvhkNMhAvi/5DS0tKG1atXv1IwANi8efPblmX9YZQJktRoVc55UQBg5cqVzPO8/xxJ+vWtYllW/6lj8CzL4pWVlU9kpDkmcpFM01S5cqmXiIewY+r3W4oBANu3b/+VEKJsOOlXan8CmJ+y/YwxUlJSsvW55547WXAAkFK+hnN7+IluBoZKZgghwDm/qlCmaA5FNTU13xdCfEST/vNA4LruoE2iWR6oLX3JJ5FIBAD+NWPfYYK95B7Lsl7UtUC6MFBLkEgAaG1tfaSAmb+Sc/5ZDN5YS3SVn0wmU8meLEt9KuRTx+eapvmJ0tLSREECIB6Pu5ZlrdIXaagoTw8HXdd9sNAYX1tbO6WmpubbnPOHfeafJ/lqV+9EHZypOoDVwVaWZTUZhhFvbm6WBQkAXxUeYIztUwgeaWE8z5NCiNLa2trqQmH+vHnzbnIcZx3nPHgukHJckUwmU2Fets9PUJtCGWOIRCLEP7FkA6X0oR07doxr+/yEA2Dy5MkdlNKfvZMSGNYZVNGA6XneonwzfuHChZPnzJnzDdd1nxZCzEOg/49zPtG2PsV4xhhRY/X8wRlfisfjifF+x4QDYP369S7nfF8ggSKHMAPEzwdQIUTdkiVLKvIo9fPPnDmzSQjxVSHE5br9VYz3PC8V30+E1PtT1IlpmlI1ezDGvheJRO6Px+NZObgyJ2nXaDT6Z9u2f8c5vxHnpn+QERIsiEQiVx4+fPgrAL6WS8bffffdFyUSidXJZPJjACqDjPcdVTKBEp+aoexvB0/1G8ZisTullK07duzI2qjenIRbBw8e7LryyiuvkFJ+VI/7041O8U+hgmEYBiFEfPCDH9xy4MCBgYm+xzvuuMO87LLL/qOvr+9HUsoPAyjRGE9c15We5xE19t23Z1ljvBqtY5qm2kiaAh5jbFd5efktW7dufeXgwYNZTSnmrPBimubLUsq/+vP7wBhLmxtXZkAIAcMwPtrb27sEwLcm4p7q6uqobdtWNBq94ezZs1u1xI5y7qTjOKnhk8FDJTJldmDUi6SUEu3ADN08upFI5Ktz5879zvLlyydk13TO+tjuvPPOKT09Pc8LIW5RizwwMJDWfvqLJP2DmA4zxha2tra+kq17WbhwodHR0TGNUvpxz/O+JIS4SldAfiRCspXBC546pn5njClzKAP86DEM46Xy8vIvNzc3H5pIvuS0kbGmpubHnPN/0bNl6bpj1KRMtUCU0g3l5eWf27x5c8c47XtFIpH4EIC/A/Ag5/z9+rHx/qBHGZT2TBidzq6rV+2QjEH7CSilxwH83jTN51paWp7JBU9yCoAFCxbc1dvb2+A7V8OOmFV5btM01fiYp2Ox2Be2bNnSNZbv/PSnP212dHRUua57G4AbpZTzhBAVivGe5wUPeBqXxKc7ozDAdAQlnlLaTgh53jTNjfF4vCWXPMl5K3NNTc0+zvm1St2ONFTR1wLqXn9hWdZPWltbfzTcdzz44IOTDhw4cIfrujcBuATAFQA+4LruoMSKdrhjVrx3nfEq40kp1Y9WHSTxjLGDlNLvCiHa5s6d+7sVK1bkfLdszgEwd+7c1Y7jfBbn9gIOOVVTMcl3kPRF7AHwJmPsNZxrfkxQSiNCiKlSyvdJKa8SQpRTSqcKIcr9nIKK1wed0jHe5w9Kuyp2ac7ceYz327ZbTNP8kRBi19VXX3161apVedsWn3MA1NbWzrdtexOACADJOSfDTRpX/oBf9dJVp/B/9L9RnGsvP290bVAtZ5PpQxx6lWK6Pxm1NxqNrvA872kppT1p0iT3ueeey/s8pJz3391+++0vbd261QNgjcbZUmFhMplUEkb8VmzF7BRTfO9dgWHUNj3opevXDCXlmmqXaYRJAnAppQnG2POmaf5k+/btv0UBUl62M9XW1rbYtj13tH7AWOLuIULKUXvr6T7HZ3zQlqdjeheATsMwTpeUlPzvtm3bnkGBU146cCsqKh62bXufn01TI9HHzdjRXKf+Ntyu5cCB1er+hpL2Pn+k/auMsZZYLLZr48aNp1EklLcNjTU1NR2c8yl6OJgNG52JHddz+sN57er9hJAjAH7tT0o9NHXq1BfWr19/EkVIeevBJ4T8EsDdo5HKiWC43p4W8NrTSjql9Ki/FfwlSumfS0tLX9+0aVMvipzyBgDLspo9z7tbhXrZlv4gmIJt6YFCTlDSiX+Peymlq13X3WsYRlc0Gu3YsmVLDy4gyhsAHMdp1rRBahRqtjJxumoPhGnB+DzFcMMwEoSQXYyxrVLKjQB6otGoG4/HBS5QyhsAGGMOpXSP4zgfzoYW0OrzkjFGtFw70ki3AMD9HxmJRNZXVlY+8cwzz7yOdxnlDQCxWMwdGBhY6wMgo+YKvSlDqXh1Lu8QTm4PpfQ0pfQApfQFy7J+uW3btja8iymvY01qamo+IqV8RTVEqEhgqBAvmKTRyqlpPXYA3D9j8CiAk5Zl7ZwzZ87TK1asSCKk/GoA3/YnpJRnAZQHD1seyoP3JV2FakN57CcIIT8nhPyRMbantLT0wKZNmxIAEI/HQ64XCgAMw+iRUh4CcJ2KwbWhUek8eJlmGDXx/+8AgA1Syv2MsTcvvfTSVxsaGkJJL2QAlJWV9fT29u7inF8HQJqmSfTTN5XEq9n4OD/H384Y2yKE+IFhGMdjsdiZTZs2hUwvFh8AAObMmfMJIcQGvHM23rDlVACIRCKbGWOPJJPJw4yxgXg87oWsLEINAACmabZ7ntfHOY8FEjTqFwFAUEpfN03z2aqqqv9ZtWpVT8i6CwcAhzzP+z2AmoCkOzhXWftLWVnZVzZv3vx7AGhpaQm5diGZAN8MrBRCqPMCPUrpLkrprkgksmH79u1tIZsuYA3gO3MbAcQAOJTS16uqqtatXr26O2TPu4juvffeyCOPPJLzcfF1dXWxe+65JxpyIKSQQgoppJBCCimkkEIKKaSQQrrQKaep4Pr6+siZM2ceADDTNM3Nruu+AuCLhJArAXx3+/btr4YsGZpqa2vnCSFcQgg3TfMP27ZtGwhXJaSQQgoppJBCyoT+H7kXKVxalYeoAAAAAElFTkSuQmCC'],
    'mail_png'      :   ['/share/icons/emim/mail.png','iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfbBQ4SKRTdLIKHAAAFFElEQVR42u2dzXXbRhSFL2UXgA6CDkRXYKYDpgLDy+zsCsIO6A5kV5AssyNVgagKKO+SFdkBsvAwh6L5MwMMMG+A754zC9MEBL77YX4egIdJXddC49UdIQAABAAIABAAIABAAIAAAAEAAgAEAAgAEAAgAEAAgAAAAQACAAQACADQIDRpsM3MtXtJhWtTQplEG0l7154lrV2LrlLSUtJOUk0z3XbOqzKG8YWkBUHNti2ch400lbQliNm3bZMhuqK7H9ywUIWYT9CG2Sqfbp8zf9g9wfTSMrCQ9BRr9ojM6kXSO7d01Nuj//ijofkvrqH+VTbwrHRefz79MKQrWUmaE38zmjtPQjx8Bc6yi9kk6l0hq7fl8YY7NZhAIJOaBvj5f24/yhICmeoJfDydyTPVuyKm2clnTrB4K+m9x86+BU5IDkPFWh1dnRqRZoczVT+u/v3lud23o+0u6b0vKT4XFA55hHO9R4GPwSouePMU4IdXz/7k8UUfXdvPExAEm38rnj7yAiDG+O8zkQSCOOa/msC1OClrSXWsW8J8DmbKcODd7U8jxXx/6wuxANh7fg8I4pgfEvM448QNlQpLQzIcNOv2L6ZyWywFo+UAlkDQm/nLiLmAqEmgByDo3PyHyMmg6FlAILBhfjIAgMCG+UkBAIL05icHAAjSmm8CACBIZ74ZAIAgjfmmABgzBKnMNwfAGCFIab5JAMYEQWrzzQIwBggsmG8agCFDYMV88wAMEQJL5mcBwJAgsGZ+NgAMAQKL5mcFQM4QWDU/OwByhMCy+VkCkBME1s3PFoAcIMjB/KwBsAxBLuZnD4BFCHIyfxAANIWg7OA4yszMzxKA8oJ5oRDELmbRpHraQ8DvGz0An/S6KunOfWYBghjmfzrZx/bM7yMVrPO1blNCEMP8BangduN7eeZs6gOCJuZXZ7p8Lga1nNydK0dXdQxBDPPljp3LwS0nddWFfTSBwKe24TyS+aHHOJobQkLH8WtnbigEh99TneQLCvfZqsH+qhs9CbeEtTDf5283gaC36tsNoBrsTaFdZvUqo+anzh6O6sGQyqD5qSEY3aNhlUHzU0IwyodDK4Pmp4JgtI+HzxT3zSc7+VXlsgbBqAtEFAqvW3SpHk8XxzbYAhHWruGXLh8f8gq8rdum7PC4TJSImeh2Kdi1pF8DzA8ZJzdu33v1o6nryn85k2DaSPrufu+mp+MJrQ0oSV8lfQwA4ObQFasHoBCEvZ6gtyEA821C0AsAmG8Xgs4BmGF+cghmbQBoWyw6ZG3c94QvR+1djDYdefCT2gJwj/nJIbhPCcAz5ieH4LntH/MZt68lWRjz084JriWrvF4H1PadQRXmJ4OgipHjifHWsFI/cuYrSX+Kl0x2ocrFduViXXqAEw0ADM0TmJsA3El69NjZB+KZnXw8ewxJ5tALDOvsf5VE4u3hw1Hw28Ml/xsndvQE5s983zuhliHr+XOzxznxNrNMnCv8+YNSkiYnvUCTR5ZfXEP9q1Szu5a+SPp8CsAh4VAS10HrRdI7udT88bWAvaTfRM5+yPrJ4zcnX/hH0r+M74PV75L+jj2bpNlvjVZvU4XdSk2z2bZt8jeFrte4odluC0W6Ilu6ZSLDQh7dvc/VQp0uA301c+3e0VWQIk6m9dHs/tn9ex2yg0ld14RxxLojBACAAAABAAIABAAIABAAIABAAIAAAAEAAgAEAAgAEAAgAEAAgAAAAQACAJS//gMKcIVGKEjCWQAAAABJRU5ErkJggg=='],
    'quit_png'      :   ['/share/icons/emim/quit.png','iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAYAAACI7Fo9AAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9sFDhQFIi8qAAIAAB3lSURBVHja7Z3/WxRXlsbfc7voBgRBEYijGEJcE6PMZmae2WdmZ/e/3/m2u8kk0ybOxIAoBqQVAcFu+kvdsz/UqVC0ratSRVdVv+/z1NMSCHRX3c99zzn3G0BRFEVRFEVRFEVRVB4k8T/u3r0LEeEdoaiSSFVx7949AIDj7aCo8ougUxRBpyiKoFMURdApiiLoFEURdIqiCDpFUQSdoiiCTlEEnaIogk5RFEGnKIqgUxRF0CmKIugURRF0iqIIOkURdIqiCDpFUQSdoiiCTlEUQacoiqBTFEXQKYp6uwLegvKqXq9XAFQBTNprJfHtEEAHQBNAZ3V1NeQdI+hUMQGfBTAHYAHAtP23WB0AhwAaAHbr9fo+gSfoVLEgnza4VwB8atccgInEj7YA7AJYE5F1EXkYBEFjc3PzcGlpqQdAeTcJOpVvyD8GcBfAbwDcAnAdwBSAWuLH2wCO7PsPAHzjvf/u+Pj44ebm5sulpaUu7yhBp/IN+W8A/A7Alwb57Fty9AUAH6nqJVWd7PV6aDab6/V6/YBhfHn084NfWFjgscnlgfwPAH5roftlg7x/hMUBGENUqJsWkQuqOgag471vqmqz0Wh0FxcXGcIXWI1Gg45eIlXNme+ak//GoJ/uc/E3dfZTqnpDRFRV26raAvDKwvsWb2/xxXH0cmgSwFUD/e57QJ5sBxdUdUlVfwng1xYNzFq0QBF0Kgdh+5SBvgzgF+8JedLZpy2nv4WTSn2Vd5mgU/kI22cN9AWD/kNduGK/67qBvmDRAkXQqRyE7XMAluz1rGBWrbOYw+uTbCiCTg3R0acNzKkUwKwgGm+fwOtDchRBp4akeLrrhAFKMCmCTlEEnaIogk5RFEGnKIqgUxRF0CmKIugURRF0iqIIOkURdIqiCDpFUQSdoiiCTlEUQacoiqBTFEXQKYoi6BRFRRrZfd0HnDQK8HRRiqCXDu4pnJw2Om0/cojo4MH9er1+ROgpgl5MyONTRq8mrnj3VBjkmwC2ER0nzCOFKYJeQMjjU0bvIjrsYAEnu6cC0emiuwb4NoA1u9YBNOr1+mEOYY8PS2whOkIpBDeIpEYN9DecMnoX0YkmU2/I0Y8A7CM6seQHAF8BuAfgUQ5h7yRSjiP7eoJNe2AdppLoGEcqLQtGDPI/4O0HEE7YNWtuH18xOLFr5ungwWYi5di1rycI98A6TLWvYxyZWkxAyAeqgpMDDJKQ7wI4rNfreWoUnUTKcWhfjzLkb6vDTCSeY1yLia+8pmYEPSPI+4FPHjy4BuCJhfW5cPXV1dWwXq/HUUbHQtNRhvxtdZiaRWTJWsyGpWV5Tc0IesaQJ2FPHjx43xyA54YXrw7Tn6Mf2f8zZ50Aygq7I+TvpNdOLOW54fnQwcGBGx8fnwyC4JqIfAng3wH8G4Db1jnHIXulLy2bs+/ftp8/1VbK9nwDQv7Orj6VyP1mLeyjqw8f9MB7PwfgMwC/BfCr93jmyTbTr1I5e0DI31mDjicm6ENWt9sdtw74cxH5AsANVf2QOkypYS+Lo1ftYd+1/CxtyOMGwVNLc9bBVyqVi6p6HcBNVb2uqhc/sA6ThD3EyTBq3oZTRzNHNzefBbBigH+ZAeTA4Blo1JA7eO/9ZQP8mqrOABg7Q0cew/6ltaUVALNlyNddCSCfThRV4gJM2pADgyemUMPVpKpeUdVlVV205y5njNoGtqeiw+5KAPlrPXAGkIeIhmLiOfD7GOGJKTlL2aZxMk5eTSlFGxghFhn2oOAPOc7Lf2uvCyk97H51DO54VdvRCMyRFhGRMzpk1sqqbpJsW027Cp2vF9LRzzEvj918H9GMuDUDvexhe2DgXBCR2vr6el4NIau6SenydVdQyM8rLw8RzR9/gmgV25rl52UP26siMi0iV0TkYrfbreX0fQ5auYeUYS9Fvl640D0Igpr3ftF7/4X1tlnm5YcAHgH4BtFS1XUA+yMQtk+o6mUA11T1sqpOAHiVw/eZ9cq9ZOT4pf2NgyKG8IVy9PX19UBVLwG4adMdb2eUlych/wrAXxAtemhgNIpwVQBTBvkFVa3m9H2+VjtB+sOecb5+O2ksRXP1woBer9cr7XZ7WlWXAHxh1xKyGS9PQv5He30E4HBENiqIi1zjAKqqmst2Ys/iyEDfALBlzy5M+V4UPoQvUuheDcNwAcDnAH6pqisAZgj5yKtpoN/DydTkLGZFFjqELwTocZXd4P5Xg30+5ZCdkBdTHQvb7+H0EFvasCdD+Hgte942ISlu6L6zs+NqtdqFSqVyVURuIVqldA3RBIm0HqICOAbwFEAdwJ8JeTFkz+Y86inJEP4zEblVqVQ+Gh8fnzw4OHAE/YxqtVpjqnoF0Q4vqwA+QfpV9lBE9kTkRxH5XxH5hpAXFvZTIyQZ5Ouz1gZXReSm937u4OAgIOhnDNnb7faM934ZwF0R+UxEsgjZXwLYFJHvnHPfVyqVTUJeWNifINoF6L79O+3iXFVE5i26vKOqHx8fH8/kvTCXd0ev9nq9K977f1HV26r68QcuQ/z/8vLHAP4O4G8ish4EwQEhLyzs++bmX5m7P0oZ9oqqXlTVj733d8IwvNXr9a4gm6nX5Qc9UYBbVtU7qvqpTeJIK0z6OS8XkTqA/wHwvYg8Gxsb6xKbwipZnPtLstaSIuxjqjqnqjdV9a73/hPkfGw9z45eBTCnqp+o6oqqLiAa101rkcXPebk1hm+995vdbvfV8vIy3bwc+XpWxTlBNHtw0QzoE0RDe1WC/gFujmiI5La9plmAY14+OrBnVZwb2Ebz6up5dfSq9ZDLdqXZWyqAYxF5aqH635mXlxr2JyLyD7u2EM3Z9wVop6UHXYIgmHLOXRWRm4imuabp5j0ReSEiD0TkGxG5z7y81LDvi8hDq8P8ICLPUwzhC+PquQN9c3MzEJGLIvIRookxsyn2kqGIvBSRRyJyzzn3vXPuJ+blpVYnCIKGc+6fzrk6gIeIpq+m9bwL4eq5A73dbtdshdp1AB8BSHM4raOqz1T1nwDuOec2arUaQ/aSu/rk5OShiDwG8A8AD5DungL9J/ksIJpvT9DfpHq9Xul0OjPe+2s2SeYKokp7Km5uxZiHiKa5/iAizycmJhiyl1xLS0s959wLVV0H8L21gTQLc7k/ySdvjh5v33sDwJI5e1phUBvAcwBrqvpjGIbb7Xb71eLioicKpZe22+1j733DYN/IwNX7T/KpEvQ3uDmiCTI3VPUzW3c+k9J77FkPvmHFt0cYjZ1iqEQIb23gEaLpsY9SdvVJA/1TC+NzVZTLk6NnVdTwAJoism2LVtbN2bld8+ipY06+kYGr57oolyfQX+sRkU4RrisiuyLywDl3zzn30DnHAhxdPW1Xz3VRLhegW4iTSY4jIscAdgz0tbGxseciQjenq2fl6rksyuXF0V+7QSm5eaiqLwH8hGgK5Nb4+Pjh7du36eZ09axcPZdFubyAPuhI4rR67z0AT1R1S1X35+fnSzecJiIQERURbzUJPWsHiZP90Doo34GSWbp6Vm252KBnGLbHPfemqj7w3m/1er2jWq2mZQPdOdezFOWViKRxYkkH0e6qu4jmi5cq1cnY1XMZvufB0bMK27PstXOlSqXStqm9zxCtymuf8VeOwsmxWbWPXIbveQA9i1BHARzZkNoDEdlEicfNnXMtREOGD60ecZa53CNxcmyfq/8D0S5Dac2Bz134PlTQMwzbeyJyYKD/5JzbQ4nHzW3l3Qtzph/x4XulxUs7t+x3baPcJ8d2nHMvbJHToxRdPXfh+7AdPZOw3fLVPRHZrFQqT6vVaqk3lFheXg7DMDzw3j9U1a8BfIv33z4puVnDPbu2UeKTY1dXV8MgCA6ccz/ZUtZnSOdAhtyF78PepjaLECdU1Zci8pM12udBELRQfp3lIIORPWtubGysHYbhnqpu2ec9RDonAA1q20Nrh3lw9Gm7EVMp9XodC2M3VXVTVfdGYYXagL3S/gjgvxEty2xYIwsHAN6y7z+wnx+pE2pWVlZ6qnoI4KnBnlZNomptes7a+Gg6uuUsk9Z7zti/08hjmgCeqepDVd323r8clRVqq6urYb1ej2EHour7EaLDL65bw0uedR5/Pz7//Stz8pE6vMJ7/wpRMfMx0jt++bX2Xa/XK8O6p8MM3bPq8TrmbLvWiEdqumsf7C27D2uI5l/P9TXg5PfXEM0ebGD0NsnMat7AoIi1NWqgZ5KfW298YFdzFBevJGBv4/TpJf0datwpNqyR7wPojNo9s/t1qt1YWypNnj5sR88iPy/tjK4PyNlb9Xq9YwBv2z2u9HWMHWvYnRFf0TcoEpxIoY3nIk8fJugV++ATOF0lPmt+XvYZXR8EPApyjvcQNajtpJGn1+z3VJHuwaDvJVeyhzXS+TmVy2jwlKENa+LMUEC3D5t2T8f8nDpr5DMoT89jiloYR88id2F+TuUxIszFvPdhgZ7Fh2d+TuWxDeWiIDdMR8+i4s78nMpbVJiLgtywQM+i4h4PFbUAtJmfUx+Yp5dyZ50yVd0VZ99CiaJKqbKA3rMeuCkibRGhm1OpRIZlcfVzBz2jobUOgJcisgvg0PZNo6gPbUulq/UMw9GzqEK2EC1NfQJgV0Q4C4z6UGVReR/6pJlhgJ7F0FoHwJGqvkC0Vxwr7tSZ2hLSrbwPfdLMsBw97Q/9c16lqh0e0EB9qDKqvA990swwQM9iaI2i8qyhT5pxfAYUdS7mNtRJMwSdokZABJ2iCDpFUQSdoiiCTlEUQacoiqBTFEXQKYoi6BRFEXSKGikN4wCHQQv7Od+dKrMUgBcRjyHtgjQMR+cmjtSoKbTNUF6JyLFzrjcKoJdyYT9VDmW0A1Ib0Q5Iz0TkZaVSaY8C6KVc2E+VRlnugLQFYM851yo96GVd2E+VRpnsgKSqr1R1T1WPxsbGuuf9oYKSPJzcHE9LlaItpR0degBdVT1W1e7y8vK574BUluG13BxPS5WiLZVuBySOo1PUCGgooDvnVERCEelZjp7G2CIr79SZlOFx3kM/5mlYoHedcy0ReSkiTQBpFCdYeafSaEOlPM57KKBXq9Vj59y+c+6piByIyHEKv5aVdyqPbSgXx3kPBfSxsbFjAPuq+lRV91U1DdBZeafyGBXmYiboUECfmZnpee9bqvrSerg0QveK9cAzdk0yT6feMz8/1X5QouO8hwW6V9WOqrbsBnQRjTUyT6fKlJ/n5rz1oQ2vWUGuaTl6Wq7OPJ3KU9vJRSFuqKDXarVj59y+iDwFsJdSQa4KYBbAVQALAKYYvlPvGLZPWZu5am0oDUfPRSFuqKBXq9U2gAMAOwAOUirIZfXAqPKH7acMIqX8PDdLsocG+vz8fNd7f6SqzxGt7GmCC1yo8oTtobXpA7uawyrEDTt01zAMW977fVV9BuAl0luyyvCdGnbYnpv8fKigZ5jDMHyn8hC25yY/zwPoWfV6DN+pYbeVXG2ZNlTQLWc5lceklKczfKeGGbbnKj/Pg6Nn1fMxfKeGGbbnKj/PC+hZ5TKT9vA+BXAdwCxdnepz81lrG59aW5nMeZsuvKPvA9gG0LCeMK3wfQ7Asl1zdHXqHNpHaG24YW16n45+kqdncWPiHvtjALftla5OJd38VNtIMWw/ZVzDzs/z4uhxqNMAsAbgid0oujpVNDf3iPZv3xaRDRF5loewPU+gdyyX2bBrl65OFdDNuyKyLyI/icimc24POTmJKBegW2izb26+Zu6eVk9IV6fOpT3YwqznIvKoUqlsjY+PH+QhbM+Tow/MbVIK3+nq1Hm4eWgbqTxFVG1/YQu3QNBfd/WsqpV0dUpqtdq4c25BRFYyaAcdAHsAnqjqlqruz8/Pdwn6YGVVlKOrj7g2NzcD7/1lg/wLAJ+k6ebWVjdV9YH3fqvX6x3VajUl6G/uFbMoyiVd/VMAtxBNlJgm7KMRsjebzWlVvQHgcwD/koGbZ9Vuywd6XJQTkcci8k8R2UQ0VziN/eRiV18B8KU5+wJD+JFQtdfrLXjvP/Per5qbz6Ts5o8A3LfX/bwU4fLq6ADQcc69EJHHADZFJM0hiqrBfdtgX2EIX343BzCrqp+o6iqAW6p6ZZTcPJegr66uhtVq9aBSqTypVCprNvngFdI7tmnawvbbdjGELzfk0wCuq+rndv0CwIWU2n4h3Dyvjo5ardauVCq7IvJIRJ4gqmamVcFkCD8iCoKg5pxbRFR8+zmCQ3onpBbCzXML+tLSUq9are475x6LyAMR2RSRl0hvX2yG8CXX+vp6oKqXANwUkSw69MK4eW5BB6Ai0lTVbQD/VNU121curd6SIXzJQ/Z2uz2tqkvm5l8g2kFmehTdHMkPvbCwABHJFezNZrMXhiFUdRzRcMglRGuG0+igHIDA7oEiOlHjFYBWo9HoLi4uKpEpbl6uqssAfg3g96p6F8B8ym6+C+AfAP4M4FsA26urq7kDvdFo5NrRMTMz44+Pj5thGD5V1TUADzPoNeMQ/i6A3wH4DaLJNHT2AkMO4GNV/bWq/k5V76QMeeHcPO+OjsXFRW00GqF1SBfsgaXt6mOIDr6fNIfvIZqhR2cvKOTWYf8h2XGnGLIXxs2Tjh4U4Bl2AOyKyEMA64iGSi4amGn0TMkGAgBeRLrOuXYQBF2DnrDnX2+MzlKEHJbiPbcIsxBunntHT7q6c05EZEJELgO4jPSOte139qqIhCJyAOCg1Wo1Z2ZmPDnKvZvPISqq/ieAf0M0kpI25D0D+76I/EVE/p5nNy9Ejt7v6kEQPHfOPRCR+za+nuZwW9LZlwDcAXBXVW80m03m68UI2V8bQUkZ8nh15aaI/IBo4dXzIrh5IRw9dvWjo6Oe994DGLPQ/TKi7XnHUgrh444vEJFAVT2App0P12S+Xoi8/PcWui8i/QlQbUTLp78RkT8DqDvnnt29ezfXoMeOXgjQAaDdbmu73e6pqlfVGoBLIjKL9KYz9sM+BsCpagcszhUB8n9HNPnpGtIr1ibdfBfRxJg/icjXAB4751rz8/NK0FPU1NSUbm1t9ZxzYSLMvmSv1RQfrCAqUo5bzh6ISOica1er1e6rV696AHR8fJzADwnwRqMRz6v4JAF5VsW3ENEhDBsAvgbwVwAPAOzduXMnzPv9KhzocQi/v78fqmqoqs567ml7HUsR9lPDbiIyLiJORLTX6/lms9nb2toK6e5Dc/FfWC4eh+tfZgz5IwBfIRpO+w5AI88FuMKDDgCHh4cahmFXVbvmvpOIFirE+TrShl1EpkXkoohMqqrz3ve8922G8kML1X+FqLr+O8vJr2UA+am8HMCfAPwNwBaAVlGee2FBn52d1e3t7Z6qHiMa7hhDVJibzSA3i2G/YL//kvf+gqoCwDHz9qHl4/+BaAjtFqLC22QGkCcnxvwXgP9GNI/jMK8LV0oFehzCNxqNrj2MCqLdQpKungXskwAuWsW/ipMTMwn7+UL+BwC/RTROfjnl+sz/m5cXCfLCg56APUS0zZQgKp5NWV6dNuxi96pm7n4BUcFOEa2TDxuNRthoNJTApwf4ORfdSpOXlwp0+xBq4Xv8ACYAXDQQ0xxffy1vN4ePi4AV63B6dPdUXfy8im5vg/zbouXlpQM9EcK3RKQjIoGIzGY0vj4I9lkLH2fM4T3dPVUX/5WF6lkX3WIli29/BPC/Bv1h0UL2UoGehD0Igp6IOAvfsxhfHwT7lME+m/h7dPf0XDwO1bMsuiXdvPDFt9KCHsN+TuPrg2CfTHQudPd0XXwVwA1kV3TrD9k3UPDi25tAD8rSYMbGxrphGD4D8D1OCnKScU4Xd5bTiAp109Zg4+sHC/126/X6PoBO0RtOyg5etWhozp7TLZxs2HndvlfN8Nn15+XfWG6+jhzv//ahjbTwjg68Nr7eQlSkC8xxs6jEv4u7zwG4ICLjlUoF1WpVR30KbZ+DX8PJBp2/RzRsdl4u3g95KYpvpQ7d+/N1A715zrAPyt0XAMzbGvpxEal473273Q739vbCS5cueQL+M+D/aqF71rn4myD/o70WuvhW+tA91urqaliv1+MH2K+sw/i485zoC0uvAfiFql4Pw/D7MAw3vPeN77777tB7/6rMIf0bQvRlA33Zvp47pzB9pCAvZeieM2fvD+cvxuG87TU+472/aM5fBeDKVrR7Rwf/FFGV/eI5hOkjCXlpHT1Hzp7sTC+oatXAXlDVmwCequoWgB8RVXufANiv1+tH1jkVzuUT7j2ZSF+u58DBR9bJY5UW9JzBLgCqqhovvJm3v7+P6AjfDURbE20jOh9+tyjQvwHuOatPXDXXHjbgIw156UF/C+yKaLfXG7ZIJUD602Xflr9PIBpvX7DrY3O8hsG+2Qf9Ub1ebyKa6jtU8PvATgKehHvJXuPPN0zARx7ykQB9EOwi4gF07JTWj1X1MqJFMefZCPuLdlcRbT64b3Anod9FdE78YQL8tjXgzOAfAHW8sCcGe9o6rLk+uGOwp/r+32EA3rF7+gTROPlfRg3ykQG9H3bbjuoVgBeI5lDfUtWr5xjKv8nl4yG5Zh/0hwb6bgL8ljXifvjTVD/UVXufMdhzBvp0juDud/EGogkw9w3we6MG+UiBnoQ9CILHqtr03u+q6g6AlwB+ec55+/tA30nAH4PfMdj74U9T/VDHEUgM9lRfCD9suAeF6vcM8PsGfGPUIB850GPYd3Z2jlqtVrvdbh957w9UtYVoXbla4WgqB/cmCT36w3T7uj0A/jTVD3XN3lfewO4P1Q+s8/sW0eKUbyx0H9lpyCMHOgAsLi56AO16vR4fp9OzRtIzeJbMUWs5asj94L8J/rT/Zl6hHgT5EYBnIrIO4HtV/QpAfRRDdYL+hrxdROKjkw9V9TaAmxY6T+e4gQ+CfxQVh+qPReQ7EfkbgO9F5Efv/c6oQw6UdGbce7q7NhqNroi0LOTbs+OeenZ/KtYhOhTnCKtRAryNkyOMvxaRP4nI/4jID865He99c5QhL/3MuPd19vv37x+pasd7f6iqLxBV5F8gGt9eKYC7j6qLJ6vqXwO455x7VKvVDldWVnq8TXT0U5qfn9f5+fnezs5Oy3K9PQM9dndHd8+li8fbPv0VUYX9J+/94eeff851/yjpMtU0Q3lE4+wHiCq1cUW7g6gyr3bvCPxwAP8J0ZZPf0O0hvxrRDvCNAC0uLkHQ/d3DuUBtOr1ejxWfRCHiCJyG8BNVV1BNGd9iuH8uYfpaxaqr2GEx8YZumfk7iLyTER2rVjXZTg/1DD9WwAP6eJ09NTdvVKpHKjqC1VtAHgG4Dmi1WfDXplVRsDjOeq7iMbBfzDQ72PEJ798iAj6+wHfXF9f77Tb7WYYhnsAdhCtJ4/XWt/AyfzveLINw6SzAb7BMJ2gn7tWVlZ69Xr9JaJDFl8immq5AWBZRD432K8BuGRLYC/Q5d8J7uRCnicJwDcM+F26OEEfWjifdB8ReYho+uwnqnpNRD5S1Y8Y1r+TezdwsjR3jYAT9NwCLyLPE060qKrXEE2lXcbJPuV5WcaZN/dO7rDTIOAEPbfAb2xsdLrd7r73frvX620g2hDygYH+KfK168ow4d4d4N5xga2we+YR9BHR8vJyaA372PL4Z9agH5nL9++jdh35XdOdJdybdG+CXtY8fhsnmyf+kHD5/l1aphPATxQEfAXQE5FjOyknnlX4Jrh36d4EvZTAG/T7CReLXb5/37UrAGZFZNr2sptCtJ/dmIiMq+o4zm8zy3dx7A6AePXfSxHZA/Dcdu4h3ASdLm8QnNpJVUQuArgkIlcAXFPVyyIyhWiY7pKIXFTVCZzMwgsycv+3bWpxamcbEWmJyKGINABsq2pDVXcJN0En8ObywOndVkWkZq8XzdEvAJhyzs0CuKqqCwCmVTWejFPrc/9qSm/zbdtU9e9V90pEjp1zB6q6h2h+QZNwE3TqLeCvr68H3W63Zs5ddc7VRGRSRC5572csfHcSLU64kHR/C/PT0Ns2njy1+6yIHItILwiCdhAErYmJia5t10URdOpNsg0TeoiKWtjZ2XGtVmus1+tNdLvdmogEqioARERqfe6flqOfgtkcfmBYf+fOHTo2QafOqngzyz7YMMj9VTWtFXSZHg5BEXTqDO5PUYPE9dMURdApiiLoFEURdIqiCDpFUQSdoiiCTlEUQacoiqBTFEGnKIqgUxRF0CmKIugURRF0iqIIOkVRFEVRFEVRFEVRFEUVVv8HxZOxQKM+4JEAAAAASUVORK5CYII='],
    }

    for file_dict_key in files_dict.iterkeys():
        file_path_name='/usr'+files_dict[file_dict_key][0]
        file_content=files_dict[file_dict_key][1]
        if not path.exists(file_path_name):
            file_path_name=home_path+'/.local'+files_dict[file_dict_key][0]
        file_content_decoded=b64decode(file_content)
        create_file(file_path_name,file_content_decoded)
        files_dict[file_dict_key][0]=file_path_name

    eLOGO_file=files_dict['eLOGO'][0]
    op_name=''
    sms_list = gtk.ListStore(str,str,str,str,str)
    sms_window_started=False
    lang_en={
'msg_del_wait':['Please  Wait While deleting your SIM SMS Inbox folder','Deleting'],
'msg_del_done':'Done Deleting SIM SMS !!!',
'credit_no_inalid':['Your recharge Card number is invalid','Attention'],
'credit_submit_wait':['Please Wait for 5 seconds while communicating with etisalat Egypt !','Attention'],
'rechargetxtlabel':['Enter the',' digit secret card number:'],
'submit':'Submit',
'cancel':'Cancel',
'ok'   :'     OK       ',
'close':'    Close     ',
'wait_activating':'Please wait while activating...',
'activated':'Activated',
'inactivated':'Inactivated',
'activate':'Activate',
'inactivate':'Inactivate',
'notactivated':'Not Activated',
'err_msg_title':"Error Message!",
'unable_activate':'Unable to Activate your USB Modem',
'nousb':"Unable to Detect any USB Modems , You Might Need to Plug in your USB Modem before trying to activate it  Via etisalat Mobile Internet Manager",
'usb_used':"Unable to Activate you USB Modem as it seams to be used by another process you Might Need to Disable Mobile broadband from network applet before Connecting Via etisalat Mobile Internet Manager or click on 'Disable & Retry'",
'gammurc_create':['~/.gammurc not found , so I created for you . Just retry again','Error'],
'plz_wait_activating':'Please wait while activating...',
'DiswanRtry':'Disable Wwan & Retry',
'wait_5_chk_crdt':['Please Wait for 5 seconds while checking your credit','Credit Check'],
'sms_fldr_ttl':'SMS Folder',
'wait_load_sms':'Please Wait while loading SMS from SIM',
'snd_sms_ttl':'Send SMS',
'send_sms_label':["Number :  ","Message:  "],
'snd_cls':['       send       ','       Close       '],
'sms_invalid':['SMS number is invalid','Attention'],
'sms_txt_70':['SMS Text length is more than 70 character','Attention'],
'pkg_chng':'Package Change',
'apply_close':['   Apply    ','    Close    '],
'pkg_apply':['Please Wait a moment while applying change of your package','Wait!'],
'main_title':app_name[0]+app_name[1],
'recharge':'Recharge',
'chkcredit':'Check Credit',
'del_all_sms':'Delete All SMS',
'mngsms':'Manage SMS',
'sendsms':'send SMS',
'pkg':'Package',
'about':'   About   ',
'quit' :'   QUIT    ',
'op_name':['vodafone','etisalat','mobinil'],
'credit':'Credit Check',
'credit_recharge':'Credit Recharge',
'go':'Pay as you Go',
'daily':'Daily Unlimited',
'19LE':'19LE  Unlimited',
'49LE':'49LE  Unlimited',
'99LE':'99LE  Unlimited',
'149LE':'149LE  Unlimited',
'249LE':'249LE  Unlimited',
'reset':'Reset Speed',
'enableSP':'Enable Speed Plus',
'disableSP':'Disable Speed Plus',
'enableLV':'Enable Life Validity',
'disableLV':'Disable Life Validity'
}
    lang=lang_en
    etisalat_data={
'op_name'     : lang['op_name'][1],
'op_code'     : '602 03',  
'credit'      : [lang['credit'],555,''],
'recharge'    : [lang['credit_recharge'],551,''],  
'go'          : [lang['go'],5031,''],
'daily'       : [lang['daily'],5033,''],
'19LE'        : [lang['19LE'],5032,''],
'49LE'        : [lang['49LE'],5035,''],
'99LE'        : [lang['99LE'],5034,''],
'149LE'       : [lang['149LE'],5037,''],
'249LE'       : [lang['249LE'],5036,''],
'reset'       : [lang['reset'],545,''],
'enableSP'    : [lang['enableSP'],553,''],
'disableSP'   : [lang['disableSP'],553,'U'],
'enableLV'    : [lang['enableLV'],552,''],
'disableLV'   : [lang['disableLV'],552,'U'],
'credit_limit': 15,
'logo'        : eLOGO_file
}

    operator_data=etisalat_data
    lang['main_title']=operator_data['op_name']+app_name[0]+'('+app_name[1]+')'
    GUI_main()
    gtk.main()
